﻿using blacklist_client.bases;
using blacklist_client.bases.factions;
using blacklist_client.bases.items;
using blacklist_client.bases.menus;
using blacklist_client.bases.menus.settings;
using blacklist_client.bases.users;
using blacklist_client.managers;
using blacklist_client.utils;
using CitizenFX.Core;
using CitizenFX.Core.Native;
using MenuAPI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blacklist_client
{
    public class Client : BaseScript
    {
        public static Client INSTANCE { get; set; }

        public Config ClientCfg = new Config();

        public Camera BASE_CAMERA;
        public bool IsPlayerCustomizing = false;

        private bool FirstSpawn = true;

        #region Managers
        public ItemManager ItemManager = new ItemManager();
        public PermissionManager PermissionManager = new PermissionManager();
        public FactionManager FactionManager = new FactionManager();
        public VendorManager VendorManager = new VendorManager();
        public MapManager MapManager = new MapManager();

        public HudManager HudManager = new HudManager();

        public NUIManager NUIManager = new NUIManager();
        #endregion

        #region Menus
        public ProfileManagement ProfileMenu;
        public ProfileCustomization ProfileCustomization = new ProfileCustomization();
        public VendorMenu VendorMenu = new VendorMenu();
        #endregion


        public Client()
        {
            Client.INSTANCE = this;

            EventHandlers.Add("onClientMapStart", new Action(this.MapStarted));
            EventHandlers.Add("onClientResourceStart", new Action<string>(this.OnResourceStart));
            EventHandlers.Add("bl:CL_E:ServerReady", new Action<string, string, string, string, string>(this.HandleServerReady));

            EventHandlers.Add("bl:CL_E:Character:AddActive", new Action<string, string, int, string>(this.MapManager.AddActiveCharacter));
            EventHandlers.Add("bl:CL_E:Character:RemoveActive", new Action<string>(this.MapManager.RemoveActiveCharacter));

            EventHandlers.Add("bl:CL_E:HasPerm", new Action<string, bool>(this.PermissionManager.HasPerms));

            EventHandlers.Add("bl:CL_E:UserProfiles", new Action<string>(this.HandleUserProfiles));
            EventHandlers.Add("bl:CL_E:ProfileCreated", new Action(this.HandleProfileCreated));
            EventHandlers.Add("bl:CL_E:ProfileDeleted", new Action(this.HandleProfileDeleted));
            EventHandlers.Add("bl:CL_E:ProfileSelected", new Action<int, string>(this.HandleProfileSelected));

            EventHandlers.Add("bl:CL_E:NUI:Notify", new Action<string, string>(this.NUIManager.ShowNotification));
            EventHandlers.Add("bl:CL_E:NUI:SyncData", new Action(this.NUIManager.SyncData));

            EventHandlers.Add("bl:CL_E:Vendor:RefreshData", new Action<int, string, int, string, string, string>(this.VendorManager.RefreshData));
            EventHandlers.Add("bl:CL_E:Vendor:UpdateList", new Action<string>(this.VendorManager.UpdateList));
            EventHandlers.Add("bl:CL_E:Vendor:CreateCallback", new Action<string, bool, int>(this.VendorManager.CreateCallback));

            //EventHandlers.Add("bl:CL_E:Inventory:ItemDropped", new Action<Vector3, int, int>(this.ItemManager.ItemDroppedInWorld));
            EventHandlers.Add("bl:CL_E:Inventory:ItemDropped", new Action<string, string>(this.ItemManager.ItemDroppedInWorld));
            EventHandlers.Add("bl:CL_E:Inventory:ItemPickedUp", new Action<string, string>(this.ItemManager.RemoveWorldItem));
        }

        public void RegisterEventHandler(string _event, Delegate _action)
        {
            try
            {
                EventHandlers.Add(_event, _action);
            }catch (Exception ex)
            {
                CitizenFX.Core.Debug.WriteLine("[Core]: Error registering EventHandler in Client => " + ex.Message);
            }
        }

        private void HandleServerReady(string cfg, string pms, string vendors, string items, string facs)
        {
            this.ClientCfg.LoadConfig(cfg);

            if(Convert.ToBoolean(this.ClientCfg.LoadedConfig["bl_base_pluginOverridesCharacterSelector"]) == true)
            {
                ProfileManagement_Settings PM_S = JsonConvert.DeserializeObject<ProfileManagement_Settings>(pms);
                if (PM_S != null)
                {
                    this.ClientCfg.PM_Settings = PM_S;
                }
            }
            else
            {
                this.ClientCfg.PM_Settings = new ProfileManagement_Settings();
                this.ClientCfg.PM_Settings.CameraLocation = new Vector3(411.7894f - 1.0f, -998.423f, -99.20408f + 0.5f);
                this.ClientCfg.PM_Settings.DoorLocation = new Vector3(409.7066f, -1001.121f, -99.00416f - 1.0f);
                this.ClientCfg.PM_Settings.StopLocation = new Vector3(409.3145f, -998.3396f, -99.00413f);
                this.ClientCfg.PM_Settings.PreviewPed_TurnToHeading = 270.4346f;
                this.ClientCfg.PM_Settings.PreviewPed_SpawnHeading = 2.538533f;
            }

            this.ProfileMenu = new ProfileManagement();

            List<BlItem> Server_Items = JsonConvert.DeserializeObject<List<BlItem>>(items);
            if(Server_Items != null)
            {
                this.ItemManager.RegisteredItems = Server_Items;
                CitizenFX.Core.Debug.WriteLine("[Core]: Loaded " + this.ItemManager.RegisteredItems.Count + " items.");
            }

            List<Vendor> Server_Vendors = JsonConvert.DeserializeObject<List<Vendor>>(vendors);
            if(Server_Vendors != null)
            {
                this.VendorManager.RegisteredVendors = Server_Vendors;
                CitizenFX.Core.Debug.WriteLine("[Core]: Loaded " + this.VendorManager.RegisteredVendors.Count + " vendors.");
            }

            List<Faction> Server_Factions = JsonConvert.DeserializeObject<List<Faction>>(facs);
            if (Server_Factions != null)
            {
                //this.Fa.RegisteredVendors = Server_Vendors;
                this.FactionManager.Registered = Server_Factions;
                CitizenFX.Core.Debug.WriteLine("[Core]: Loaded " + this.FactionManager.Registered.Count + " factions.");
            }

            Utils.SetPlayerModel("mp_m_freemode_01");
            Game.PlayerPed.Position = new Vector3(413.122f, -998.4281f, -99.40414f);
            Game.PlayerPed.IsPositionFrozen = true;
            API.FreezeEntityPosition(Game.PlayerPed.Handle, true);
            this.BASE_CAMERA.Position = this.ClientCfg.PM_Settings.CameraLocation;
            this.BASE_CAMERA.Rotation = new Vector3(0.0f, 0.0f, +90.0f);
            API.RefreshInterior(API.GetInteriorAtCoords(413.122f, -998.4281f, -99.40414f));
            this.ProfileMenu.ResetMenuItems();
            this.ProfileMenu.ToggleMenuVisibility();
            API.RefreshInterior(API.GetInteriorAtCoords(413.122f, -998.4281f, -99.40414f));

            MenuController.MenuToggleKey = (Control)(-1);

            this.NUIManager.SetupCallbacks();
        }

        private void HandleUserProfiles(string json)
        {
            Utils.Registered_Profiles = JsonConvert.DeserializeObject<List<Profile>>(json);
            if(Utils.Registered_Profiles.Count > 0)
            {
                if(Utils.GetSelectedProfile() != null)
                {
                    //NUI Inventory setup
                    this.NUIManager.SyncData();
                }
            }
        }

        private void HandleProfileCreated()
        {
            this.ProfileMenu.ResetMenuItems();
            this.HandleProfileSelected(Utils.Registered_Profiles.Count - 1, "");
        }

        private void HandleProfileDeleted()
        {
            this.ProfileMenu.ResetMenuItems();
            this.ProfileMenu.ToggleMenuVisibility();
        }

        public void LoadProfileCharacter()
        {
            API.RenderScriptCams(false, false, 0, false, false);
            Profile p = Utils.GetSelectedProfile();
            if(p != null)
            {
                if(p.Heritage != null)
                {
                    if (p.gender == 0)
                    {
                        Utils.SetPlayerModel("mp_m_freemode_01");
                    }
                    else
                    {
                        Utils.SetPlayerModel("mp_f_freemode_01");
                    }

                    if(p.Class != null)
                    {
                        Game.PlayerPed.Position = p.Class.SpawnLocation;
                    }
                    else
                    {
                        Game.PlayerPed.Position = new Vector3(460.16f, -990.82f, 30.69f);
                    }

                    Game.Player.CanControlCharacter = true;
                    MenuController.DisableBackButton = false;

                    if(p.UsesModel == false)
                    {
                        p.Heritage.Load(Game.PlayerPed);
                        p.Clothing.Load(Game.PlayerPed);
                    }
                    else
                    {
                        Utils.SetPlayerModel(p.characterModelInfo.MODEL_PATH);
                        foreach (KeyValuePair<int, int> BodyGroup in p.characterModelInfo.BODYGROUP_DATA)
                        {
                            Function.Call(Hash.SET_PED_COMPONENT_VARIATION, Game.PlayerPed, BodyGroup.Key, BodyGroup.Value, 0, 0);
                        }
                    }

                    this.IsPlayerCustomizing = false;
                    PlayerList pl = new PlayerList();
                    foreach (Player pp in pl)
                    {
                        if (pp != Game.Player)
                        {
                            if (Function.Call<bool>(Hash.NETWORK_IS_PLAYER_CONCEALED, pp) == true)
                            {
                                Function.Call(Hash.NETWORK_CONCEAL_PLAYER, pp, false, false);
                            }
                        }
                    }
                    API.FreezeEntityPosition(Game.PlayerPed.Handle, false);
                    API.EnableAllControlActions(1);
                }
            }
        }

        private void HandleProfileSelected(int index, string activeCharJson)
        {
            API.DisableControlAction(1, 200, false);
            Utils.Selected_Profile = index;

            Profile p = Utils.GetSelectedProfile();
            if(p != null)
            {
                if(p.Heritage == null)
                {
                    if(p.gender == 0)
                    {
                        Utils.SetPlayerModel("mp_m_freemode_01");
                    }
                    else
                    {
                        Utils.SetPlayerModel("mp_f_freemode_01");
                    }

                    Game.PlayerPed.Position = new Vector3(402.97f, -996.71f, -99.00f);
                    Game.PlayerPed.Heading = 179.90f;

                    this.ProfileCustomization.ResetMenuItems();
                    this.ProfileCustomization.ToggleMenuVisibility();

                    this.BASE_CAMERA.Position = new Vector3(403.03f, -1000.29f + 2.0f, -99.00f + 1.0f);
                    this.BASE_CAMERA.Rotation = new Vector3(0.0f, 0.0f, 0.0f);
                    this.BASE_CAMERA.PointAt(new Vector3(402.97f, -996.71f, -99.00f));
                }
                else
                {
                    if (p.gender == 0)
                    {
                        Utils.SetPlayerModel("mp_m_freemode_01");
                    }
                    else
                    {
                        Utils.SetPlayerModel("mp_f_freemode_01");
                    }
                    this.LoadProfileCharacter();
                }
            }
            else
            {
                Debug.WriteLine("No profile in slot: " + index);
            }

            Function.Call(Hash.ENABLE_DISPATCH_SERVICE, 1, false);
            Function.Call(Hash.ENABLE_DISPATCH_SERVICE, 2, false);
            Function.Call(Hash.ENABLE_DISPATCH_SERVICE, 3, false);
            Function.Call(Hash.ENABLE_DISPATCH_SERVICE, 4, false);
            Function.Call(Hash.ENABLE_DISPATCH_SERVICE, 5, false);
            Function.Call(Hash.ENABLE_DISPATCH_SERVICE, 6, false);
            Function.Call(Hash.ENABLE_DISPATCH_SERVICE, 7, false);
            Function.Call(Hash.ENABLE_DISPATCH_SERVICE, 8, false);
            Function.Call(Hash.ENABLE_DISPATCH_SERVICE, 9, false);
            Function.Call(Hash.ENABLE_DISPATCH_SERVICE, 10, false);
            Function.Call(Hash.ENABLE_DISPATCH_SERVICE, 11, false);
            Function.Call(Hash.ENABLE_DISPATCH_SERVICE, 12, false);
            Function.Call(Hash.ENABLE_DISPATCH_SERVICE, 13, false);
            Function.Call(Hash.ENABLE_DISPATCH_SERVICE, 14, false);
            Function.Call(Hash.ENABLE_DISPATCH_SERVICE, 15, false);

            API.SetCanAttackFriendly(Game.PlayerPed.Handle, true, false);
            API.NetworkSetFriendlyFireOption(true);

            Game.PlayerPed.IsPositionFrozen = false;

            BaseScript.TriggerServerEvent("bl:SV_E:CheckPerm", "vendors.edit");
            BaseScript.TriggerServerEvent("bl:SV_E:CheckPerm", "vendors.create");

            if (activeCharJson != "")
            {
                this.MapManager.ActiveCharacters = JsonConvert.DeserializeObject<List<BlCharacter>>(activeCharJson);
            }

            //Tick += this.MapManager.CreateVendors;
            this.MapManager.SetupVendorPeds();

            Tick += new Func<Task>(async delegate
            {
                if(Game.Player.WantedLevel > 0)
                {
                    Game.Player.WantedLevel = 0;
                }

                //API.DrawRect(0.1f, 0.8f, 0.1f, 0.1f, 82, 80, 85, 200);

                this.VendorManager.Tick();

                this.ItemManager.Tick();

                this.NUIManager.OnTick();
            });

            MenuController.MenuToggleKey = (Control)(-1);

            this.NUIManager.SyncData();
        }

        private void OnSpawn()
        {
            if(this.FirstSpawn)
            {
                Function.Call(Hash.SET_PED_DEFAULT_COMPONENT_VARIATION, Game.PlayerPed);
                Game.PlayerPed.Position = new Vector3(413.122f, -998.4281f, -99.40414f);
                Game.PlayerPed.IsPositionFrozen = true;
                this.FirstSpawn = false;
                API.RefreshInterior(94722);
            }
            else
            {
                if(Utils.GetSelectedProfile() != null)
                {
                    if(!(Utils.GetSelectedProfile().UsesModel))
                    {
                        Utils.GetSelectedProfile().Heritage.Load(Game.PlayerPed);
                        Utils.GetSelectedProfile().Clothing.Load(Game.PlayerPed);
                    }
                }
            }
        }

        private void OnResourceStart(string name)
        {
            if(API.GetCurrentResourceName() != name)
            {
                return;
            }

            BaseScript.TriggerServerEvent("bl:SV_E:PlayerReady");

            API.DoScreenFadeOut(200);
            this.BASE_CAMERA = new Camera(API.CreateCam("DEFAULT_SCRIPTED_CAMERA", true));
            Game.Player.CanControlCharacter = false;
            Utils.SetPlayerModel("mp_m_freemode_01");
            Game.PlayerPed.Position = new Vector3(413.122f, -998.4281f, -99.40414f);
            Game.PlayerPed.IsPositionFrozen = true;
            API.FreezeEntityPosition(Game.PlayerPed.Handle, true);
            API.RefreshInterior(API.GetInteriorAtCoords(413.122f, -998.4281f, -99.40414f));
            API.DoScreenFadeIn(200);

            if(this.BASE_CAMERA != null)
            {
                API.RenderScriptCams(true, false, 0, true, true);
            }

            PlayerList pl = new PlayerList();
            foreach(Player p in pl)
            {
                if(p != Game.Player)
                {
                    if(Function.Call<bool>(Hash.NETWORK_IS_PLAYER_CONCEALED, p) == false)
                    {
                        Function.Call(Hash.NETWORK_CONCEAL_PLAYER, p, true, true);
                    }
                }
            }

            Utils.SetPlayerModel("mp_m_freemode_01");
            Function.Call(Hash.SET_PED_DEFAULT_COMPONENT_VARIATION, Game.PlayerPed);
            Game.PlayerPed.Position = new Vector3(413.122f, -998.4281f, -99.40414f);
            Game.PlayerPed.IsPositionFrozen = true;
            API.RefreshInterior(API.GetInteriorAtCoords(413.122f, -998.4281f, -99.40414f));
        }

        private void MapStarted()
        {
            Utils.SetPlayerModel("mp_m_freemode_01");
            Function.Call(Hash.SET_PED_DEFAULT_COMPONENT_VARIATION, Game.PlayerPed);
            Game.PlayerPed.Position = new Vector3(413.122f, -998.4281f, -99.40414f);
            Game.PlayerPed.IsPositionFrozen = true;
            BaseScript.TriggerServerEvent("bl:SV_E:PlayerMapLoaded");
            API.RefreshInterior(API.GetInteriorAtCoords(413.122f, -998.4281f, -99.40414f));
        }

        public async Task OnTick()
        {
            await BaseScript.Delay(10);
            if(this.IsPlayerCustomizing)
            {
                PlayerList pl = new PlayerList();
                foreach (Player pp in pl)
                {
                    if (pp != Game.Player)
                    {
                        if (Function.Call<bool>(Hash.NETWORK_IS_PLAYER_CONCEALED, pp) == false)
                        {
                            Function.Call(Hash.NETWORK_CONCEAL_PLAYER, pp, true, true);
                        }
                    }
                }
            }

            API.SetParkedVehicleDensityMultiplierThisFrame(0.03f);
            API.SetVehicleDensityMultiplierThisFrame(0.03f);
            API.SetRandomVehicleDensityMultiplierThisFrame(0.03f);
            API.SetPedDensityMultiplierThisFrame(0.03f);
            API.SetScenarioPedDensityMultiplierThisFrame(0.03f, 0.03f);
        }
    }
}
