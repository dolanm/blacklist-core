﻿using blacklist_client.bases.menus.settings;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blacklist_client
{
    public class Config
    {
        public ProfileManagement_Settings PM_Settings = new ProfileManagement_Settings();

        public string[] SanitizedConfigValues = new string[]
        {
            "bl_db_host",
            "bl_db_user",
            "bl_db_name",
            "bl_db_pass"
        };

        public Dictionary<string, dynamic> LoadedConfig = new Dictionary<string, dynamic>();

        public void LoadConfig(string cfgJson)
        {
            if(cfgJson != "")
            {
                Dictionary<string, dynamic> ReceivedCfg = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(cfgJson);
                foreach(string s in this.SanitizedConfigValues)
                {
                    if(ReceivedCfg.ContainsKey(s))
                    {
                        ReceivedCfg.Remove(s);
                    }
                }
                this.LoadedConfig = ReceivedCfg;
            }
        }
    }
}
