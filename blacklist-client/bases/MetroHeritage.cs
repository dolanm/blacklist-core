﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blacklist_client.bases
{
    public class MetroHeritage
    {
        private Dictionary<string, int> FaceFeatures = new Dictionary<string, int>()
        {
            ["NoseWidth"] = 0,
            ["NoseHeight"] = 1,
            ["NoseLength"] = 2,
            ["NoseBoneHeight"] = 3,
            ["NoseLowering"] = 4,
            ["NoseTwist"] = 5,
            ["EyebrowHeight"] = 6,
            ["EyebrowForward"] = 7,
            ["CheekHeight"] = 8,
            ["CheekWidth"] = 9,
            ["CheekWidth2"] = 10,
            ["EyeWideness"] = 11,
            ["LipThickness"] = 12,
            ["JawWidth"] = 13,
            ["JawLength"] = 14,
            ["ChinLowering"] = 15,
            ["ChinLength"] = 16,
            ["ChinWidth"] = 17,
            ["ChinHole"] = 18,
            ["NeckThickness"] = 19
        };
        public int DadFace { get; set; } = 0;
        public int MomFace { get; set; } = 0;

        public float ParentMorph { get; set; } = 0.0f;

        public int Hair { get; set; } = 0;
        public int Eyebrow { get; set; } = 0;
        public int Beard { get; set; } = 0;
        public int HairColor { get; set; } = 0;
        public int EyeColor { get; set; } = 0;
        public int Ageing { get; set; } = 0;
        public int Makeup { get; set; } = 0;
        public int Blush { get; set; } = 0;
        public int Lipstick { get; set; } = 0;
        public int SunDamage { get; set; } = 0;
        public int Freckles { get; set; } = 0;

        public float BeardOpacity { get; set; } = 0.0f;
        public float EyebrowOpacity { get; set; } = 0.0f;

        public float AgeingOpacity { get; set; } = 0.0f;
        public float MakeupOpacity { get; set; } = 0.0f;
        public float BlushOpacity { get; set; } = 0.0f;
        public float LipstickOpacity { get; set; } = 0.0f;
        public float SunDamageOpacity { get; set; } = 0.0f;
        public float FreckleOpacity { get; set; } = 0.0f;
        public int BlushColor { get; set; } = 0;
        public int LipstickColor { get; set; } = 0;
        public int MakeupColor { get; set; } = 0;

        public float NoseWidth { get; set; } = 0.0f;
        public float NoseHeight { get; set; } = 0.0f;
        public float NoseLength { get; set; } = 0.0f;
        public float NoseBoneHeight { get; set; } = 0.0f;
        public float NoseLowering { get; set; } = 0.0f;
        public float NoseBoneTwist { get; set; } = 0.0f;

        public float EyebrowHeight { get; set; } = 0.0f;
        public float EyebrowWidth { get; set; } = 0.0f;

        public float CheekBoneHeight { get; set; } = 0.0f;
        public float CheekBoneWidth { get; set; } = 0.0f;
        public float CheekWidth { get; set; } = 0.0f;

        public float EyeWidth { get; set; } = 0.0f;

        public float LipThickness { get; set; } = 0.0f;

        public float JawWidth { get; set; } = 0.0f;
        public float JawLength { get; set; } = 0.0f;

        public float ChinLowering { get; set; } = 0.0f;
        public float ChinLength { get; set; } = 0.0f;
        public float ChinWidth { get; set; } = 0.0f;
        public float ChinHole { get; set; } = 0.0f;

        public float NeckThickness { get; set; } = 0.0f;

        public void Load(Ped p)
        {
            Function.Call(Hash.SET_PED_HEAD_BLEND_DATA, p, this.MomFace, this.DadFace, 0, this.MomFace, this.DadFace, 0, this.ParentMorph, this.ParentMorph, 0.0f, false);
            Function.Call(Hash.SET_PED_COMPONENT_VARIATION, p, 2, this.Hair, 0, 0);
            Function.Call(Hash.SET_PED_HEAD_OVERLAY, p, 1, this.Beard, this.BeardOpacity);
            Function.Call(Hash.SET_PED_HEAD_OVERLAY, p, 2, this.Eyebrow, this.EyebrowOpacity);
            Function.Call(Hash.SET_PED_HEAD_OVERLAY, p, 3, this.Ageing, this.AgeingOpacity);
            Function.Call(Hash.SET_PED_HEAD_OVERLAY, p, 7, this.SunDamage, this.SunDamageOpacity);
            Function.Call(Hash.SET_PED_HEAD_OVERLAY, p, 9, this.Freckles, this.FreckleOpacity);
            Function.Call(Hash.SET_PED_HEAD_OVERLAY, p, 4, this.Makeup, this.MakeupOpacity);
            Function.Call(Hash.SET_PED_HEAD_OVERLAY, p, 5, this.Blush, this.BlushOpacity);
            Function.Call(Hash.SET_PED_HEAD_OVERLAY, p, 8, this.Lipstick, this.LipstickOpacity);
            Function.Call(Hash._SET_PED_HAIR_COLOR, p, this.HairColor, this.HairColor);
            Function.Call(Hash._SET_PED_HEAD_OVERLAY_COLOR, p, 1, 1, this.HairColor, this.HairColor);
            Function.Call(Hash._SET_PED_HEAD_OVERLAY_COLOR, p, 2, 1, this.HairColor, this.HairColor);
            Function.Call(Hash._SET_PED_HEAD_OVERLAY_COLOR, p, 5, 2, this.BlushColor, this.BlushColor);
            Function.Call(Hash._SET_PED_HEAD_OVERLAY_COLOR, p, 8, 2, this.LipstickColor, this.LipstickColor);
            Function.Call(Hash._SET_PED_HEAD_OVERLAY_COLOR, p, 4, 2, this.MakeupColor, this.MakeupColor);
            Function.Call(Hash._SET_PED_EYE_COLOR, p, this.EyeColor);

            Function.Call(Hash._SET_PED_FACE_FEATURE, p, this.FaceFeatures["NoseWidth"], this.NoseWidth);
            Function.Call(Hash._SET_PED_FACE_FEATURE, p, this.FaceFeatures["NoseHeight"], this.NoseHeight);
            Function.Call(Hash._SET_PED_FACE_FEATURE, p, this.FaceFeatures["NoseLength"], this.NoseLength);
            Function.Call(Hash._SET_PED_FACE_FEATURE, p, this.FaceFeatures["NoseBoneHeight"], this.NoseBoneHeight);
            Function.Call(Hash._SET_PED_FACE_FEATURE, p, this.FaceFeatures["NoseLowering"], this.NoseLowering);
            Function.Call(Hash._SET_PED_FACE_FEATURE, p, this.FaceFeatures["NoseTwist"], this.NoseBoneTwist);
            Function.Call(Hash._SET_PED_FACE_FEATURE, p, this.FaceFeatures["EyebrowHeight"], this.EyebrowHeight);
            Function.Call(Hash._SET_PED_FACE_FEATURE, p, this.FaceFeatures["EyebrowForward"], this.EyebrowWidth);
            Function.Call(Hash._SET_PED_FACE_FEATURE, p, this.FaceFeatures["CheekHeight"], this.CheekBoneHeight);
            Function.Call(Hash._SET_PED_FACE_FEATURE, p, this.FaceFeatures["CheekWidth"], this.CheekBoneWidth);
            Function.Call(Hash._SET_PED_FACE_FEATURE, p, this.FaceFeatures["CheekWidth2"], this.CheekWidth);
            Function.Call(Hash._SET_PED_FACE_FEATURE, p, this.FaceFeatures["EyeWideness"], this.EyeWidth);
            Function.Call(Hash._SET_PED_FACE_FEATURE, p, this.FaceFeatures["LipThickness"], this.LipThickness);
            Function.Call(Hash._SET_PED_FACE_FEATURE, p, this.FaceFeatures["JawWidth"], this.JawWidth);
            Function.Call(Hash._SET_PED_FACE_FEATURE, p, this.FaceFeatures["JawLength"], this.JawLength);
            Function.Call(Hash._SET_PED_FACE_FEATURE, p, this.FaceFeatures["ChinLowering"], this.ChinLowering);
            Function.Call(Hash._SET_PED_FACE_FEATURE, p, this.FaceFeatures["ChinLength"], this.ChinLength);
            Function.Call(Hash._SET_PED_FACE_FEATURE, p, this.FaceFeatures["ChinWidth"], this.ChinWidth);
            Function.Call(Hash._SET_PED_FACE_FEATURE, p, this.FaceFeatures["ChinHole"], this.ChinHole);
            Function.Call(Hash._SET_PED_FACE_FEATURE, p, this.FaceFeatures["NeckThickness"], this.NeckThickness);
        }
    }
}
