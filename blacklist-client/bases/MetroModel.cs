﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blacklist_client.bases
{
    public class MetroModel
    {
        public string MODEL_PATH { get; set; }
        public Dictionary<int, int> BODYGROUP_DATA { get; set; }

        public MetroModel(string path, Dictionary<int, int> bgroups)

        {
            this.MODEL_PATH = path;
            this.BODYGROUP_DATA = bgroups;
        }
    }
}
