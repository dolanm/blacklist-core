﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blacklist_client.bases
{
    public class Outfit
    {
        public int Mask { get; set; } = 0;
        public int MaskTexture { get; set; } = 0;

        public int Arms { get; set; } = 0;
        public int Pants { get; set; } = 0;
        public int PantTexture { get; set; } = 0;

        public int Shoes { get; set; } = 0;
        public int ShoeTexture { get; set; } = 0;

        public int Holster { get; set; } = 0;
        public int HolsterTexture { get; set; } = 0;

        public int Armor { get; set; } = 0;
        public int ArmorTexture { get; set; } = 0;

        public int Shirt { get; set; } = 0;
        public int ShirtTexture { get; set; } = 0;

        public int Hat { get; set; } = -1;
        public int HatTexture { get; set; } = 0;

        public int Undershirt { get; set; } = 14;
        public int UndershirtTexture { get; set; } = 0;

        public int Earring { get; set; } = -1;

        public int Glasses { get; set; } = -1;
        public int GlassesTexture { get; set; } = 0;

        public int Watch { get; set; } = -1;
        public int WatchTexture { get; set; } = 0;

        public int Bracelet { get; set; } = -1;
        public int BraceletTexture { get; set; } = 0;

        public Outfit(int arm, int m, int mt, int p, int pt, int s, int st, int h, int ht, int a, int at, int sh, int sht, int ha, int hate, int ushirt, int ushirt_text, int ering, int gl, int gltext, int w, int wt, int b, int bt)
        {
            this.Arms = arm;

            this.Mask = m;
            this.MaskTexture = mt;

            this.Pants = p;
            this.PantTexture = pt;

            this.Shoes = s;
            this.ShoeTexture = st;

            this.Holster = h;
            this.HolsterTexture = ht;

            this.Armor = a;
            this.ArmorTexture = at;

            this.Shirt = sh;
            this.ShirtTexture = sht;

            this.Hat = ha;
            this.HatTexture = hate;

            this.Undershirt = ushirt;
            this.UndershirtTexture = ushirt_text;

            this.Earring = ering;

            this.Glasses = gl;
            this.GlassesTexture = gltext;

            this.Watch = w;
            this.WatchTexture = wt;

            this.Bracelet = b;
            this.BraceletTexture = bt;
        }

        public void Load(Ped p)
        {
            Function.Call(Hash.SET_PED_COMPONENT_VARIATION, p, 1, this.Mask, this.MaskTexture, 0);
            Function.Call(Hash.SET_PED_COMPONENT_VARIATION, p, 4, this.Pants, this.PantTexture, 0);
            Function.Call(Hash.SET_PED_COMPONENT_VARIATION, p, 6, this.Shoes, this.ShoeTexture, 0);
            Function.Call(Hash.SET_PED_COMPONENT_VARIATION, p, 7, this.Holster, this.HolsterTexture, 0);
            Function.Call(Hash.SET_PED_COMPONENT_VARIATION, p, 9, this.Armor, this.ArmorTexture, 0);
            Function.Call(Hash.SET_PED_COMPONENT_VARIATION, p, 11, this.Shirt, this.ShirtTexture, 0);
            Function.Call(Hash.SET_PED_COMPONENT_VARIATION, p, 3, this.Arms, 0, 0);
            Function.Call(Hash.SET_PED_COMPONENT_VARIATION, p, 8, this.Undershirt, this.UndershirtTexture, 0);
            if (this.Hat == -1)
            {
                Function.Call(Hash.SET_PED_PROP_INDEX, p, 0, -1, this.HatTexture, true);
                Function.Call(Hash.CLEAR_PED_PROP, p, 0);
            }
            else
            {
                Function.Call(Hash.SET_PED_PROP_INDEX, p, 0, this.Hat, this.HatTexture, true);
            }
            if (this.Glasses == -1)
            {
                Function.Call(Hash.SET_PED_PROP_INDEX, p, 1, -1, this.GlassesTexture, true);
                Function.Call(Hash.CLEAR_PED_PROP, p, 1);
            }
            else
            {
                Function.Call(Hash.SET_PED_PROP_INDEX, p, 1, this.Glasses, this.GlassesTexture, true);
            }
            if (this.Earring == -1)
            {
                Function.Call(Hash.SET_PED_PROP_INDEX, p, 2, -1, 0, true);
                Function.Call(Hash.CLEAR_PED_PROP, p, 2);
            }
            else
            {
                Function.Call(Hash.SET_PED_PROP_INDEX, p, 2, this.Earring, 0, true);
            }
            if (this.Watch == -1)
            {
                Function.Call(Hash.SET_PED_PROP_INDEX, p, 6, -1, 0, true);
                Function.Call(Hash.CLEAR_PED_PROP, p, 6);
            }
            else
            {
                Function.Call(Hash.SET_PED_PROP_INDEX, p, 6, this.Watch, this.WatchTexture, true);
            }
            if (this.Bracelet == -1)
            {
                Function.Call(Hash.SET_PED_PROP_INDEX, p, 7, -1, 0, true);
                Function.Call(Hash.CLEAR_PED_PROP, p, 7);
            }
            else
            {
                Function.Call(Hash.SET_PED_PROP_INDEX, p, 7, this.Bracelet, this.BraceletTexture, true);
            }
        }
    }
}
