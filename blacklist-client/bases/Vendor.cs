﻿using blacklist_client.bases.factions;
using blacklist_client.bases.items;
using CitizenFX.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blacklist_client.bases
{
    public class Vendor
    {
        public int id { get; set; } = -1;
        public string title { get; set; } = "";
        public int blipId { get; set; } = 0;
        public Vector3 location { get; set; } = new Vector3(0.0f, 0.0f, 0.0f);
        public float heading { get; set; } = 0.0f;
        public string model { get; set; } = "";
        public bool isHidden { get; set; } = false;
        public List<Faction> factions { get; set; } = new List<Faction>();
        public List<MetroClass> classes { get; set; } = new List<MetroClass>();
        public List<VendorItem> items { get; set; } = new List<VendorItem>();
        public bool useMoney { get; set; } = false;
        public int storedMoney { get; set; } = 0;
    }
}
