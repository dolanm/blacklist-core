﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blacklist_client.bases.factions
{
    public class Faction
    {
        public string FactionName { get; set; }
        public List<MetroClass> Classes = new List<MetroClass>();

        public bool IsDefault { get; set; }

        public Faction(string n, bool def = true)
        {
            this.FactionName = n;
            this.IsDefault = def;
        }

        public void AddClass(MetroClass c)
        {
            this.Classes.Add(c);
        }
    }
}
