﻿using blacklist_client.utils;
using CitizenFX.Core;
using CitizenFX.Core.Native;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blacklist_client.bases.items
{
    public class DroppedItem
    {
        public string ItemName { get; } = "";
        public int ItemID { get; set; } = 0;
        public int ItemAmount { get; set; } = 0;
        public Vector3 Position { get; set; }
        private Prop WorldProp { get; set; }

        public DroppedItem(int id, int amount, Vector3 pos)
        {
            this.ItemID = id;

            if(Client.INSTANCE.ItemManager.GetItemByID(id) != null)
            {
                BlItem tempItem = Client.INSTANCE.ItemManager.GetItemByID(id);
                this.ItemName = tempItem.ItemName;
            }

            this.ItemAmount = amount;
            this.Position = pos;
        }

        public async void SpawnInWorld()
        {
            if(this.Position != null)
            {
                this.WorldProp = await EntityUtils.CreateProp(new Model(API.GetHashKey("prop_paper_bag_01")), new Vector3(this.Position.X, this.Position.Y, this.Position.Z - 1.0f), false, false, false);
            }
        }

        public void DeleteWorldProp()
        {
            if(this.WorldProp != null && this.WorldProp.Exists())
            {
                this.WorldProp.Delete();
                this.WorldProp = null;
            }
        }
    }
}
