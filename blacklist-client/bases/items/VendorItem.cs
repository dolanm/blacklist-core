﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blacklist_client.bases.items
{
    public class VendorItem
    {
        public int baseId { get; set; } = 0;
        public string name { get; set; } = "";
        public int mode { get; set; } = 0;
        public int buyPrice { get; set; } = 0;
        public int sellPrice { get; set; } = 0;
        public int currentStock { get; set; } = 0;
        public int maxStock { get; set; } = 0;
        public bool useStock { get; set; } = false;
    }
}
