﻿using blacklist_client.utils;
using CitizenFX.Core;
using CitizenFX.Core.Native;
using MenuAPI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blacklist_client.bases.menus
{
    public class ProfileCustomization
    {
        public int[] ParentFaces = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45 };

        public Menu m;
        public Menu heritageMenu;
        public Menu boneMenu;
        public Menu accessoryMenu;

        public MetroHeritage TempHeritage;
        public Outfit TempOutfit;

        private Dictionary<string, int> FaceFeatures = new Dictionary<string, int>()
        {
            ["NoseWidth"] = 0,
            ["NoseHeight"] = 1,
            ["NoseLength"] = 2,
            ["NoseBoneHeight"] = 3,
            ["NoseLowering"] = 4,
            ["NoseTwist"] = 5,
            ["EyebrowHeight"] = 6,
            ["EyebrowForward"] = 7,
            ["CheekHeight"] = 8,
            ["CheekWidth"] = 9,
            ["CheekWidth2"] = 10,
            ["EyeWideness"] = 11,
            ["LipThickness"] = 12,
            ["JawWidth"] = 13,
            ["JawLength"] = 14,
            ["ChinLowering"] = 15,
            ["ChinLength"] = 16,
            ["ChinWidth"] = 17,
            ["ChinHole"] = 18,
            ["NeckThickness"] = 19
        };

        private Dictionary<string, Bone> BoneList = new Dictionary<string, Bone>()
        {
            ["Head"] = Bone.SKEL_Head,
            ["Torso"] = Bone.SKEL_Spine3,
            ["Legs"] = Bone.SKEL_R_Calf,
            ["Feet"] = Bone.SKEL_R_Foot,
            ["Arms"] = Bone.SKEL_L_UpperArm
        };

        public void ToggleMenuVisibility()
        {
            m.Visible = !m.Visible;
        }

        public void ResetMenuItems()
        {
            this.boneMenu.ClearMenuItems();
            this.heritageMenu.ClearMenuItems();
            this.m.ClearMenuItems();

            this.TempHeritage = new MetroHeritage();
            this.TempOutfit = new Outfit(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, -1, -1, 0, -1, 0, -1, 0);

            MenuController.DisableBackButton = true;

            Client.INSTANCE.IsPlayerCustomizing = true;

            #region Heritage Menu
            List<string> DadHeadList = new List<string>();
            for (int i = 0; i < this.ParentFaces.Length; i++)
            {
                DadHeadList.Add($"Head #{i}");
            }

            List<string> MomHeadList = new List<string>();
            for (int i = 0; i < this.ParentFaces.Length; i++)
            {
                MomHeadList.Add($"Head #{i}");
            }

            MenuListItem DadHeadListItem = new MenuListItem("Father", DadHeadList, 0, "Desired head for your father");
            MenuListItem MomHeadListItem = new MenuListItem("Mother", MomHeadList, 0, "Desired head for your mother");
            this.heritageMenu.AddMenuItem(DadHeadListItem);
            this.heritageMenu.AddMenuItem(MomHeadListItem);

            MenuSliderItem ParentMix = new MenuSliderItem("Parent Resemblance", 0, 9, 4)
            {
                BarColor = Color.FromArgb(255, 44, 102, 184),
                BackgroundColor = Color.FromArgb(255, 0, 43, 100),
                SliderLeftIcon = MenuItem.Icon.FEMALE,
                SliderRightIcon = MenuItem.Icon.MALE,
                ShowDivider = true
            };
            this.heritageMenu.AddMenuItem(ParentMix);

            //_SET_PED_EYE_COLOR
            List<string> ColorList = new List<string>();
            for (var i = 0; i < 64; i++)
            {
                ColorList.Add($"Color #{i}");
            }
            MenuListItem EyeColorListItem = new MenuListItem("Eye Color", ColorList, 0, "Desired eye color for your character");

            this.heritageMenu.AddMenuItem(EyeColorListItem);

            int MaxDrawableHair = Function.Call<int>(Hash.GET_NUMBER_OF_PED_DRAWABLE_VARIATIONS, Game.PlayerPed, 2);
            List<string> HairList = new List<string>();
            for (int i = 0; i < MaxDrawableHair; i++)
            {
                HairList.Add($"Hair #{i}");
            }
            MenuListItem HairListItem = new MenuListItem("Hair", HairList, 0, "Desired hair for your character");
            this.heritageMenu.AddMenuItem(HairListItem);

            int MaxDrawableEyebrows = Function.Call<int>(Hash._GET_NUM_HEAD_OVERLAY_VALUES, 2);
            List<string> BrowList = new List<string>();
            for (int i = 0; i < MaxDrawableEyebrows; i++)
            {
                BrowList.Add($"Eyebrows #{i}");
            }
            MenuListItem BrowListItem = new MenuListItem("Eyebrows", BrowList, 0, "Desired eyebrows for your character");
            this.heritageMenu.AddMenuItem(BrowListItem);

            int MaxDrawableBeards = Function.Call<int>(Hash._GET_NUM_HEAD_OVERLAY_VALUES, 1);
            List<string> BeardList = new List<string>();
            for (int i = 0; i < MaxDrawableBeards; i++)
            {
                BeardList.Add($"Beard #{i}");
            }
            MenuListItem BeardListItem = new MenuListItem("Beard", BeardList, 0, "Desired facial hair for your character");
            this.heritageMenu.AddMenuItem(BeardListItem);

            MenuSliderItem BeardOpacity = new MenuSliderItem("Beard Opacity", 0, 9, 4)
            {
                BarColor = Color.FromArgb(255, 44, 102, 184),
                BackgroundColor = Color.FromArgb(255, 0, 43, 100),
                ShowDivider = true
            };
            this.heritageMenu.AddMenuItem(BeardOpacity);

            MenuSliderItem EyebrowOpacityItem = new MenuSliderItem("Eyebrow Opacity", 0, 9, 4)
            {
                BarColor = Color.FromArgb(255, 44, 102, 184),
                BackgroundColor = Color.FromArgb(255, 0, 43, 100),
                ShowDivider = true
            };
            this.heritageMenu.AddMenuItem(EyebrowOpacityItem);

            MenuListItem HairColorListItem = new MenuListItem("Hair Color", ColorList, 0, "Desired hair color for your character")
            {
                ShowColorPanel = true
            };
            this.heritageMenu.AddMenuItem(HairColorListItem);

            int MaxDrawableAgeing = Function.Call<int>(Hash._GET_NUM_HEAD_OVERLAY_VALUES, 3);
            List<string> AgeingList = new List<string>();
            for (int i = 0; i < MaxDrawableAgeing; i++)
            {
                AgeingList.Add($"Ageing #{i}");
            }
            MenuListItem AgeingListItem = new MenuListItem("Ageing", AgeingList, 0, "Desired ageing for your character");
            this.heritageMenu.AddMenuItem(AgeingListItem);

            int MaxDrawableSunDamage = Function.Call<int>(Hash._GET_NUM_HEAD_OVERLAY_VALUES, 7);
            List<string> SunDamageList = new List<string>();
            for (int i = 0; i < MaxDrawableSunDamage; i++)
            {
                SunDamageList.Add($"Sun Damage #{i}");
            }
            MenuListItem SunDamageListItem = new MenuListItem("Sun Damage", SunDamageList, 0, "Desired sun damage for your character");
            this.heritageMenu.AddMenuItem(SunDamageListItem);

            int MaxDrawableFreckles = Function.Call<int>(Hash._GET_NUM_HEAD_OVERLAY_VALUES, 9);
            List<string> FreckleList = new List<string>();
            for (int i = 0; i < MaxDrawableFreckles; i++)
            {
                FreckleList.Add($"Freckles #{i}");
            }
            MenuListItem FreckleListItem = new MenuListItem("Freckles", FreckleList, 0, "Desired freckles for your character");
            this.heritageMenu.AddMenuItem(FreckleListItem);

            MenuSliderItem AgeingOpacityItem = new MenuSliderItem("Ageing Opacity", 0, 9, 4)
            {
                BarColor = Color.FromArgb(255, 44, 102, 184),
                BackgroundColor = Color.FromArgb(255, 0, 43, 100),
                ShowDivider = true
            };
            this.heritageMenu.AddMenuItem(AgeingOpacityItem);

            MenuSliderItem SunDamageOpacityItem = new MenuSliderItem("Sun Damage Opacity", 0, 9, 4)
            {
                BarColor = Color.FromArgb(255, 44, 102, 184),
                BackgroundColor = Color.FromArgb(255, 0, 43, 100),
                ShowDivider = true
            };
            this.heritageMenu.AddMenuItem(SunDamageOpacityItem);

            MenuSliderItem FreckleOpacityItem = new MenuSliderItem("Freckle Opacity", 0, 9, 4)
            {
                BarColor = Color.FromArgb(255, 44, 102, 184),
                BackgroundColor = Color.FromArgb(255, 0, 43, 100),
                ShowDivider = true
            };
            this.heritageMenu.AddMenuItem(FreckleOpacityItem);

            MenuItem BackItem = new MenuItem("Back", "Return to the previous menu")
            {
                Label = "→→→"
            };
            this.heritageMenu.AddMenuItem(BackItem);
            #endregion

            #region Bone Menu
            MenuSliderItem NoseWidthSlider = new MenuSliderItem("Nose Width", 0, 9, 4)
            {
                BarColor = Color.FromArgb(255, 44, 102, 184),
                BackgroundColor = Color.FromArgb(255, 0, 43, 100),
                ShowDivider = true
            };
            MenuSliderItem NoseHeightSlider = new MenuSliderItem("Nose Height", 0, 9, 4)
            {
                BarColor = Color.FromArgb(255, 44, 102, 184),
                BackgroundColor = Color.FromArgb(255, 0, 43, 100),
                ShowDivider = true
            };
            MenuSliderItem NoseLengthSlider = new MenuSliderItem("Nose Length", 0, 9, 4)
            {
                BarColor = Color.FromArgb(255, 44, 102, 184),
                BackgroundColor = Color.FromArgb(255, 0, 43, 100),
                ShowDivider = true
            };
            MenuSliderItem NoseBoneHeightSlider = new MenuSliderItem("Nose Bone Height", 0, 9, 4)
            {
                BarColor = Color.FromArgb(255, 44, 102, 184),
                BackgroundColor = Color.FromArgb(255, 0, 43, 100),
                ShowDivider = true
            };
            MenuSliderItem NoseLoweringSlider = new MenuSliderItem("Nose Lowering", 0, 9, 4)
            {
                BarColor = Color.FromArgb(255, 44, 102, 184),
                BackgroundColor = Color.FromArgb(255, 0, 43, 100),
                ShowDivider = true
            };
            MenuSliderItem NoseBoneTwistSlider = new MenuSliderItem("Nose Twist", 0, 9, 4)
            {
                BarColor = Color.FromArgb(255, 44, 102, 184),
                BackgroundColor = Color.FromArgb(255, 0, 43, 100),
                ShowDivider = true
            };
            MenuSliderItem EyebrowHeightSlider = new MenuSliderItem("Eyebrow Height", 0, 9, 4)
            {
                BarColor = Color.FromArgb(255, 44, 102, 184),
                BackgroundColor = Color.FromArgb(255, 0, 43, 100),
                ShowDivider = true
            };
            MenuSliderItem EyebrowWidthSlider = new MenuSliderItem("Eyebrow Width", 0, 9, 4)
            {
                BarColor = Color.FromArgb(255, 44, 102, 184),
                BackgroundColor = Color.FromArgb(255, 0, 43, 100),
                ShowDivider = true
            };
            MenuSliderItem CheekBoneHeight = new MenuSliderItem("Cheek Bone Height", 0, 9, 4)
            {
                BarColor = Color.FromArgb(255, 44, 102, 184),
                BackgroundColor = Color.FromArgb(255, 0, 43, 100),
                ShowDivider = true
            };
            MenuSliderItem CheekBoneWidth = new MenuSliderItem("Cheek Bone Width", 0, 9, 4)
            {
                BarColor = Color.FromArgb(255, 44, 102, 184),
                BackgroundColor = Color.FromArgb(255, 0, 43, 100),
                ShowDivider = true
            };
            MenuSliderItem CheekWidthSlider = new MenuSliderItem("Cheek Width", 0, 9, 4)
            {
                BarColor = Color.FromArgb(255, 44, 102, 184),
                BackgroundColor = Color.FromArgb(255, 0, 43, 100),
                ShowDivider = true
            };
            MenuSliderItem EyeWidthSlider = new MenuSliderItem("Eye Width", 0, 9, 4)
            {
                BarColor = Color.FromArgb(255, 44, 102, 184),
                BackgroundColor = Color.FromArgb(255, 0, 43, 100),
                ShowDivider = true
            };
            MenuSliderItem LipThicknessSlider = new MenuSliderItem("Lip Thickness", 0, 9, 4)
            {
                BarColor = Color.FromArgb(255, 44, 102, 184),
                BackgroundColor = Color.FromArgb(255, 0, 43, 100),
                ShowDivider = true
            };
            MenuSliderItem JawWidthSlider = new MenuSliderItem("Jaw Width", 0, 9, 4)
            {
                BarColor = Color.FromArgb(255, 44, 102, 184),
                BackgroundColor = Color.FromArgb(255, 0, 43, 100),
                ShowDivider = true
            };
            MenuSliderItem JawLengthSlider = new MenuSliderItem("Jaw Length", 0, 9, 4)
            {
                BarColor = Color.FromArgb(255, 44, 102, 184),
                BackgroundColor = Color.FromArgb(255, 0, 43, 100),
                ShowDivider = true
            };
            MenuSliderItem ChinLoweringSlider = new MenuSliderItem("Chin Lowering", 0, 9, 4)
            {
                BarColor = Color.FromArgb(255, 44, 102, 184),
                BackgroundColor = Color.FromArgb(255, 0, 43, 100),
                ShowDivider = true
            };
            MenuSliderItem ChinLengthSlider = new MenuSliderItem("Chin Length", 0, 9, 4)
            {
                BarColor = Color.FromArgb(255, 44, 102, 184),
                BackgroundColor = Color.FromArgb(255, 0, 43, 100),
                ShowDivider = true
            };
            MenuSliderItem ChinWidthSlider = new MenuSliderItem("Chin Width", 0, 9, 4)
            {
                BarColor = Color.FromArgb(255, 44, 102, 184),
                BackgroundColor = Color.FromArgb(255, 0, 43, 100),
                ShowDivider = true
            };
            MenuSliderItem ChinHoleSlider = new MenuSliderItem("Chin Depth", 0, 9, 4)
            {
                BarColor = Color.FromArgb(255, 44, 102, 184),
                BackgroundColor = Color.FromArgb(255, 0, 43, 100),
                ShowDivider = true
            };
            MenuSliderItem NeckThicknessSlider = new MenuSliderItem("Neck Thickness", 0, 9, 4)
            {
                BarColor = Color.FromArgb(255, 44, 102, 184),
                BackgroundColor = Color.FromArgb(255, 0, 43, 100),
                ShowDivider = true
            };
            MenuItem BackButton = new MenuItem("Back", "Return to the previous menu");

            this.boneMenu.AddMenuItem(NoseWidthSlider);
            this.boneMenu.AddMenuItem(NoseHeightSlider);
            this.boneMenu.AddMenuItem(NoseLengthSlider);
            this.boneMenu.AddMenuItem(NoseBoneHeightSlider);
            this.boneMenu.AddMenuItem(NoseLoweringSlider);
            this.boneMenu.AddMenuItem(NoseBoneTwistSlider);
            this.boneMenu.AddMenuItem(EyebrowHeightSlider);
            this.boneMenu.AddMenuItem(EyebrowWidthSlider);
            this.boneMenu.AddMenuItem(CheekBoneHeight);
            this.boneMenu.AddMenuItem(CheekBoneWidth);
            this.boneMenu.AddMenuItem(CheekWidthSlider);
            this.boneMenu.AddMenuItem(EyeWidthSlider);
            this.boneMenu.AddMenuItem(LipThicknessSlider);
            this.boneMenu.AddMenuItem(JawWidthSlider);
            this.boneMenu.AddMenuItem(JawLengthSlider);
            this.boneMenu.AddMenuItem(ChinLoweringSlider);
            this.boneMenu.AddMenuItem(ChinLengthSlider);
            this.boneMenu.AddMenuItem(ChinWidthSlider);
            this.boneMenu.AddMenuItem(ChinHoleSlider);
            this.boneMenu.AddMenuItem(NeckThicknessSlider);
            this.boneMenu.AddMenuItem(BackButton);
            #endregion

            MenuItem heritageBUtton = new MenuItem("Customize Heritage", "")
            {
                Label = "→→→"
            };
            MenuItem boneButton = new MenuItem("Customize Bones", "")
            {
                Label = "→→→"
            };
            MenuItem accButton = new MenuItem("Customize Accessories", "")
            {
                Label = "→→→"
            };
            this.m.AddMenuItem(heritageBUtton);
            this.m.AddMenuItem(boneButton);
            this.m.AddMenuItem(accButton);
            MenuController.BindMenuItem(this.m, this.heritageMenu, heritageBUtton);
            MenuController.BindMenuItem(this.m, this.boneMenu, boneButton);

            #region Main Menu
            int MaxDrawableArms = Function.Call<int>(Hash.GET_NUMBER_OF_PED_DRAWABLE_VARIATIONS, Game.PlayerPed, 3);
            int MaxDrawableShirt = Function.Call<int>(Hash.GET_NUMBER_OF_PED_DRAWABLE_VARIATIONS, Game.PlayerPed, 11);
            int MaxDrawableUndershirt = Function.Call<int>(Hash.GET_NUMBER_OF_PED_DRAWABLE_VARIATIONS, Game.PlayerPed, 8);
            int MaxDrawablePants = Function.Call<int>(Hash.GET_NUMBER_OF_PED_DRAWABLE_VARIATIONS, Game.PlayerPed, 4);
            int MaxDrawableShoes = Function.Call<int>(Hash.GET_NUMBER_OF_PED_DRAWABLE_VARIATIONS, Game.PlayerPed, 6);
            int MaxDrawableAccessories = Function.Call<int>(Hash.GET_NUMBER_OF_PED_DRAWABLE_VARIATIONS, Game.PlayerPed, 7);


            List<string> ArmList = new List<string>();
            for (int i = 0; i < MaxDrawableArms; i++)
            {
                ArmList.Add($"Arms #{i}");
            }
            List<string> ShirtList = new List<string>();
            for (int i = 0; i < MaxDrawableShirt; i++)
            {
                ShirtList.Add($"Shirt #{i}");
            }
            List<string> UndershirtList = new List<string>();
            for (int i = 0; i < MaxDrawableUndershirt; i++)
            {
                UndershirtList.Add($"Undershirt #{i}");
            }
            List<string> PantList = new List<string>();
            for (int i = 0; i < MaxDrawablePants; i++)
            {
                PantList.Add($"Pants #{i}");
            }
            List<string> ShoeList = new List<string>();
            for (int i = 0; i < MaxDrawableShoes; i++)
            {
                ShoeList.Add($"Shoes #{i}");
            }
            List<string> AccessoryList = new List<string>();
            for (int i = 0; i < MaxDrawableAccessories; i++)
            {
                AccessoryList.Add($"Accessory #{i}");
            }

            MenuListItem ArmListItem = new MenuListItem("Arms", ArmList, 0, "Desired arms for your character");
            MenuListItem ShirtListItem = new MenuListItem("Shirt", ShirtList, 0, "Desired shirt for your character");
            MenuListItem UndershirtListItem = new MenuListItem("Undershirt", UndershirtList, 0, "Desired undershirt for your character");
            MenuListItem PantListItem = new MenuListItem("Pants", PantList, 0, "Desired pants for your character");
            MenuListItem ShoeListItem = new MenuListItem("Shoes", ShoeList, 0, "Desired shoes for your character");
            MenuListItem AccessoryListItem = new MenuListItem("Accessory", AccessoryList, 0, "Desired accessory for your character");
            /*
            MenuListItem HatListItem = new MenuListItem("Accessory", AccessoryList, 0, "Desired accessory for your character");
            MenuListItem AccessoryListItem = new MenuListItem("Accessory", AccessoryList, 0, "Desired accessory for your character");
            MenuListItem AccessoryListItem = new MenuListItem("Accessory", AccessoryList, 0, "Desired accessory for your character");*/

            this.m.AddMenuItem(ArmListItem);
            this.m.AddMenuItem(ShirtListItem);
            this.m.AddMenuItem(UndershirtListItem);
            this.m.AddMenuItem(PantListItem);
            this.m.AddMenuItem(ShoeListItem);
            this.m.AddMenuItem(AccessoryListItem);

            MenuController.BindMenuItem(this.m, this.accessoryMenu, accButton);

            int MaxDrawableHats = Function.Call<int>(Hash.GET_NUMBER_OF_PED_PROP_DRAWABLE_VARIATIONS, Game.PlayerPed, 0);
            int MaxDrawableGlasses = Function.Call<int>(Hash.GET_NUMBER_OF_PED_PROP_DRAWABLE_VARIATIONS, Game.PlayerPed, 1);
            int MaxDrawableMasks = Function.Call<int>(Hash.GET_NUMBER_OF_PED_DRAWABLE_VARIATIONS, Game.PlayerPed, 1);
            int MaxDrawableEarring = Function.Call<int>(Hash.GET_NUMBER_OF_PED_PROP_DRAWABLE_VARIATIONS, Game.PlayerPed, 2);
            int MaxDrawableWatch = Function.Call<int>(Hash.GET_NUMBER_OF_PED_PROP_DRAWABLE_VARIATIONS, Game.PlayerPed, 6);
            int MaxDrawableBracelet = Function.Call<int>(Hash.GET_NUMBER_OF_PED_PROP_DRAWABLE_VARIATIONS, Game.PlayerPed, 7);
            int MaxDrawableMakeup = Function.Call<int>(Hash._GET_NUM_HEAD_OVERLAY_VALUES, 4);
            int MaxDrawableBlush = Function.Call<int>(Hash._GET_NUM_HEAD_OVERLAY_VALUES, 5);
            int MaxDrawableLipstick = Function.Call<int>(Hash._GET_NUM_HEAD_OVERLAY_VALUES, 8);

            List<string> HatList = new List<string>();
            HatList.Add("Hat #-1");
            for (int i = 0; i < MaxDrawableHats; i++)
            {
                HatList.Add($"Hat #{i}");
            }
            List<string> GlassesList = new List<string>();
            for (int i = 0; i < MaxDrawableGlasses; i++)
            {
                GlassesList.Add($"Glasses #{i}");
            }
            List<string> MaskList = new List<string>();
            for (int i = 0; i < MaxDrawableMasks; i++)
            {
                MaskList.Add($"Mask #{i}");
            }
            List<string> EarringList = new List<string>();
            EarringList.Add("Earring #-1");
            for (int i = 0; i < MaxDrawableEarring; i++)
            {
                EarringList.Add($"Earring #{i}");
            }
            List<string> WatchList = new List<string>();
            WatchList.Add("Watch #-1");
            for (int i = 0; i < MaxDrawableWatch; i++)
            {
                WatchList.Add($"Watch #{i}");
            }
            List<string> BraceletList = new List<string>();
            BraceletList.Add("Bracelet #-1");
            for (int i = 0; i < MaxDrawableBracelet; i++)
            {
                BraceletList.Add($"Bracelet #{i}");
            }
            List<string> MakeupList = new List<string>();
            for (int i = 0; i < MaxDrawableMakeup; i++)
            {
                MakeupList.Add($"Makeup #{i}");
            }
            MenuListItem MakeupListItem = new MenuListItem("Makeup", MakeupList, 0, "Desired makeup for your character");
            List<string> BlushList = new List<string>();
            for (int i = 0; i < MaxDrawableBlush; i++)
            {
                BlushList.Add($"Blush #{i}");
            }
            MenuListItem BlushListItem = new MenuListItem("Blush", BlushList, 0, "Desired blush for your character");
            List<string> LipstickList = new List<string>();
            for (int i = 0; i < MaxDrawableLipstick; i++)
            {
                LipstickList.Add($"Lipstick #{i}");
            }
            MenuListItem LipstickListItem = new MenuListItem("Lipstick", LipstickList, 0, "Desired lipstick for your character");

            MenuSliderItem MakeupOpacityItem = new MenuSliderItem("Makeup Opacity", 0, 9, 4)
            {
                BarColor = Color.FromArgb(255, 44, 102, 184),
                BackgroundColor = Color.FromArgb(255, 0, 43, 100),
                ShowDivider = true
            };

            MenuSliderItem BlushOpacityItem = new MenuSliderItem("Blush Opacity", 0, 9, 4)
            {
                BarColor = Color.FromArgb(255, 44, 102, 184),
                BackgroundColor = Color.FromArgb(255, 0, 43, 100),
                ShowDivider = true
            };

            MenuSliderItem LipstickOpacityItem = new MenuSliderItem("Lipstick Opacity", 0, 9, 4)
            {
                BarColor = Color.FromArgb(255, 44, 102, 184),
                BackgroundColor = Color.FromArgb(255, 0, 43, 100),
                ShowDivider = true
            };

            MenuListItem BlushColorItem = new MenuListItem("Blush Color", ColorList, 0, "Desired blush color for your character");
            MenuListItem LipstickColorItem = new MenuListItem("Lipstick Color", ColorList, 0, "Desired lipstick color for your character");
            MenuListItem MakeupColorItem = new MenuListItem("Makeup Color", ColorList, 0, "Desired makeup color for your character");

            MenuListItem HatListItem = new MenuListItem("Hat", HatList, 0, "Desired hat for your character");
            MenuListItem GlassesListItem = new MenuListItem("Glasses", GlassesList, 0, "Desired glasses for your character");
            MenuListItem MaskListItem = new MenuListItem("Mask", MaskList, 0, "Desired mask for your character");
            MenuListItem EarringListItem = new MenuListItem("Earring", EarringList, 0, "Desired earring for your character");
            MenuListItem WatchListItem = new MenuListItem("Watch", WatchList, 0, "Desired watch for your character");
            MenuListItem BraceletListItem = new MenuListItem("Bracelet", BraceletList, 0, "Desired bracelet for your character");
            this.accessoryMenu.AddMenuItem(HatListItem);
            this.accessoryMenu.AddMenuItem(GlassesListItem);
            this.accessoryMenu.AddMenuItem(MaskListItem);
            this.accessoryMenu.AddMenuItem(EarringListItem);
            this.accessoryMenu.AddMenuItem(WatchListItem);
            this.accessoryMenu.AddMenuItem(BraceletListItem);
            this.accessoryMenu.AddMenuItem(MakeupListItem);
            this.accessoryMenu.AddMenuItem(BlushListItem);
            this.accessoryMenu.AddMenuItem(LipstickListItem);
            this.accessoryMenu.AddMenuItem(MakeupOpacityItem);
            this.accessoryMenu.AddMenuItem(BlushOpacityItem);
            this.accessoryMenu.AddMenuItem(LipstickOpacityItem);
            this.accessoryMenu.AddMenuItem(BlushColorItem);
            this.accessoryMenu.AddMenuItem(LipstickColorItem);
            this.accessoryMenu.AddMenuItem(MakeupColorItem);

            MenuItem BackButton2 = new MenuItem("Back", "Return to the previous menu");
            this.accessoryMenu.AddMenuItem(BackButton2);


            MenuItem SaveItem = new MenuItem("Save Appearance", "Save your character's appearance. (THIS IS FINAL)")
            {
                Label = "→→→"
            };
            this.m.AddMenuItem(SaveItem);
            #endregion
        }

        public ProfileCustomization()
        {
            if (this.m == null)
                this.m = new Menu("Customize Character", "");
            if (this.heritageMenu == null)
                this.heritageMenu = new Menu("Customize Heritage", "");
            if (this.boneMenu == null)
                this.boneMenu = new Menu("Customize Bones", "");
            if (this.accessoryMenu == null)
                this.accessoryMenu = new Menu("Customize Accessories", "");

            MenuController.MenuAlignment = MenuController.MenuAlignmentOption.Left;
            MenuController.AddMenu(this.m);
            MenuController.AddSubmenu(this.m, this.heritageMenu);
            MenuController.AddSubmenu(this.m, this.boneMenu);
            MenuController.AddSubmenu(this.m, this.accessoryMenu);

            #region Main Menu Events
            this.m.OnItemSelect += (_menu, _item, _index) =>
            {
                if (_item.Text == "Save Appearance")
                {
                    if (this.TempOutfit.ShirtTexture != 0)
                        this.TempOutfit.ShirtTexture = this.TempOutfit.ShirtTexture - 1;
                    if (this.TempOutfit.UndershirtTexture != 0)
                        this.TempOutfit.UndershirtTexture = this.TempOutfit.UndershirtTexture - 1;
                    if (this.TempOutfit.PantTexture != 0)
                        this.TempOutfit.PantTexture = this.TempOutfit.PantTexture - 1;
                    if (this.TempOutfit.ShoeTexture != 0)
                        this.TempOutfit.ShoeTexture = this.TempOutfit.ShoeTexture - 1;
                    if (this.TempOutfit.HolsterTexture != 0)
                        this.TempOutfit.HolsterTexture = this.TempOutfit.HolsterTexture - 1;
                    if (this.TempOutfit.GlassesTexture != 0)
                        this.TempOutfit.Glasses = this.TempOutfit.GlassesTexture - 1;
                    if (this.TempOutfit.WatchTexture != 0)
                        this.TempOutfit.WatchTexture = this.TempOutfit.WatchTexture - 1;
                    if (this.TempOutfit.BraceletTexture != 0)
                        this.TempOutfit.BraceletTexture = this.TempOutfit.BraceletTexture - 1;
                    //Save character data
                    BaseScript.TriggerServerEvent("bl:SV_E:Character:SaveLooks", Game.Player.ServerId, JsonConvert.SerializeObject(this.TempHeritage), JsonConvert.SerializeObject(this.TempOutfit));
                    MenuController.DisableBackButton = false;
                    MenuController.CloseAllMenus();
                    PlayerList pl = new PlayerList();
                    foreach (Player pp in pl)
                    {
                        if (pp != Game.Player)
                        {
                            if (Function.Call<bool>(Hash.NETWORK_IS_PLAYER_CONCEALED, pp) == true)
                            {
                                Function.Call(Hash.NETWORK_CONCEAL_PLAYER, pp, false, false);
                            }
                        }
                    }
                    Utils.GetSelectedProfile().Heritage = this.TempHeritage;
                    Utils.GetSelectedProfile().Clothing = this.TempOutfit;
                    Client.INSTANCE.LoadProfileCharacter();
                }
                else if (_item.Text == "Customize Heritage" || _item.Text == "Customize Bones" || _item.Text == "Customize Accessories")
                {
                    Client.INSTANCE.BASE_CAMERA.Position = new Vector3(403f, -997f - 0.3f, -98.25f);
                    Client.INSTANCE.BASE_CAMERA.PointAt(Game.PlayerPed.Bones[31086]);
                }
            };

            this.m.OnListIndexChange += (_menu, _listItem, _oldIndex, _newIndex, _itemIndex) =>
            {
                if (_listItem.Text == "Arms")
                {
                    Function.Call(Hash.SET_PED_COMPONENT_VARIATION, Game.PlayerPed, 3, _newIndex, 0, 0);
                    this.TempOutfit.Arms = _newIndex;
                }
                else if (_listItem.Text == "Shirt")
                {
                    this.TempOutfit.ShirtTexture = 0;
                    Function.Call(Hash.SET_PED_COMPONENT_VARIATION, Game.PlayerPed, 11, _newIndex, this.TempOutfit.ShirtTexture, 0);
                    this.TempOutfit.Shirt = _newIndex;
                }
                else if (_listItem.Text == "Undershirt")
                {
                    this.TempOutfit.UndershirtTexture = 0;
                    Function.Call(Hash.SET_PED_COMPONENT_VARIATION, Game.PlayerPed, 8, _newIndex, this.TempOutfit.UndershirtTexture, 0);
                    this.TempOutfit.Undershirt = _newIndex;
                }
                else if (_listItem.Text == "Pants")
                {
                    this.TempOutfit.PantTexture = 0;
                    Function.Call(Hash.SET_PED_COMPONENT_VARIATION, Game.PlayerPed, 4, _newIndex, this.TempOutfit.PantTexture, 0);
                    this.TempOutfit.Pants = _newIndex;
                }
                else if (_listItem.Text == "Shoes")
                {
                    this.TempOutfit.ShoeTexture = 0;
                    Function.Call(Hash.SET_PED_COMPONENT_VARIATION, Game.PlayerPed, 6, _newIndex, this.TempOutfit.ShoeTexture, 0);
                    this.TempOutfit.Shoes = _newIndex;
                }
                else if (_listItem.Text == "Accessory")
                {
                    this.TempOutfit.HolsterTexture = 0;
                    Function.Call(Hash.SET_PED_COMPONENT_VARIATION, Game.PlayerPed, 7, _newIndex, this.TempOutfit.HolsterTexture, 0);
                    this.TempOutfit.Holster = _newIndex;
                }
            };

            this.m.OnListItemSelect += (_menu, _listItem, _listIndex, _itemIndex) =>
            {
                if (_listItem.Text == "Shirt")
                {
                    int MaxTextures = Function.Call<int>(Hash.GET_NUMBER_OF_PED_TEXTURE_VARIATIONS, Game.PlayerPed, 11, _listIndex);
                    if (this.TempOutfit.ShirtTexture >= MaxTextures - 1)
                    {
                        Function.Call(Hash.SET_PED_COMPONENT_VARIATION, Game.PlayerPed, 11, _listIndex, 0, 0);
                        this.TempOutfit.ShirtTexture = 0;
                    }
                    else
                    {
                        Function.Call(Hash.SET_PED_COMPONENT_VARIATION, Game.PlayerPed, 11, _listIndex, this.TempOutfit.ShirtTexture, 0);
                        this.TempOutfit.ShirtTexture += 1;
                    }
                }
                else if (_listItem.Text == "Undershirt")
                {
                    int MaxTextures = Function.Call<int>(Hash.GET_NUMBER_OF_PED_TEXTURE_VARIATIONS, Game.PlayerPed, 8, _listIndex);
                    if (this.TempOutfit.UndershirtTexture >= MaxTextures - 1)
                    {
                        Function.Call(Hash.SET_PED_COMPONENT_VARIATION, Game.PlayerPed, 8, _listIndex, 0, 0);
                        this.TempOutfit.UndershirtTexture = 0;
                    }
                    else
                    {
                        Function.Call(Hash.SET_PED_COMPONENT_VARIATION, Game.PlayerPed, 8, _listIndex, this.TempOutfit.UndershirtTexture, 0);
                        this.TempOutfit.UndershirtTexture += 1;
                    }
                }
                else if (_listItem.Text == "Pants")
                {
                    int MaxTextures = Function.Call<int>(Hash.GET_NUMBER_OF_PED_TEXTURE_VARIATIONS, Game.PlayerPed, 4, _listIndex);
                    if (this.TempOutfit.PantTexture >= MaxTextures - 1)
                    {
                        Function.Call(Hash.SET_PED_COMPONENT_VARIATION, Game.PlayerPed, 4, _listIndex, 0, 0);
                        this.TempOutfit.PantTexture = 0;
                    }
                    else
                    {
                        Function.Call(Hash.SET_PED_COMPONENT_VARIATION, Game.PlayerPed, 4, _listIndex, this.TempOutfit.PantTexture, 0);
                        this.TempOutfit.PantTexture += 1;
                    }
                }
                else if (_listItem.Text == "Shoes")
                {
                    int MaxTextures = Function.Call<int>(Hash.GET_NUMBER_OF_PED_TEXTURE_VARIATIONS, Game.PlayerPed, 6, _listIndex);
                    if (this.TempOutfit.ShoeTexture >= MaxTextures - 1)
                    {
                        Function.Call(Hash.SET_PED_COMPONENT_VARIATION, Game.PlayerPed, 6, _listIndex, 0, 0);
                        this.TempOutfit.ShoeTexture = 0;
                    }
                    else
                    {
                        Function.Call(Hash.SET_PED_COMPONENT_VARIATION, Game.PlayerPed, 6, _listIndex, this.TempOutfit.ShoeTexture, 0);
                        this.TempOutfit.ShoeTexture += 1;
                    }
                }
                else if (_listItem.Text == "Accessory")
                {
                    int MaxTextures = Function.Call<int>(Hash.GET_NUMBER_OF_PED_TEXTURE_VARIATIONS, Game.PlayerPed, 7, _listIndex);
                    if (this.TempOutfit.HolsterTexture >= MaxTextures - 1)
                    {
                        Function.Call(Hash.SET_PED_COMPONENT_VARIATION, Game.PlayerPed, 7, _listIndex, 0, 0);
                        this.TempOutfit.HolsterTexture = 0;
                    }
                    else
                    {
                        Function.Call(Hash.SET_PED_COMPONENT_VARIATION, Game.PlayerPed, 7, _listIndex, this.TempOutfit.HolsterTexture, 0);
                        this.TempOutfit.HolsterTexture += 1;
                    }
                }
            };

            this.m.OnIndexChange += (_menu, _oldItem, _newItem, _oldIndex, _newIndex) =>
            {
                if (_newItem.Text == "Arms")
                {
                    Client.INSTANCE.BASE_CAMERA.Position = new Vector3(401.9558f, -997.1799f, -99.00404f);
                    Client.INSTANCE.BASE_CAMERA.PointAt(Game.PlayerPed.Bones[40296]);
                }
                else if (_newItem.Text == "Shirt" || _newItem.Text == "Undershirt" || _newItem.Text == "Accessory")
                {
                    Client.INSTANCE.BASE_CAMERA.Position = new Vector3(403.0408f, -998.124f, -99.00404f);
                    Client.INSTANCE.BASE_CAMERA.PointAt(Game.PlayerPed.Bones[24818]);
                }
                else if (_newItem.Text == "Pants")
                {
                    Client.INSTANCE.BASE_CAMERA.Position = new Vector3(403.0408f, -998.124f, -99.00404f - 0.5f);
                    Client.INSTANCE.BASE_CAMERA.PointAt(Game.PlayerPed.Bones[36864]);
                }
                else if (_newItem.Text == "Shoes")
                {
                    Client.INSTANCE.BASE_CAMERA.Position = new Vector3(401.9558f, -997.1799f, -99.00404f - 0.8f);
                    Client.INSTANCE.BASE_CAMERA.PointAt(Game.PlayerPed.Bones[20781]);
                }
            };
            #endregion

            #region Heritage Menu Events
            this.heritageMenu.OnItemSelect += (_menu, _item, _index) =>
            {
                if (_item.Text == "Back")
                {
                    _menu.CloseMenu();
                    _menu.ParentMenu.OpenMenu();
                }
            };

            this.heritageMenu.OnListIndexChange += (_menu, _listItem, _oldIndex, _newIndex, _itemIndex) =>
            {
                if (_listItem.Text == "Father")
                {
                    this.TempHeritage.DadFace = _newIndex;
                }
                else if (_listItem.Text == "Mother")
                {
                    this.TempHeritage.MomFace = _newIndex;
                }
                else if (_listItem.Text == "Eye Color")
                {
                    Function.Call(Hash._SET_PED_EYE_COLOR, Game.PlayerPed, _newIndex);
                    this.TempHeritage.EyeColor = _newIndex;
                }
                else if (_listItem.Text == "Hair")
                {
                    Function.Call(Hash.SET_PED_COMPONENT_VARIATION, Game.PlayerPed, 2, _newIndex, 0, 0);
                    this.TempHeritage.Hair = _newIndex;
                }
                else if (_listItem.Text == "Eyebrows")
                {
                    Function.Call(Hash.SET_PED_HEAD_OVERLAY, Game.PlayerPed, 2, _newIndex, this.TempHeritage.EyebrowOpacity);
                    this.TempHeritage.Eyebrow = _newIndex;
                }
                else if (_listItem.Text == "Beard")
                {
                    Function.Call(Hash.SET_PED_HEAD_OVERLAY, Game.PlayerPed, 1, _newIndex, this.TempHeritage.BeardOpacity);
                    this.TempHeritage.Beard = _newIndex;
                }
                else if (_listItem.Text == "Hair Color")
                {
                    Function.Call(Hash._SET_PED_HAIR_COLOR, Game.PlayerPed, _newIndex, _newIndex);
                    Function.Call(Hash._SET_PED_HEAD_OVERLAY_COLOR, Game.PlayerPed, 1, 1, _newIndex, _newIndex);
                    Function.Call(Hash._SET_PED_HEAD_OVERLAY_COLOR, Game.PlayerPed, 2, 1, _newIndex, _newIndex);
                    this.TempHeritage.HairColor = _newIndex;
                }
                else if (_listItem.Text == "Ageing")
                {
                    Function.Call(Hash.SET_PED_HEAD_OVERLAY, Game.PlayerPed, 3, _newIndex, this.TempHeritage.AgeingOpacity);
                    this.TempHeritage.Ageing = _newIndex;
                }
                else if (_listItem.Text == "Sun Damage")
                {
                    Function.Call(Hash.SET_PED_HEAD_OVERLAY, Game.PlayerPed, 7, _newIndex, this.TempHeritage.SunDamageOpacity);
                    this.TempHeritage.SunDamage = _newIndex;
                }
                else if (_listItem.Text == "Freckles")
                {
                    Function.Call(Hash.SET_PED_HEAD_OVERLAY, Game.PlayerPed, 9, _newIndex, this.TempHeritage.FreckleOpacity);
                    this.TempHeritage.Freckles = _newIndex;
                }
            };

            this.heritageMenu.OnSliderPositionChange += (_menu, _sliderItem, _oldPosition, _newPosition, _itemIndex) =>
            {
                if (_sliderItem.Text == "Parent Resemblance")
                {
                    //SET_PED_HEAD_BLEND_DATA(Ped ped, int shapeFirstID, int shapeSecondID,  int shapeThirdID, int skinFirstID, int skinSecondID,  int skinThirdID, float shapeMix, float skinMix, float thirdMix,  BOOL isParent)
                    Function.Call(Hash.SET_PED_HEAD_BLEND_DATA, Game.PlayerPed, this.TempHeritage.MomFace, this.TempHeritage.DadFace, 0, this.TempHeritage.MomFace, this.TempHeritage.DadFace, 0, (_newPosition / 10.0f), (_newPosition / 10.0f), 0.0f, false);
                    this.TempHeritage.ParentMorph = (_newPosition / 10.0f);
                }
                else if (_sliderItem.Text == "Beard Opacity")
                {
                    this.TempHeritage.BeardOpacity = (_newPosition / 10.0f);
                    Function.Call(Hash.SET_PED_HEAD_OVERLAY, Game.PlayerPed, 1, this.TempHeritage.Beard, this.TempHeritage.BeardOpacity);
                }
                else if (_sliderItem.Text == "Eyebrow Opacity")
                {
                    this.TempHeritage.EyebrowOpacity = (_newPosition / 10.0f);
                    Function.Call(Hash.SET_PED_HEAD_OVERLAY, Game.PlayerPed, 2, this.TempHeritage.Eyebrow, this.TempHeritage.EyebrowOpacity);
                }
                else if (_sliderItem.Text == "Ageing Opacity")
                {
                    this.TempHeritage.AgeingOpacity = (_newPosition / 10.0f);
                    Function.Call(Hash.SET_PED_HEAD_OVERLAY, Game.PlayerPed, 3, this.TempHeritage.Ageing, this.TempHeritage.AgeingOpacity);
                }
                else if (_sliderItem.Text == "Sun Damage Opacity")
                {
                    this.TempHeritage.SunDamageOpacity = (_newPosition / 10.0f);
                    Function.Call(Hash.SET_PED_HEAD_OVERLAY, Game.PlayerPed, 7, this.TempHeritage.SunDamage, this.TempHeritage.SunDamageOpacity);
                }
                else if (_sliderItem.Text == "Freckle Opacity")
                {
                    this.TempHeritage.FreckleOpacity = (_newPosition / 10.0f);
                    Function.Call(Hash.SET_PED_HEAD_OVERLAY, Game.PlayerPed, 9, this.TempHeritage.Freckles, this.TempHeritage.FreckleOpacity);
                }
            };
            #endregion

            #region Accessory Menu Events
            this.accessoryMenu.OnItemSelect += (_menu, _item, _index) =>
            {
                if (_item.Text == "Back")
                {
                    _menu.CloseMenu();
                    _menu.ParentMenu.OpenMenu();
                }
            };

            this.accessoryMenu.OnIndexChange += (_menu, _oldItem, _newItem, _oldIndex, _newIndex) =>
            {
                if (_newItem.Text == "Bracelet")
                {
                    Client.INSTANCE.BASE_CAMERA.Position = new Vector3(401.9558f, -997.1799f, -99.00404f);
                    Client.INSTANCE.BASE_CAMERA.PointAt(Game.PlayerPed.Bones[40296]);
                }
                else if (_newItem.Text == "Watch")
                {
                    Client.INSTANCE.BASE_CAMERA.Position = new Vector3(401.9558f + 1.5f, -997.1799f, -99.00404f);
                    Client.INSTANCE.BASE_CAMERA.PointAt(Game.PlayerPed.Bones[26610]);

                }
                else if (_newItem.Text == "Hat" || _newItem.Text == "Glasses" || _newItem.Text == "Mask" || _newItem.Text == "Makeup" || _newItem.Text == "Blush" || _newItem.Text == "Lipstick" || _newItem.Text == "Makeup Opacity" || _newItem.Text == "Blush Opacity" || _newItem.Text == "Lipstick Opacity" || _newItem.Text == "Lipstick Color" || _newItem.Text == "Blush Color" || _newItem.Text == "Makeup Color")
                {
                    Client.INSTANCE.BASE_CAMERA.Position = new Vector3(403f, -997f - 0.3f, -98.25f);
                    Client.INSTANCE.BASE_CAMERA.PointAt(Game.PlayerPed.Bones[31086]);
                }
                else if (_newItem.Text == "Earring")
                {
                    Client.INSTANCE.BASE_CAMERA.Position = new Vector3(403f - 0.5f, -997f - 0.3f, -98.25f);
                    Client.INSTANCE.BASE_CAMERA.PointAt(Game.PlayerPed.Bones[31086]);
                }
            };

            this.accessoryMenu.OnSliderPositionChange += (_menu, _sliderItem, _oldPosition, _newPosition, _itemIndex) =>
            {
                if (_sliderItem.Text == "Makeup Opacity")
                {
                    this.TempHeritage.MakeupOpacity = (_newPosition / 10.0f);
                    Function.Call(Hash.SET_PED_HEAD_OVERLAY, Game.PlayerPed, 4, this.TempHeritage.Makeup, this.TempHeritage.MakeupOpacity);
                }
                else if (_sliderItem.Text == "Blush Opacity")
                {
                    this.TempHeritage.BlushOpacity = (_newPosition / 10.0f);
                    Function.Call(Hash.SET_PED_HEAD_OVERLAY, Game.PlayerPed, 5, this.TempHeritage.Blush, this.TempHeritage.BlushOpacity);
                }
                else if (_sliderItem.Text == "Lipstick Opacity")
                {
                    this.TempHeritage.LipstickOpacity = (_newPosition / 10.0f);
                    Function.Call(Hash.SET_PED_HEAD_OVERLAY, Game.PlayerPed, 8, this.TempHeritage.Lipstick, this.TempHeritage.LipstickOpacity);
                }
            };

            this.accessoryMenu.OnListIndexChange += (_menu, _listItem, _oldIndex, _newIndex, _itemIndex) =>
            {
                if (_listItem.Text == "Hat")
                {
                    if (_listItem.GetCurrentSelection() == "Hat #-1")
                    {
                        this.TempOutfit.HatTexture = 0;
                        Function.Call(Hash.SET_PED_PROP_INDEX, Game.PlayerPed, 0, -1, 0, true);
                        Function.Call(Hash.CLEAR_PED_PROP, Game.PlayerPed, 0);
                        this.TempOutfit.Hat = -1;
                    }
                    else
                    {
                        this.TempOutfit.HatTexture = 0;
                        Function.Call(Hash.SET_PED_PROP_INDEX, Game.PlayerPed, 0, _newIndex, 0, true);
                        this.TempOutfit.Hat = _newIndex;
                    }
                }
                else if (_listItem.Text == "Glasses")
                {
                    this.TempOutfit.GlassesTexture = 0;
                    Function.Call(Hash.SET_PED_PROP_INDEX, Game.PlayerPed, 1, _newIndex, 0, true);
                    this.TempOutfit.Glasses = _newIndex;
                }
                else if (_listItem.Text == "Mask")
                {
                    this.TempOutfit.MaskTexture = 0;
                    Function.Call(Hash.SET_PED_COMPONENT_VARIATION, Game.PlayerPed, 1, _newIndex, 0, 0);
                    this.TempOutfit.Mask = _newIndex;
                }
                else if (_listItem.Text == "Earring")
                {
                    if (_listItem.GetCurrentSelection() == "Earring #-1")
                    {
                        Function.Call(Hash.SET_PED_PROP_INDEX, Game.PlayerPed, 2, -1, 0, true);
                        Function.Call(Hash.CLEAR_PED_PROP, Game.PlayerPed, 2);
                        this.TempOutfit.Earring = -1;
                    }
                    else
                    {
                        Function.Call(Hash.SET_PED_PROP_INDEX, Game.PlayerPed, 2, _newIndex, 0, true);
                        this.TempOutfit.Earring = _newIndex;
                    }
                }
                else if (_listItem.Text == "Watch")
                {
                    if (_listItem.GetCurrentSelection() == "Watch #-1")
                    {
                        this.TempOutfit.WatchTexture = 0;
                        Function.Call(Hash.SET_PED_PROP_INDEX, Game.PlayerPed, 6, -1, 0, true);
                        Function.Call(Hash.CLEAR_PED_PROP, Game.PlayerPed, 6);
                        this.TempOutfit.Watch = -1;
                    }
                    else
                    {
                        Function.Call(Hash.SET_PED_PROP_INDEX, Game.PlayerPed, 6, _newIndex, 0, true);
                        this.TempOutfit.Watch = _newIndex;
                    }
                }
                else if (_listItem.Text == "Bracelet")
                {
                    if (_listItem.GetCurrentSelection() == "Bracelet #-1")
                    {
                        this.TempOutfit.BraceletTexture = 0;
                        Function.Call(Hash.SET_PED_PROP_INDEX, Game.PlayerPed, 7, -1, 0, true);
                        Function.Call(Hash.CLEAR_PED_PROP, Game.PlayerPed, 7);
                        this.TempOutfit.Bracelet = -1;
                    }
                    else
                    {
                        Function.Call(Hash.SET_PED_PROP_INDEX, Game.PlayerPed, 7, _newIndex, 0, true);
                        this.TempOutfit.Bracelet = _newIndex;
                    }
                }
                else if (_listItem.Text == "Makeup")
                {
                    Function.Call(Hash.SET_PED_HEAD_OVERLAY, Game.PlayerPed, 4, _newIndex, this.TempHeritage.MakeupOpacity);
                    this.TempHeritage.Makeup = _newIndex;
                }
                else if (_listItem.Text == "Blush")
                {
                    Function.Call(Hash.SET_PED_HEAD_OVERLAY, Game.PlayerPed, 5, _newIndex, this.TempHeritage.BlushOpacity);
                    this.TempHeritage.Blush = _newIndex;
                }
                else if (_listItem.Text == "Lipstick")
                {
                    Function.Call(Hash.SET_PED_HEAD_OVERLAY, Game.PlayerPed, 8, _newIndex, this.TempHeritage.LipstickOpacity);
                    this.TempHeritage.Lipstick = _newIndex;
                }
                else if (_listItem.Text == "Blush Color")
                {
                    Function.Call(Hash._SET_PED_HEAD_OVERLAY_COLOR, Game.PlayerPed, 5, 2, _newIndex, _newIndex);
                    this.TempHeritage.BlushColor = _newIndex;
                }
                else if (_listItem.Text == "Lipstick Color")
                {
                    Function.Call(Hash._SET_PED_HEAD_OVERLAY_COLOR, Game.PlayerPed, 8, 2, _newIndex, _newIndex);
                    this.TempHeritage.LipstickColor = _newIndex;
                }
                else if (_listItem.Text == "Makeup Color")
                {
                    Function.Call(Hash._SET_PED_HEAD_OVERLAY_COLOR, Game.PlayerPed, 4, 2, _newIndex, _newIndex);
                    this.TempHeritage.MakeupColor = _newIndex;
                }
            };

            this.accessoryMenu.OnListItemSelect += (_menu, _listItem, _listIndex, _itemIndex) =>
            {
                if (_listItem.Text == "Hat")
                {
                    int MaxTextures = Function.Call<int>(Hash.GET_NUMBER_OF_PED_PROP_TEXTURE_VARIATIONS, Game.PlayerPed, 0, _listIndex);
                    if (this.TempOutfit.HatTexture >= MaxTextures - 1)
                    {
                        Function.Call(Hash.SET_PED_PROP_INDEX, Game.PlayerPed, 0, _listIndex, 0, true);
                        this.TempOutfit.HatTexture = 0;
                    }
                    else
                    {
                        Function.Call(Hash.SET_PED_PROP_INDEX, Game.PlayerPed, 0, _listIndex, this.TempOutfit.HatTexture, true);
                        this.TempOutfit.HatTexture += 1;
                    }
                }
                else if (_listItem.Text == "Glasses")
                {
                    int MaxTextures = Function.Call<int>(Hash.GET_NUMBER_OF_PED_PROP_TEXTURE_VARIATIONS, Game.PlayerPed, 1, _listIndex);
                    if (this.TempOutfit.GlassesTexture >= MaxTextures - 1)
                    {
                        Function.Call(Hash.SET_PED_PROP_INDEX, Game.PlayerPed, 1, _listIndex, 0, true);
                        this.TempOutfit.GlassesTexture = 0;
                    }
                    else
                    {
                        Function.Call(Hash.SET_PED_PROP_INDEX, Game.PlayerPed, 1, _listIndex, this.TempOutfit.GlassesTexture, true);
                        this.TempOutfit.GlassesTexture += 1;
                    }
                }
                else if (_listItem.Text == "Mask")
                {
                    int MaxTextures = Function.Call<int>(Hash.GET_NUMBER_OF_PED_TEXTURE_VARIATIONS, Game.PlayerPed, 1, _listIndex);
                    if (this.TempOutfit.MaskTexture >= MaxTextures - 1)
                    {
                        Function.Call(Hash.SET_PED_COMPONENT_VARIATION, Game.PlayerPed, 1, _listIndex, 0, 0);
                        this.TempOutfit.MaskTexture = 0;
                    }
                    else
                    {
                        Function.Call(Hash.SET_PED_COMPONENT_VARIATION, Game.PlayerPed, 1, _listIndex, this.TempOutfit.MaskTexture, true);
                        this.TempOutfit.MaskTexture += 1;
                    }
                }
                else if (_listItem.Text == "Watch")
                {
                    int MaxTextures = Function.Call<int>(Hash.GET_NUMBER_OF_PED_PROP_TEXTURE_VARIATIONS, Game.PlayerPed, 6, _listIndex);
                    if (this.TempOutfit.WatchTexture >= MaxTextures - 1)
                    {
                        Function.Call(Hash.SET_PED_PROP_INDEX, Game.PlayerPed, 6, _listIndex, 0, true);
                        this.TempOutfit.WatchTexture = 0;
                    }
                    else
                    {
                        Function.Call(Hash.SET_PED_PROP_INDEX, Game.PlayerPed, 6, _listIndex, this.TempOutfit.WatchTexture, true);
                        this.TempOutfit.WatchTexture += 1;
                    }
                }
                else if (_listItem.Text == "Bracelet")
                {
                    int MaxTextures = Function.Call<int>(Hash.GET_NUMBER_OF_PED_PROP_TEXTURE_VARIATIONS, Game.PlayerPed, 7, _listIndex);
                    if (this.TempOutfit.BraceletTexture >= MaxTextures - 1)
                    {
                        Function.Call(Hash.SET_PED_PROP_INDEX, Game.PlayerPed, 7, _listIndex, 0, true);
                        this.TempOutfit.BraceletTexture = 0;
                    }
                    else
                    {
                        Function.Call(Hash.SET_PED_PROP_INDEX, Game.PlayerPed, 7, _listIndex, this.TempOutfit.BraceletTexture, true);
                        this.TempOutfit.BraceletTexture += 1;
                    }
                }
            };
            #endregion

            #region Bone Menu Events
            this.boneMenu.OnItemSelect += (_menu, _item, _index) =>
            {
                if (_item.Text == "Back")
                {
                    _menu.CloseMenu();
                    _menu.ParentMenu.OpenMenu();
                }
            };

            this.boneMenu.OnSliderPositionChange += (_menu, _sliderItem, _oldPosition, _newPosition, _itemIndex) =>
            {
                if (_sliderItem.Text == "Nose Width")
                {
                    this.TempHeritage.NoseWidth = (_newPosition / 10.0f);
                    Function.Call(Hash._SET_PED_FACE_FEATURE, Game.PlayerPed, this.FaceFeatures["NoseWidth"], this.TempHeritage.NoseWidth);
                }
                else if (_sliderItem.Text == "Nose Height")
                {
                    this.TempHeritage.NoseHeight = (_newPosition / 10.0f);
                    Function.Call(Hash._SET_PED_FACE_FEATURE, Game.PlayerPed, this.FaceFeatures["NoseHeight"], this.TempHeritage.NoseHeight);
                }
                else if (_sliderItem.Text == "Nose Length")
                {
                    this.TempHeritage.NoseLength = (_newPosition / 10.0f);
                    Function.Call(Hash._SET_PED_FACE_FEATURE, Game.PlayerPed, this.FaceFeatures["NoseLength"], this.TempHeritage.NoseLength);
                }
                else if (_sliderItem.Text == "Nose Bone Height")
                {
                    this.TempHeritage.NoseBoneHeight = (_newPosition / 10.0f);
                    Function.Call(Hash._SET_PED_FACE_FEATURE, Game.PlayerPed, this.FaceFeatures["NoseBoneHeight"], this.TempHeritage.NoseBoneHeight);
                }
                else if (_sliderItem.Text == "Nose Lowering")
                {
                    this.TempHeritage.NoseLowering = (_newPosition / 10.0f);
                    Function.Call(Hash._SET_PED_FACE_FEATURE, Game.PlayerPed, this.FaceFeatures["NoseLowering"], this.TempHeritage.NoseLowering);
                }
                else if (_sliderItem.Text == "Nose Twist")
                {
                    this.TempHeritage.NoseBoneTwist = (_newPosition / 10.0f);
                    Function.Call(Hash._SET_PED_FACE_FEATURE, Game.PlayerPed, this.FaceFeatures["NoseTwist"], this.TempHeritage.NoseBoneTwist);
                }
                else if (_sliderItem.Text == "Eyebrow Height")
                {
                    this.TempHeritage.EyebrowHeight = (_newPosition / 10.0f);
                    Function.Call(Hash._SET_PED_FACE_FEATURE, Game.PlayerPed, this.FaceFeatures["EyebrowHeight"], this.TempHeritage.EyebrowHeight);
                }
                else if (_sliderItem.Text == "Eyebrow Width")
                {
                    this.TempHeritage.EyebrowWidth = (_newPosition / 10.0f);
                    Function.Call(Hash._SET_PED_FACE_FEATURE, Game.PlayerPed, this.FaceFeatures["EyebrowForward"], this.TempHeritage.EyebrowWidth);
                }
                else if (_sliderItem.Text == "Cheek Bone Height")
                {
                    this.TempHeritage.CheekBoneHeight = (_newPosition / 10.0f);
                    Function.Call(Hash._SET_PED_FACE_FEATURE, Game.PlayerPed, this.FaceFeatures["CheekHeight"], this.TempHeritage.CheekBoneHeight);
                }
                else if (_sliderItem.Text == "Cheek Bone Width")
                {
                    this.TempHeritage.CheekBoneWidth = (_newPosition / 10.0f);
                    Function.Call(Hash._SET_PED_FACE_FEATURE, Game.PlayerPed, this.FaceFeatures["CheekWidth"], this.TempHeritage.CheekBoneWidth);
                }
                else if (_sliderItem.Text == "Cheek Width")
                {
                    this.TempHeritage.CheekWidth = (_newPosition / 10.0f);
                    Function.Call(Hash._SET_PED_FACE_FEATURE, Game.PlayerPed, this.FaceFeatures["CheekWidth2"], this.TempHeritage.CheekWidth);
                }
                else if (_sliderItem.Text == "Eye Width")
                {
                    this.TempHeritage.EyeWidth = (_newPosition / 10.0f);
                    Function.Call(Hash._SET_PED_FACE_FEATURE, Game.PlayerPed, this.FaceFeatures["EyeWideness"], this.TempHeritage.EyeWidth);
                }
                else if (_sliderItem.Text == "Lip Thickness")
                {
                    this.TempHeritage.LipThickness = (_newPosition / 10.0f);
                    Function.Call(Hash._SET_PED_FACE_FEATURE, Game.PlayerPed, this.FaceFeatures["LipThickness"], this.TempHeritage.LipThickness);
                }
                else if (_sliderItem.Text == "Jaw Width")
                {
                    this.TempHeritage.JawWidth = (_newPosition / 10.0f);
                    Function.Call(Hash._SET_PED_FACE_FEATURE, Game.PlayerPed, this.FaceFeatures["JawWidth"], this.TempHeritage.JawWidth);
                }
                else if (_sliderItem.Text == "Jaw Length")
                {
                    this.TempHeritage.JawLength = (_newPosition / 10.0f);
                    Function.Call(Hash._SET_PED_FACE_FEATURE, Game.PlayerPed, this.FaceFeatures["JawLength"], this.TempHeritage.JawLength);
                }
                else if (_sliderItem.Text == "Chin Lowering")
                {
                    this.TempHeritage.ChinLowering = (_newPosition / 10.0f);
                    Function.Call(Hash._SET_PED_FACE_FEATURE, Game.PlayerPed, this.FaceFeatures["ChinLowering"], this.TempHeritage.ChinLowering);
                }
                else if (_sliderItem.Text == "Chin Length")
                {
                    this.TempHeritage.ChinLength = (_newPosition / 10.0f);
                    Function.Call(Hash._SET_PED_FACE_FEATURE, Game.PlayerPed, this.FaceFeatures["ChinLength"], this.TempHeritage.ChinLength);
                }
                else if (_sliderItem.Text == "Chin Width")
                {
                    this.TempHeritage.ChinWidth = (_newPosition / 10.0f);
                    Function.Call(Hash._SET_PED_FACE_FEATURE, Game.PlayerPed, this.FaceFeatures["ChinWidth"], this.TempHeritage.ChinWidth);
                }
                else if (_sliderItem.Text == "Chin Depth")
                {
                    this.TempHeritage.ChinHole = (_newPosition / 10.0f);
                    Function.Call(Hash._SET_PED_FACE_FEATURE, Game.PlayerPed, this.FaceFeatures["ChinHole"], this.TempHeritage.ChinHole);
                }
                else if (_sliderItem.Text == "Neck Thickness")
                {
                    this.TempHeritage.NeckThickness = (_newPosition / 10.0f);
                    Function.Call(Hash._SET_PED_FACE_FEATURE, Game.PlayerPed, this.FaceFeatures["NeckThickness"], this.TempHeritage.NeckThickness);
                }
            };
            #endregion
        }
    }
}
