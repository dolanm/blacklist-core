﻿using blacklist_client.bases.users;
using blacklist_client.utils;
using CitizenFX.Core;
using CitizenFX.Core.Native;
using MenuAPI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blacklist_client.bases.menus
{
    public class ProfileManagement
    {
        public Menu mainMenu;
        public Menu viewProfileMenu;
        public Menu createProfileMenu;
        public Menu manageProfileMenu;

        private Ped previewPed;

        private int DeleteButtonPressCount = 0;
        private int profileIndex = 0;

        #region CreateProfile Items
        public MenuItem NameItem;
        public MenuListItem HeightItem;
        public MenuListItem GenderItem;

        private string charName;
        private double newHeight = 4.0;
        private int gender = 0;
        #endregion

        private Dictionary<double, string> HeightReferences = new Dictionary<double, string>()
        {
            [4.0] = "4'0\"",
            [4.1] = "4'1\"",
            [4.2] = "4'2\"",
            [4.3] = "4'3\"",
            [4.4] = "4'4\"",
            [4.5] = "4'5\"",
            [4.6] = "4'6\"",
            [4.7] = "4'7\"",
            [4.8] = "4'8\"",
            [4.9] = "4'9\"",
            [5.0] = "5'0\"",
            [5.1] = "5'1\"",
            [5.2] = "5'2\"",
            [5.3] = "5'3\"",
            [5.4] = "5'4\"",
            [5.5] = "5'5\"",
            [5.6] = "5'6\"",
            [5.7] = "5'7\"",
            [5.8] = "5'8\"",
            [5.9] = "5'9\"",
            [6.0] = "6'0\"",
            [6.1] = "6'1\"",
            [6.2] = "6'2\"",
            [6.3] = "6'3\"",
            [6.4] = "6'4\"",
            [6.5] = "6'5\"",
            [6.6] = "6'6\"",
            [6.7] = "6'7\"",
            [6.8] = "6'8\"",
            [6.9] = "6'9\"",
            [7.0] = "7'0\""
        };

        public void ToggleMenuVisibility()
        {
            this.mainMenu.Visible = !this.mainMenu.Visible;
        }

        public void ResetMenuItems()
        {
            API.RequestInteriorRoomByName(94722, "limbo");
            API.RequestInteriorRoomByName(94722, "Mugshot_Room");
            API.RequestInteriorRoomByName(94722, "Mugshot_Room02");
            API.RefreshInterior(94722);

            this.mainMenu.ClearMenuItems();
            this.viewProfileMenu.ClearMenuItems();
            this.createProfileMenu.ClearMenuItems();

            MenuItem viewProfiles = new MenuItem("Manage Profiles", "Manage your existing profiles")
            {
                Label = "→→→"
            };

            MenuItem createProfile = new MenuItem("Create Profile", "Create a new profile")
            {
                Label = "→→→"
            };

            if(Utils.Registered_Profiles == null)
            {
                CitizenFX.Core.Debug.WriteLine("[BLACKLIST]: MENU->REGISTERED_PROFILES NULL");
            }

            if(Utils.Registered_Profiles.Count >= Convert.ToInt32(Client.INSTANCE.ClientCfg.LoadedConfig["bl_base_maxProfiles"]))
            {
                createProfile.LeftIcon = MenuItem.Icon.LOCK;
                createProfile.Enabled = false;
            }

            if(Utils.Registered_Profiles.Count <= 0)
            {
                viewProfiles.LeftIcon = MenuItem.Icon.LOCK;
                viewProfiles.Enabled = false;
            }

            MenuController.BindMenuItem(this.mainMenu, this.viewProfileMenu, viewProfiles);
            MenuController.BindMenuItem(this.mainMenu, this.createProfileMenu, createProfile);

            foreach(Profile p in Utils.Registered_Profiles)
            {
                if(p != null)
                {
                    MenuItem profileItem = new MenuItem(p.ProfileName, "Manage " + p.ProfileName)
                    {
                        Label = "→→→"
                    };

                    if(p.Heritage == null)
                    {
                        profileItem.LeftIcon = MenuItem.Icon.WARNING;
                        profileItem.Description += " (MISSING/CORRUPTED HERITAGE)";
                    }

                    if (p.Clothing == null)
                    {
                        profileItem.LeftIcon = MenuItem.Icon.WARNING;
                        profileItem.Description += " (MISSING/CORRUPTED MODEL)";
                    }

                    MenuController.BindMenuItem(this.viewProfileMenu, this.manageProfileMenu, profileItem);
                    this.viewProfileMenu.AddMenuItem(profileItem);
                }
            }

            this.mainMenu.AddMenuItem(viewProfiles);
            this.mainMenu.AddMenuItem(createProfile);

            this.NameItem = new MenuItem("Name:", "");

            List<string> heights = new List<string>();
            foreach(KeyValuePair<double, string> k in this.HeightReferences)
            {
                heights.Add(k.Value);
            }

            this.HeightItem = new MenuListItem("Height", heights, 0);
            this.GenderItem = new MenuListItem("Gender", new List<string>() { "Male", "Female" }, 0);
            MenuItem SubmitProfile = new MenuItem("Submit Profile", "Add your profile to the server")
            {
                Label = "→→→"
            };

            this.createProfileMenu.AddMenuItem(this.NameItem);
            this.createProfileMenu.AddMenuItem(this.HeightItem);
            this.createProfileMenu.AddMenuItem(this.GenderItem);
            this.createProfileMenu.AddMenuItem(SubmitProfile);
        }

        private void SetupSelectedProfile(int index)
        {
            if(Utils.Registered_Profiles.Count > 0)
            {
                Profile pr = Utils.Registered_Profiles[index];
                if(pr != null)
                {
                    this.manageProfileMenu.MenuTitle = pr.ProfileName;
                    MenuController.DisableBackButton = false;
                    this.manageProfileMenu.ClearMenuItems();

                    MenuItem TempNameItem = new MenuItem("Name: " + pr.ProfileName);
                    MenuItem TempDescItem = new MenuItem("Description: " + pr.ProfileDesc);
                    MenuItem TempHeightItem = new MenuItem("Height: " + pr.height.ToString());
                    MenuItem TempGenderItem = new MenuItem("Wallet: $" + pr.Bal_Wallet.ToString());
                    MenuItem TempFactionItem;
                    if(pr.Faction != null)
                    {
                        TempFactionItem = new MenuItem("Faction: " + pr.Faction.FactionName);
                    }
                    else
                    {
                        TempFactionItem = new MenuItem("Faction: null");
                    }

                    MenuItem TempUseCharacter = new MenuItem("Use Profile", "Use this profile for your current session")
                    {
                        Label = "→→→"
                    };
                    MenuItem TempDeleteCharacter = new MenuItem("Delete Profile", "Permanently delete this profile")
                    {
                        Label = "→→→"
                    };

                    this.manageProfileMenu.AddMenuItem(TempNameItem);
                    this.manageProfileMenu.AddMenuItem(TempDescItem);
                    this.manageProfileMenu.AddMenuItem(TempHeightItem);
                    this.manageProfileMenu.AddMenuItem(TempGenderItem);
                    this.manageProfileMenu.AddMenuItem(TempFactionItem);
                    this.manageProfileMenu.AddMenuItem(TempUseCharacter);
                    this.manageProfileMenu.AddMenuItem(TempDeleteCharacter);
                }
            }
        }

        public ProfileManagement()
        {
            MenuController.MenuAlignment = MenuController.MenuAlignmentOption.Left;
            if(this.mainMenu == null)
            {
                this.mainMenu = new Menu(Client.INSTANCE.ClientCfg.LoadedConfig["bl_base_schemaName"], "");
            }

            MenuController.AddMenu(this.mainMenu);
            MenuController.DisableBackButton = true;

            if(this.viewProfileMenu == null)
            {
                this.viewProfileMenu = new Menu("Profile Management", "");
            }

            if(this.createProfileMenu == null)
            {
                this.createProfileMenu = new Menu("Create Profile", "");
            }

            if(this.manageProfileMenu == null)
            {
                this.manageProfileMenu = new Menu("PLACEHOLDER_NAME", "");
            }

            MenuController.AddSubmenu(this.mainMenu, this.viewProfileMenu);
            MenuController.AddSubmenu(this.mainMenu, this.createProfileMenu);
            MenuController.AddSubmenu(this.viewProfileMenu, this.manageProfileMenu);

            this.mainMenu.OnItemSelect += (_menu, _item, _index) =>
            {
                if(_item.Text == "Manage Profiles")
                {
                    MenuController.DisableBackButton = false;
                    this.DeleteButtonPressCount = 0;
                    this.CreateProfilePreview(0);
                }
                else if(_item.Text == "Create Profile")
                {
                    MenuController.DisableBackButton = false;
                }
            };

            this.viewProfileMenu.OnIndexChange += (_menu, _oldItem, _newItem, _oldIndex, _newIndex) =>
            {
                this.CreateProfilePreview(_newIndex);
            };

            this.viewProfileMenu.OnMenuClose += (_menu) =>
            {
                MenuController.DisableBackButton = true;
            };

            this.mainMenu.OnMenuOpen += (_menu) =>
            {
                MenuController.DisableBackButton = true;
                Utils.SetPlayerModel("mp_m_freemode_01");
                Game.PlayerPed.Position = new Vector3(413.122f, -998.4281f, -99.40414f);
                Game.PlayerPed.IsPositionFrozen = true;
                API.FreezeEntityPosition(Game.PlayerPed.Handle, true);
                Client.INSTANCE.BASE_CAMERA.Position = Client.INSTANCE.ClientCfg.PM_Settings.CameraLocation;
                Client.INSTANCE.BASE_CAMERA.Rotation = new Vector3(0.0f, 0.0f, +90.0f);
                API.RefreshInterior(API.GetInteriorAtCoords(413.122f, -998.4281f, -99.40414f));
            };

            this.viewProfileMenu.OnItemSelect += (_menu, _item, _index) =>
            {
                SetupSelectedProfile(_index);
                this.profileIndex = _index;
            };

            this.createProfileMenu.OnItemSelect += async (_menu, _item, _index) =>
            {
                if(_item == this.NameItem)
                {
                    string CharacterName = await Utils.GetUserInput("Character Name", 255);
                    this.NameItem.Text = "Name: " + CharacterName;
                    this.charName = CharacterName;
                }else if(_item.Text == "Submit Profile")
                {
                    if(String.IsNullOrWhiteSpace(this.charName) || this.charName == "" || String.IsNullOrEmpty(this.charName))
                    {
                        return;
                    }
                    BaseScript.TriggerServerEvent("bl:SV_E:Character:CreateProfile", this.charName, this.newHeight, this.gender);
                    MenuController.CloseAllMenus();
                }
            };

            this.createProfileMenu.OnListIndexChange += (_menu, _listItem, _oldIndex, _newIndex, _itemIndex) =>
            {
                if (_listItem == this.HeightItem)
                {
                    double kv = 0.0;
                    foreach(KeyValuePair<double, string> k in this.HeightReferences)
                    {
                        if(k.Value == _listItem.GetCurrentSelection())
                        {
                            kv = k.Key;
                        }
                    }

                    this.newHeight = kv;
                }
                else if (_listItem == this.GenderItem)
                {
                    if (_listItem.GetCurrentSelection() == "Male")
                    {
                        this.gender = 0;
                        Utils.SetPlayerModel("mp_m_freemode_01");
                    }
                    else if (_listItem.GetCurrentSelection() == "Female")
                    {
                        this.gender = 1;
                        Utils.SetPlayerModel("mp_f_freemode_01");
                    }
                }
            };

            this.manageProfileMenu.OnItemSelect += (_menu, _item, _index) =>
            {
                if (_item.Text == "Use Profile")
                {
                    if (this.previewPed != null)
                    {
                        this.previewPed.Delete();
                        this.previewPed = null;
                    }
                    BaseScript.TriggerServerEvent("bl:SV_E:Character:SelectProfile", this.profileIndex);
                    MenuController.CloseAllMenus();
                }
                else if (_item.Text == "Delete Profile")
                {
                    if (this.DeleteButtonPressCount == 0)
                    {
                        this.DeleteButtonPressCount = 1;
                        _item.Label = "Press again to confirm deletion";
                    }
                    else
                    {
                        if (this.previewPed != null)
                        {
                            this.previewPed.Delete();
                            this.previewPed = null;
                        }
                        BaseScript.TriggerServerEvent("bl:SV_E:Character:DeleteProfile", this.profileIndex);
                        this.DeleteButtonPressCount = 0;
                        this.manageProfileMenu.CloseMenu();
                    }
                }
            };
        }

        private void MainMenu_OnMenuOpen(Menu menu)
        {
            throw new NotImplementedException();
        }

        public async void CreateProfilePreview(int index)
        {
            if (this.previewPed != null)
            {
                this.previewPed.Delete();
                this.previewPed = null;
            }

            if (Utils.Registered_Profiles[index] != null)
            {
                if (Utils.Registered_Profiles[index].gender == 0)
                {
                    this.previewPed = await EntityUtils.CreatePed(new Model(PedHash.FreemodeMale01), PedType.PED_TYPE_CIVMALE, Client.INSTANCE.ClientCfg.PM_Settings.DoorLocation, Client.INSTANCE.ClientCfg.PM_Settings.PreviewPed_SpawnHeading, false);
                }
                else
                {
                    this.previewPed = await EntityUtils.CreatePed(new Model(PedHash.FreemodeFemale01), PedType.PED_TYPE_CIVFEMALE, Client.INSTANCE.ClientCfg.PM_Settings.DoorLocation, Client.INSTANCE.ClientCfg.PM_Settings.PreviewPed_SpawnHeading, false);
                }
                if (Utils.Registered_Profiles[index].Heritage != null)
                {
                    Utils.Registered_Profiles[index].Heritage.Load(this.previewPed);
                    Utils.Registered_Profiles[index].Clothing.Load(this.previewPed);
                }
                CitizenFX.Core.Debug.WriteLine(JsonConvert.SerializeObject(Utils.Registered_Profiles[index].Heritage));
                Function.Call(Hash.TASK_GO_STRAIGHT_TO_COORD, this.previewPed, Client.INSTANCE.ClientCfg.PM_Settings.StopLocation.X, Client.INSTANCE.ClientCfg.PM_Settings.StopLocation.Y, Client.INSTANCE.ClientCfg.PM_Settings.StopLocation.Z, 1f, -1, Client.INSTANCE.ClientCfg.PM_Settings.PreviewPed_TurnToHeading, 0f);
                API.RequestInteriorRoomByName(94722, "limbo");
                API.RequestInteriorRoomByName(94722, "Mugshot_Room");
                API.RequestInteriorRoomByName(94722, "Mugshot_Room02");
            }
        }
    }
}
