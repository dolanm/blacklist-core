﻿using blacklist_client.bases.factions;
using blacklist_client.bases.items;
using blacklist_client.utils;
using CitizenFX.Core;
using CitizenFX.Core.Native;
using MenuAPI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blacklist_client.bases.menus
{
    public class VendorMenu
    {
        public Menu mainMenu;
        public Menu purchaseMenu;
        public Menu sellMenu;
        public Menu editMenu;

        public Menu edit_addItemMenu;
        public Menu edit_manageItemMenu;
        public Menu edit_changeItemMenu;

        public Menu edit_accessMenu;
        public Menu edit_manageFactionMenu;
        public Menu edit_manageClassMenu;

        public bool edit_itemDeletePressed = false;

        public int AccessedVendor = 0;
        public Vendor SelectedVendor = null;

        public VendorItem editedItem = null;
        public int selectedItem = 0;

        public void ToggleMenuVisibility()
        {
            this.mainMenu.Visible = !this.mainMenu.Visible;
        }

        public void ResetMenuItems()
        {
            this.mainMenu.ClearMenuItems();
            this.purchaseMenu.ClearMenuItems();
            this.sellMenu.ClearMenuItems();
            this.editMenu.ClearMenuItems();
            this.edit_addItemMenu.ClearMenuItems();
            this.edit_manageItemMenu.ClearMenuItems();
            this.edit_changeItemMenu.ClearMenuItems();
            this.edit_accessMenu.ClearMenuItems();
            this.edit_manageFactionMenu.ClearMenuItems();
            this.edit_manageClassMenu.ClearMenuItems();

            this.editedItem = null;
            this.selectedItem = 0;
            this.edit_itemDeletePressed = false;

            SelectedVendor = Client.INSTANCE.VendorManager.RegisteredVendors[this.AccessedVendor - 1];

            this.mainMenu.MenuTitle = SelectedVendor.title;
            this.mainMenu.MenuSubtitle = "STORED MONEY: $" + SelectedVendor.storedMoney;

             MenuItem buyMenuItem = new MenuItem("Purchase", "Purchase an item")
             {
                 Label = "→→→"
             };
            MenuItem sellMenuItem = new MenuItem("Sell", "Sell an item")
            {
                Label = "→→→"
            };

            MenuItem editMenuItem = new MenuItem("Edit", "Edit Vendor")
            {
                Label = "→→→"
            };

            MenuController.BindMenuItem(this.mainMenu, this.purchaseMenu, buyMenuItem);
            MenuController.BindMenuItem(this.mainMenu, this.sellMenu, sellMenuItem);
            MenuController.BindMenuItem(this.mainMenu, this.editMenu, editMenuItem);

            this.mainMenu.AddMenuItem(buyMenuItem);
            this.mainMenu.AddMenuItem(sellMenuItem);

            if(Client.INSTANCE.PermissionManager.HasAccess("vendors.edit"))
            {
                this.mainMenu.AddMenuItem(editMenuItem);
            }

            if(SelectedVendor.items.Count > 0)
            {
                foreach(VendorItem vi in SelectedVendor.items)
                {
                    MenuItem P_tempMenuItem;
                    MenuItem S_tempMenuItem;
                    if (vi.useStock)
                    {
                        P_tempMenuItem = new MenuItem(vi.name + " (" + vi.currentStock + "/" + vi.maxStock + ")", "Purchase " + vi.name + " for " + Client.INSTANCE.ClientCfg.LoadedConfig["bl_econ_currencyIcon"] + vi.buyPrice.ToString());
                        S_tempMenuItem = new MenuItem(vi.name + " (" + vi.currentStock + "/" + vi.maxStock + ")", "Sell " + vi.name + " for " + Client.INSTANCE.ClientCfg.LoadedConfig["bl_econ_currencyIcon"] + vi.sellPrice.ToString());
                    }
                    else
                    {
                        P_tempMenuItem = new MenuItem(vi.name, "Purchase " + vi.name + " for " + Client.INSTANCE.ClientCfg.LoadedConfig["bl_econ_currencyIcon"] + vi.buyPrice.ToString());
                        S_tempMenuItem = new MenuItem(vi.name, "Sell " + vi.name + " for " + Client.INSTANCE.ClientCfg.LoadedConfig["bl_econ_currencyIcon"] + vi.sellPrice.ToString());
                    }
                    this.purchaseMenu.AddMenuItem(P_tempMenuItem);
                    this.sellMenu.AddMenuItem(S_tempMenuItem);
                }
            }

            #region VENDOR EDITOR
            if (Client.INSTANCE.PermissionManager.HasAccess("vendors.edit"))
            {
                this.editMenu.MenuSubtitle = "EDITING: " + SelectedVendor.id;

                MenuItem edit_nameMenuItem = new MenuItem("Vendor Title", "Vendor Title")
                {
                    Label = SelectedVendor.title
                };

                MenuItem edit_addItemMenuItem = new MenuItem("Add Item", "Add Item")
                {
                    Label = "→→→"
                };

                MenuItem edit_changeItemMenuItem = new MenuItem("Manage Items", "Manage Items")
                {
                    Label = "→→→"
                };

                MenuItem edit_changeAccessMenuItem = new MenuItem("Manage Access", "Manage Access")
                {
                    Label = "→→→"
                };

                MenuItem edit_takeMoneyMenuItem = new MenuItem("~r~Retrieve Money", "Retrieve Money");


                MenuController.BindMenuItem(this.editMenu, this.edit_addItemMenu, edit_addItemMenuItem);
                MenuController.BindMenuItem(this.editMenu, this.edit_manageItemMenu, edit_changeItemMenuItem);
                MenuController.BindMenuItem(this.editMenu, this.edit_accessMenu, edit_changeAccessMenuItem);

                this.editMenu.AddMenuItem(edit_nameMenuItem);
                this.editMenu.AddMenuItem(edit_addItemMenuItem);
                this.editMenu.AddMenuItem(edit_changeItemMenuItem);
                this.editMenu.AddMenuItem(edit_changeAccessMenuItem);

                if (!(this.SelectedVendor.useMoney))
                {
                    edit_takeMoneyMenuItem.Enabled = false;
                    edit_takeMoneyMenuItem.LeftIcon = MenuItem.Icon.LOCK;
                }

                this.editMenu.AddMenuItem(edit_takeMoneyMenuItem);

                if (Client.INSTANCE.ItemManager.RegisteredItems.Count > 0)
                {
                    foreach(BlItem i in Client.INSTANCE.ItemManager.RegisteredItems)
                    {
                        MenuItem mI = new MenuItem(i.ItemName, i.ItemDescription);
                        this.edit_addItemMenu.AddMenuItem(mI);
                    }
                }

                if(SelectedVendor.items.Count > 0)
                {
                    foreach(VendorItem i in SelectedVendor.items)
                    {
                        MenuItem mI = new MenuItem(i.name, "")
                        {
                            Label = "→→→"
                        };
                        this.edit_manageItemMenu.AddMenuItem(mI);
                        MenuController.BindMenuItem(this.edit_manageItemMenu, this.edit_changeItemMenu, mI);
                    }
                }
                else
                {
                    edit_changeItemMenuItem.LeftIcon = MenuItem.Icon.LOCK;
                    edit_changeItemMenuItem.Enabled = false;
                }

                MenuItem edit_changeFactionMenuItem = new MenuItem("Manage Factions", "Manage Factions")
                {
                    Label = "→→→"
                };

                MenuItem edit_changeClassMenuItem = new MenuItem("Manage Classes", "Manage Classes")
                {
                    Label = "→→→"
                };

                MenuController.BindMenuItem(this.edit_accessMenu, this.edit_manageFactionMenu, edit_changeFactionMenuItem);
                MenuController.BindMenuItem(this.edit_accessMenu, this.edit_manageClassMenu, edit_changeClassMenuItem);

                this.edit_accessMenu.AddMenuItem(edit_changeFactionMenuItem);
                this.edit_accessMenu.AddMenuItem(edit_changeClassMenuItem);
            }
            #endregion
        }

        public VendorMenu()
        {
            MenuController.MenuAlignment = MenuController.MenuAlignmentOption.Left;
            if (this.mainMenu == null)
            {
                this.mainMenu = new Menu("VENDOR_NAME", "STORED MONEY: $0");
            }
            if (this.editMenu == null)
            {
                this.editMenu = new Menu("Edit Vendor", "EDITING: -1");
            }
            if (this.purchaseMenu == null)
            {
                this.purchaseMenu = new Menu("Purchase", "STORED MONEY: $0");
            }
            if (this.sellMenu == null)
            {
                this.sellMenu = new Menu("Sell", "STORED MONEY: $0");
            }

            if (this.edit_addItemMenu == null)
            {
                this.edit_addItemMenu = new Menu("Add Item", "");
            }
            if (this.edit_manageItemMenu == null)
            {
                this.edit_manageItemMenu = new Menu("Mamage Items", "");
            }
            if (this.edit_changeItemMenu == null)
            {
                this.edit_changeItemMenu = new Menu("Change Item", "");
            }
            if(this.edit_accessMenu == null)
            {
                this.edit_accessMenu = new Menu("Edit Access", "");
            }
            if(this.edit_manageFactionMenu == null)
            {
                this.edit_manageFactionMenu = new Menu("Manage Factions", "");
            }
            if(this.edit_manageClassMenu == null)
            {
                this.edit_manageClassMenu = new Menu("Manage Classes", "");
            }

            MenuController.AddMenu(this.mainMenu);
            MenuController.AddMenu(this.editMenu);
            MenuController.AddMenu(this.purchaseMenu);
            MenuController.AddMenu(this.sellMenu);

            MenuController.AddMenu(this.edit_addItemMenu);
            MenuController.AddMenu(this.edit_manageItemMenu);
            MenuController.AddMenu(this.edit_changeItemMenu);
            MenuController.AddMenu(this.edit_accessMenu);
            MenuController.AddMenu(this.edit_manageFactionMenu);
            MenuController.AddMenu(this.edit_manageClassMenu);

            this.purchaseMenu.OnItemSelect += (_menu, _item, _index) =>
            {
                if(this.SelectedVendor.items.Count > 0)
                {
                    if(this.SelectedVendor.items[_index] != null)
                    {
                        VendorItem vi = this.SelectedVendor.items[_index];

                        if(vi.useStock)
                        {
                            if(!(vi.currentStock <= 0))
                            {
                                //Allow purchase and deduct from stock
                                BaseScript.TriggerServerEvent("bl:SV_E:Vendor:AttemptPurchase", this.AccessedVendor, _index);
                            }
                            else
                            {
                                Client.INSTANCE.NUIManager.ShowNotification("This item is out of stock", "right");
                            }
                        }
                        else
                        {
                            BaseScript.TriggerServerEvent("bl:SV_E:Vendor:AttemptPurchase", this.AccessedVendor, _index);
                        }
                    }
                }
            };

            this.sellMenu.OnItemSelect += (_menu, _item, _index) =>
            {
                if (this.SelectedVendor.items.Count > 0)
                {
                    if (this.SelectedVendor.items[_index] != null)
                    {
                        VendorItem vi = this.SelectedVendor.items[_index];

                        if (vi.useStock)
                        {
                            if (!(vi.currentStock <= 0))
                            {
                                //Allow purchase and deduct from stock
                                BaseScript.TriggerServerEvent("bl:SV_E:Vendor:AttemptSell", this.AccessedVendor, _index);
                            }
                            else
                            {
                                Client.INSTANCE.NUIManager.ShowNotification("This item is fully stocked", "right");
                            }
                        }
                        else
                        {
                            BaseScript.TriggerServerEvent("bl:SV_E:Vendor:AttemptSell", this.AccessedVendor, _index);
                        }
                    }
                }
            };

            this.edit_addItemMenu.OnItemSelect += (_menu, _item, _index) =>
            {
                string referenceName = "";
                if(_item.Text.Contains(" "))
                {
                    referenceName = _item.Text.Replace(' ', '_');
                }
                else
                {
                    referenceName = _item.Text;
                }
                BaseScript.TriggerServerEvent("bl:SV_E:Vendor:EDIT_AddItem", referenceName, this.AccessedVendor);
                MenuController.CloseAllMenus();
            };

            this.editMenu.OnItemSelect += async (_menu, _item, _index) =>
            {
                if(_item.Text == "~r~Retrieve Money" && _item.Enabled)
                {
                    BaseScript.TriggerServerEvent("bl:SV_E:Vendor:EDIT_RetrieveMoney", this.AccessedVendor);
                    MenuController.CloseAllMenus();
                }else if(_item.Text == "Vendor Title")
                {
                    string VendorTitle = await Utils.GetUserInput("Vendor Title", 255);
                    if(!(String.IsNullOrWhiteSpace(VendorTitle)))
                    {
                        BaseScript.TriggerServerEvent("bl:SV_E:Vendor:EDIT_SetName", VendorTitle, this.AccessedVendor);
                    }
                }
            };

            this.edit_accessMenu.OnItemSelect += (_menu, _item, _index) =>
            {
                if(_item.Text == "Manage Factions")
                {
                    //Reset faction list
                    this.edit_manageFactionMenu.ClearMenuItems();
                    foreach(Faction f in Client.INSTANCE.FactionManager.Registered)
                    {
                        MenuCheckboxItem tempItem = new MenuCheckboxItem(f.FactionName);
                        if(this.SelectedVendor.factions.Count > 0)
                        {
                            foreach(Faction ff in this.SelectedVendor.factions)
                            {
                                if(ff.FactionName == f.FactionName)
                                {
                                    tempItem.Checked = true;
                                }
                            }
                        }
                        this.edit_manageFactionMenu.AddMenuItem(tempItem);
                    }
                }
                else if(_item.Text == "Manage Classes")
                {
                    //Reset class list
                    this.edit_manageClassMenu.ClearMenuItems();
                    foreach (Faction f in this.SelectedVendor.factions)
                    {
                        if(f.Classes.Count > 0)
                        {
                            foreach(MetroClass c in f.Classes)
                            {
                                MenuCheckboxItem tempItem = new MenuCheckboxItem(f.FactionName + "->" + c.ClassName);
                                foreach(MetroClass cc in this.SelectedVendor.classes)
                                {
                                    if(cc.ClassName == c.ClassName)
                                    {
                                        tempItem.Checked = true;
                                    }
                                }
                                this.edit_manageClassMenu.AddMenuItem(tempItem);
                            }
                        }
                    }
                }
            };

            this.edit_manageFactionMenu.OnCheckboxChange += (_menu, _item, _index, _checked) =>
            {
                if (this.SelectedVendor.items.Count > 0)
                {
                    if(!(_checked))
                    {
                        //Remove
                        BaseScript.TriggerServerEvent("bl:SV_E:Vendor:EDIT_RemoveFaction", _item.Text, this.SelectedVendor.id);
                    }
                    else
                    {
                        //Add
                        BaseScript.TriggerServerEvent("bl:SV_E:Vendor:EDIT_AddFaction", _item.Text, this.SelectedVendor.id);
                    }
                }
            };

            this.edit_manageClassMenu.OnCheckboxChange += (_menu, _item, _index, _checked) =>
            {
                if (this.SelectedVendor.items.Count > 0)
                {
                    if (_item.Text.Contains("->"))
                    {
                        //AddClassToVendor([FromSource]Player p, string facName, string className, int vendorId)
                        string cl_fixed = _item.Text.Replace("-", "");
                        string[] cl = cl_fixed.Split('>');

                        if(!(_checked))
                        {
                            //Remove
                            BaseScript.TriggerServerEvent("bl:SV_E:Vendor:EDIT_RemoveClass", cl[1], this.SelectedVendor.id);
                        }
                        else
                        {
                            //Add
                            BaseScript.TriggerServerEvent("bl:SV_E:Vendor:EDIT_AddClass", cl[0], cl[1], this.SelectedVendor.id);
                        }
                    }
                }
            };

            this.edit_manageItemMenu.OnItemSelect += (_menu, _item, _index) =>
            {
                if(this.SelectedVendor.items.Count > 0)
                {
                    //CitizenFX.Core.Debug.WriteLine("CURRENT CLASSES: " + JsonConvert.SerializeObject(Utils.GetSelectedProfile().Faction.Classes));

                    if(this.edit_changeItemMenu.GetMenuItems().Count > 0)
                    {
                        this.edit_changeItemMenu.ClearMenuItems();
                    }
                    this.editedItem = this.SelectedVendor.items[_index];
                    this.selectedItem = _index;
                    List<string> BuyModes = new List<string>() { "BOTH", "BUY_ONLY", "SELL_ONLY" };

                    MenuItem item_baseId = new MenuItem("Base ID:", "")
                    {
                        Label = this.editedItem.baseId.ToString()
                    };
                    MenuItem item_name = new MenuItem("Item Name:", "")
                    {
                        Label = this.editedItem.name
                    };
                    MenuListItem item_buyMode = new MenuListItem("Buy Mode:", BuyModes, this.editedItem.mode);
                    MenuItem item_buyPrice = new MenuItem("Buy Price:", "")
                    {
                        Label = this.editedItem.buyPrice.ToString()
                    };
                    MenuItem item_sellPrice = new MenuItem("Sell Price:", "")
                    {
                        Label = this.editedItem.sellPrice.ToString()
                    };
                    MenuCheckboxItem item_useStock = new MenuCheckboxItem("Use Stock:")
                    {
                        Checked = this.editedItem.useStock
                    };
                    MenuItem item_maxStock = new MenuItem("Max Stock:", "")
                    {
                        Label = this.editedItem.maxStock.ToString()
                    };
                    MenuItem item_currentStock = new MenuItem("Current Stock:", "")
                    {
                        Label = this.editedItem.currentStock.ToString()
                    };
                    MenuItem vendor_deleteItem = new MenuItem("~r~Delete Item", "")
                    {
                        Label = "→→→",
                        LeftIcon = MenuItem.Icon.WARNING
                    };
                    MenuItem vendor_saveItem = new MenuItem("Save", "")
                    {
                        Label = "→→→"
                    };

                    item_baseId.Enabled = false;
                    item_baseId.LeftIcon = MenuItem.Icon.LOCK;
                    item_name.Enabled = false;
                    item_name.LeftIcon = MenuItem.Icon.LOCK;

                    this.edit_changeItemMenu.AddMenuItem(item_baseId);
                    this.edit_changeItemMenu.AddMenuItem(item_name);
                    this.edit_changeItemMenu.AddMenuItem(item_buyMode);
                    this.edit_changeItemMenu.AddMenuItem(item_buyPrice);
                    this.edit_changeItemMenu.AddMenuItem(item_sellPrice);
                    this.edit_changeItemMenu.AddMenuItem(item_useStock);
                    this.edit_changeItemMenu.AddMenuItem(item_maxStock);
                    this.edit_changeItemMenu.AddMenuItem(item_currentStock);
                    this.edit_changeItemMenu.AddMenuItem(vendor_deleteItem);
                    this.edit_changeItemMenu.AddMenuItem(vendor_saveItem);
                }
            };

            this.edit_changeItemMenu.OnItemSelect += async (_menu, _item, _index) =>
            {
                if (this.SelectedVendor.items.Count > 0)
                {
                    if (_item.Text == "Buy Price:")
                    {
                        int newBuyPrice = Convert.ToInt32(await Utils.GetUserInput("Buy Price", 255));
                        _item.Label = newBuyPrice.ToString();
                        this.editedItem.buyPrice = newBuyPrice;
                    }else if (_item.Text == "Sell Price:")
                    {
                        int newSellPrice = Convert.ToInt32(await Utils.GetUserInput("Sell Price", 255));
                        _item.Label = newSellPrice.ToString();
                        this.editedItem.sellPrice = newSellPrice;
                    }
                    else if (_item.Text == "Max Stock:")
                    {
                        int newMaxStock = Convert.ToInt32(await Utils.GetUserInput("Max Stock", 255));
                        _item.Label = newMaxStock.ToString();
                        this.editedItem.maxStock = newMaxStock;
                    }
                    else if (_item.Text == "Current Stock:")
                    {
                        int newCurrentStock = Convert.ToInt32(await Utils.GetUserInput("Current Stock", 255));
                        _item.Label = newCurrentStock.ToString();
                        this.editedItem.currentStock = newCurrentStock;
                    }else if(_item.Text == "Save")
                    {
                        BaseScript.TriggerServerEvent("bl:SV_E:Vendor:EDIT_SaveItem", this.SelectedVendor.id, this.selectedItem, JsonConvert.SerializeObject(this.editedItem));
                        MenuController.CloseAllMenus();
                    }else if(_item.Text == "~r~Delete Item")
                    {
                        if(this.edit_itemDeletePressed == false)
                        {
                            _item.Description = "~r~Press once more to confirm deletion!";
                            this.edit_itemDeletePressed = true;
                        }
                        else
                        {
                            BaseScript.TriggerServerEvent("bl:SV_E:Vendor:EDIT_DeleteItem", this.SelectedVendor.id, this.selectedItem);
                            MenuController.CloseAllMenus();
                        }
                    }
                }
            };

            this.edit_changeItemMenu.OnCheckboxChange += (_menu, _item, _index, _checked) =>
            {
                if(this.SelectedVendor.items.Count > 0)
                {
                    if(_item.Text == "Use Stock:")
                    {
                        this.editedItem.useStock = _checked;
                    }
                }
            };

            this.edit_changeItemMenu.OnListIndexChange += (_menu, _listItem, _oldIndex, _newIndex, _itemIndex) =>
            {
                if(_listItem.Text == "Buy Mode:")
                {
                    this.editedItem.mode = _newIndex;
                }
            };
        }
    }
}
