﻿using CitizenFX.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blacklist_client.bases.menus.settings
{
    public class ProfileManagement_Settings
    {
        public Vector3 CameraLocation { get; set; }
        public Vector3 DoorLocation { get; set; }
        public Vector3 StopLocation { get; set; }
        public float PreviewPed_TurnToHeading { get; set; }
        public float PreviewPed_SpawnHeading { get; set; }
    }
}
