﻿using blacklist_client.bases.factions;
using blacklist_client.bases.items;
using blacklist_client.bases.skills;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blacklist_client.bases.users
{
    public class Profile
    {
        public string ProfileName { get; set; }
        public string ProfileDesc { get; set; }

        public int gender { get; set; } = 0;

        public double height { get; set; }

        public int Bal_Wallet { get; set; } = 0;

        public Faction Faction { get; set; }
        public MetroClass Class { get; set; }

        public bool UsesModel { get; set; }
        public MetroModel characterModelInfo { get; set; }

        public MetroHeritage Heritage { get; set; }
        public Outfit Clothing { get; set; }

        public List<BlSkill> Skills = new List<BlSkill>();

        public InventoryItem[] Inventory { get; set; } = new InventoryItem[25];
        public Dictionary<string, string> RecognizedCharacters { get; set; } = new Dictionary<string, string>();
    }
}
