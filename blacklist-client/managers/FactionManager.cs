﻿using blacklist_client.bases.factions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blacklist_client.managers
{
    public class FactionManager
    {
        public List<Faction> Registered = new List<Faction>();

        public Faction GetDefaultFaction()
        {
            Faction returnedFaction = null;
            if (this.Registered.Count > 0)
            {
                foreach (Faction f in this.Registered)
                {
                    if (f.IsDefault)
                    {
                        returnedFaction = f;
                    }
                }
            }
            return returnedFaction;
        }

        public Faction GetFaction(string name)
        {
            if (this.Registered.Count > 0)
            {
                Faction f = null;
                foreach (Faction fac in this.Registered)
                {
                    if (fac.FactionName.ToLower() == name.ToLower())
                    {
                        f = fac;
                    }
                }
                return f;
            }
            else
            {
                return null;
            }
        }
    }
}
