﻿using blacklist_client.bases.items;
using blacklist_client.utils;
using CitizenFX.Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blacklist_client.managers
{
    public class ItemManager
    {
        public List<BlItem> RegisteredItems = new List<BlItem>();

        //public List<DroppedItem> WorldItems = new List<DroppedItem>();
        public Dictionary<string, DroppedItem> WorldItems = new Dictionary<string, DroppedItem>();

        public BlItem GetItemByID(int id)
        {
            BlItem returnedItem = null;
            if(this.RegisteredItems.Count > 0)
            {
                foreach(BlItem i in this.RegisteredItems)
                {
                    if(i.ItemID == id)
                    {
                        returnedItem = i;
                    }
                }
            }
            return returnedItem;
        }

        /*public void ItemDroppedInWorld(Vector3 pos, int id, int amount)
        {
            if(this.GetItemByID(id) != null && amount > 0)
            {
                DroppedItem di = new DroppedItem(id, amount, pos);
                this.WorldItems.Add(di);
                di.SpawnInWorld();
            }
        }*/

        public void ItemDroppedInWorld(string uuid, string itemJson)
        {
            if(!(String.IsNullOrWhiteSpace(itemJson)) && !(String.IsNullOrWhiteSpace(uuid)))
            {
                DroppedItem di = JsonConvert.DeserializeObject<DroppedItem>(itemJson);
                if(this.GetItemByID(di.ItemID) != null && di.ItemAmount > 0)
                {
                    this.WorldItems.Add(uuid, di);
                    di.SpawnInWorld();
                }
            }
        }

        public void RemoveWorldItem(string uuid, string itemJson)
        {
            if(!(String.IsNullOrWhiteSpace(itemJson)) && !(String.IsNullOrWhiteSpace(uuid)))
            {
                //DroppedItem di = JsonConvert.DeserializeObject<DroppedItem>(itemJson);
                if(this.WorldItems.ContainsKey(uuid))
                {
                    this.WorldItems[uuid].DeleteWorldProp();
                    this.WorldItems.Remove(uuid);
                }
            }
        }

        public void Tick()
        {
            if(this.WorldItems.Count > 0)
            {
                for(int i = 0; i < this.WorldItems.Count; i++)
                {
                    var di = this.WorldItems.ElementAt(i);
                    if(di.Value != null)
                    {
                        if(Game.PlayerPed.Position.DistanceToSquared(di.Value.Position) <= 2.0f)
                        {
                            Utils.Render3DText(new Vector3(di.Value.Position.X, di.Value.Position.Y, di.Value.Position.Z - 0.5f), "Press 'E' to pick up " + di.Value.ItemName + "(x" + di.Value.ItemAmount + ")", 255, 0.3f);
                            if(Game.IsControlJustPressed(1, Control.Pickup))
                            {
                                BaseScript.TriggerServerEvent("bl:SV_E:Inventory:PickupItem", di.Key, JsonConvert.SerializeObject(di.Value));
                            }
                        }
                    }
                }
            }
        }
    }
}
