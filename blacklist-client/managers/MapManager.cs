﻿using blacklist_client.bases;
using blacklist_client.utils;
using CitizenFX.Core;
using CitizenFX.Core.Native;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blacklist_client.managers
{
    public class MapManager
    {
        public List<Blip> MapBlips = new List<Blip>();
        public List<Blip> VendorBlips = new List<Blip>();
        private bool vendorsCreated = false;
        //private int spawnedVendorCount = 0;

        public List<BlCharacter> ActiveCharacters = new List<BlCharacter>();
        public List<Ped> VendorPeds = new List<Ped>();

        public BlCharacter GetCharacterByOwner(string id)
        {
            if(this.ActiveCharacters.Count > 0)
            {
                BlCharacter c = null;
                for(int i = 0; i < this.ActiveCharacters.Count; i++)
                {
                    if(this.ActiveCharacters[i].OwnerID == id)
                    {
                        c = this.ActiveCharacters[i];
                    }
                }
                return c;
            }
            else
            {
                return null;
            }
        }

        public void AddActiveCharacter(string name, string desc, int level, string owner)
        {
            this.ActiveCharacters.Add(new BlCharacter(name, desc, level, owner));
        }

        public void RemoveActiveCharacter(string owner)
        {
            if(this.GetCharacterByOwner(owner) != null)
            {
                BlCharacter c = this.GetCharacterByOwner(owner);
                this.ActiveCharacters.Remove(c);
            }
        }

        public async void SetupVendorPeds()
        {
            if(this.VendorPeds.Count > 0)
            {
                for(int i = 0; i < this.VendorPeds.Count; i++)
                {
                    if(this.VendorPeds[i] != null)
                    {
                        this.VendorPeds[i].Delete();
                        this.VendorPeds.RemoveAt(i);
                    }
                }
                for(int i = 0; i < this.VendorBlips.Count; i++)
                {
                    if(this.VendorBlips[i] != null)
                    {
                        this.VendorBlips[i].Delete();
                        this.VendorBlips.RemoveAt(i);
                    }
                }
            }

            if(Client.INSTANCE.VendorManager.RegisteredVendors.Count > 0)
            {
                for(int i = 0; i < Client.INSTANCE.VendorManager.RegisteredVendors.Count; i++)
                {
                    Vendor v = Client.INSTANCE.VendorManager.RegisteredVendors[i];
                    Ped vendorPed = await EntityUtils.CreatePed(new Model(API.GetHashKey(v.model)), PedType.PED_TYPE_MISSION, new Vector3(v.location.X, v.location.Y, v.location.Z - 1.0f), v.heading, false);
                    API.SetPedDefaultComponentVariation(vendorPed.Handle);
                    API.SetEntityCanBeDamaged(vendorPed.Handle, false);
                    API.SetBlockingOfNonTemporaryEvents(vendorPed.Handle, true);
                    API.SetPedCanRagdollFromPlayerImpact(vendorPed.Handle, false);
                    API.SetPedCanRagdoll(vendorPed.Handle, false);
                    API.SetPedResetFlag(vendorPed.Handle, 249, true);
                    API.SetPedResetFlag(vendorPed.Handle, 185, true);
                    API.SetPedResetFlag(vendorPed.Handle, 108, true);
                    API.SetPedResetFlag(vendorPed.Handle, 208, true);
                    vendorPed.CanBeTargetted = false;
                    vendorPed.IsInvincible = true;
                    vendorPed.CanRagdoll = false;
                    vendorPed.CanPlayGestures = false;
                    vendorPed.IsPositionFrozen = true;

                    this.VendorPeds.Add(vendorPed);

                    if (!(v.isHidden))
                    {
                        Blip vendorBlip = new Blip(API.AddBlipForCoord(v.location.X, v.location.Y, v.location.Z));
                        vendorBlip.Sprite = (BlipSprite)v.blipId;
                        vendorBlip.Name = v.title;
                        vendorBlip.Color = BlipColor.White;
                        vendorBlip.IsShortRange = true;
                        this.VendorBlips.Add(vendorBlip);
                    }
                }
            }
        }

        /*public async Task CreateVendors()
        {
            if(!(this.vendorsCreated))
            {
                if(this.VendorPeds.Count < Client.INSTANCE.VendorManager.RegisteredVendors.Count)
                {
                    foreach (Vendor v in Client.INSTANCE.VendorManager.RegisteredVendors)
                    {
                        //TODO Add peds to list for resyncing functionality and optimize creation for realtime vendor creation
                        Ped vendorPed = await EntityUtils.CreatePed(new Model(API.GetHashKey(v.model)), PedType.PED_TYPE_MISSION, new Vector3(v.location.X, v.location.Y, v.location.Z - 1.0f), v.heading, false);
                        API.SetPedDefaultComponentVariation(vendorPed.Handle);
                        API.SetEntityCanBeDamaged(vendorPed.Handle, false);
                        API.SetBlockingOfNonTemporaryEvents(vendorPed.Handle, true);
                        API.SetPedCanRagdollFromPlayerImpact(vendorPed.Handle, false);
                        API.SetPedCanRagdoll(vendorPed.Handle, false);
                        API.SetPedResetFlag(vendorPed.Handle, 249, true);
                        API.SetPedResetFlag(vendorPed.Handle, 185, true);
                        API.SetPedResetFlag(vendorPed.Handle, 108, true);
                        API.SetPedResetFlag(vendorPed.Handle, 208, true);
                        vendorPed.CanBeTargetted = false;
                        vendorPed.IsInvincible = true;
                        vendorPed.CanRagdoll = false;
                        vendorPed.CanPlayGestures = false;
                        vendorPed.IsPositionFrozen = true;

                        if (!(v.isHidden))
                        {
                            Blip vendorBlip = new Blip(API.AddBlipForCoord(v.location.X, v.location.Y, v.location.Z));
                            vendorBlip.Sprite = (BlipSprite)v.blipId;
                            vendorBlip.Name = v.title;
                            vendorBlip.Color = BlipColor.White;
                            vendorBlip.IsShortRange = true;
                            this.MapBlips.Add(vendorBlip);
                        }
                        //Blip vendorBlip = new Blip(API.AddBlipForCoord())
                        //this.spawnedVendorCount++;
                    }
                }
                else if(this.spawnedVendorCount >= Client.INSTANCE.VendorManager.RegisteredVendors.Count)
                {
                    this.vendorsCreated = true;
                    this.spawnedVendorCount = 0;
                }
            }
        }*/
    }
}
