﻿using blacklist_client.bases.items;
using blacklist_client.bases.users;
using blacklist_client.nui;
using blacklist_client.utils;
using CitizenFX.Core;
using CitizenFX.Core.Native;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blacklist_client.managers
{
    public class NUIManager
    {
        private bool isUIOpen = false;

        public NUIManager()
        {

        }

        public void SetupCallbacks()
        {
            this.RegisterNUICallback("ui_close", (data, callback) =>
            {
                API.SendNuiMessage(JsonConvert.SerializeObject(new
                {
                    action = "closeUI"
                }));
                API.SetNuiFocus(false, false);
                this.isUIOpen = false;
            });

            this.RegisterNUICallback("ui_item_use", (data, callback) =>
            {
                if(data.item != -1)
                {
                    BaseScript.TriggerServerEvent("bl:SV_E:Inventory:UseItem", data.item);
                }
            });

            this.RegisterNUICallback("ui_item_drop", (data, callback) =>
            {
                if(data.item != -1 && data.amount > 0)
                {
                    BaseScript.TriggerServerEvent("bl:SV_E:Inventory:DropItem", Game.PlayerPed.Position, data.item, data.amount);
                }
            });
        }

        private void RegisterNUICallback(string _type, Action<dynamic, CallbackDelegate> _callback)
        {
            API.RegisterNuiCallbackType(_type);
            Client.INSTANCE.RegisterEventHandler($"__cfx_nui:{_type}", _callback);
        }

        private void ShowF1Menu()
        {
            API.SendNuiMessage(JsonConvert.SerializeObject(new
            {
                action = "openUI"
            }));

            API.SetNuiFocus(true, true);
            this.isUIOpen = true;
        }

        public void ShowNotification(string msg, string side)
        {
            if(!(String.IsNullOrWhiteSpace(msg)) && !(String.IsNullOrWhiteSpace(side)))
            {
                API.SendNuiMessage(JsonConvert.SerializeObject(new
                {
                    action = "notify",
                    msg = msg,
                    side = side
                }));
                Function.Call(Hash.PLAY_SOUND_FRONTEND, -1, "COLLECTED", "HUD_AWARDS", true);
            }
        }

        public void SyncData()
        {
            if(Utils.GetSelectedProfile() != null)
            {
                Profile p = Utils.GetSelectedProfile();

                NUIPlayerData npd = null;

                List<NUIInventItem> niv = new List<NUIInventItem>();

                if (p.Inventory.Length > 0)
                {
                    foreach(InventoryItem i in p.Inventory)
                    {
                        if(i != null)
                        {
                            //CitizenFX.Core.Debug.WriteLine("i not null");
                            if (Client.INSTANCE.ItemManager.GetItemByID(i.id) != null)
                            {
                                //CitizenFX.Core.Debug.WriteLine("Got item by ID");
                                NUIInventItem n = new NUIInventItem(i.quantity, Client.INSTANCE.ItemManager.GetItemByID(i.id));
                                niv.Add(n);
                            }
                        }
                    }
                    //CitizenFX.Core.Debug.WriteLine("INV: " + JsonConvert.SerializeObject(p.Inventory));
                }

                npd = new NUIPlayerData(Game.PlayerPed.Health, Game.PlayerPed.MaxHealth, p.Bal_Wallet, 0, 0, niv);

                //CitizenFX.Core.Debug.WriteLine("NPD: " + JsonConvert.SerializeObject(npd));

                API.SendNuiMessage(JsonConvert.SerializeObject(new
                {
                    action = "updateData",
                    playerData = JsonConvert.SerializeObject(npd)
                }));
            }
        }

        public void OnTick()
        {
            if(Game.IsControlJustPressed(0, Control.ReplayStartStopRecording) && isUIOpen == false)
            {
                this.ShowF1Menu();
            }
        }
    }
}
