﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blacklist_client.managers
{
    public class PermissionManager
    {
        public List<string> ValidPerms = new List<string>();

        public void HasPerms(string node, bool has)
        {
            if(!(String.IsNullOrWhiteSpace(node)))
            {
                if(has)
                {
                    if (!(this.ValidPerms.Contains(node)))
                    {
                        this.ValidPerms.Add(node);
                    }
                }
            }
        }

        public bool HasAccess(string node)
        {
            if(this.ValidPerms.Contains(node))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
