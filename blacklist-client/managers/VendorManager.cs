﻿using blacklist_client.bases;
using blacklist_client.bases.factions;
using blacklist_client.bases.items;
using blacklist_client.utils;
using CitizenFX.Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blacklist_client.managers
{
    public enum VendorItemMode
    {
        BOTH = 0,
        BUY_ONLY = 1,
        SELL_ONLY = 2
    }

    public class VendorManager
    {
        public List<Vendor> RegisteredVendors = new List<Vendor>();
        public List<Ped> VendorPeds = new List<Ped>();


        public void CreateCallback(string modelId, bool hidden, int blip)
        {
            if(!(String.IsNullOrWhiteSpace(modelId)))
            {
                BaseScript.TriggerServerEvent("bl:SV_E:CheckPerm", "vendors.create");
                if (Client.INSTANCE.PermissionManager.HasAccess("vendors.create"))
                {
                    BaseScript.TriggerServerEvent("bl:SV_E:Vendor:Create", modelId, hidden, blip, Game.PlayerPed.Position, Game.PlayerPed.Heading);
                }
            }
        }

        public void RefreshData(int vendorId, string vendorTitle, int vendorMoney, string itemJson, string facs, string classes)
        {
            if(!(String.IsNullOrWhiteSpace(itemJson)))
            {
                Vendor tempVendor = this.RegisteredVendors[vendorId - 1];
                if(tempVendor != null)
                {
                    tempVendor.storedMoney = vendorMoney;
                    tempVendor.title = vendorTitle;
                    tempVendor.items = JsonConvert.DeserializeObject<List<VendorItem>>(itemJson);
                    tempVendor.factions = JsonConvert.DeserializeObject<List<Faction>>(facs);
                    tempVendor.classes = JsonConvert.DeserializeObject<List<MetroClass>>(classes);
                }
            }
        }

        public void UpdateList(string vJson)
        {
            if(!(String.IsNullOrWhiteSpace(vJson)))
            {
                this.RegisteredVendors = JsonConvert.DeserializeObject<List<Vendor>>(vJson);
                Client.INSTANCE.MapManager.SetupVendorPeds();
            }
        }

        public void Tick()
        {
            if(this.RegisteredVendors.Count > 0)
            {
                for(int i = 0; i < this.RegisteredVendors.Count; i++)
                {
                    Vendor v = this.RegisteredVendors[i];
                    if(v != null)
                    {
                        if (v.location.DistanceToSquared(Game.PlayerPed.Position) <= 2.0f)
                        {
                            if (v.factions.Count > 0)
                            {
                                if (v.factions.Contains(Utils.GetSelectedProfile().Faction) || Client.INSTANCE.PermissionManager.HasAccess("vendors.edit"))
                                {
                                    if (v.classes.Count > 0)
                                    {
                                        if (v.classes.Contains(Utils.GetSelectedProfile().Class) || Client.INSTANCE.PermissionManager.HasAccess("vendors.edit"))
                                        {
                                            Utils.Render3DText(new Vector3(v.location.X, v.location.Y, v.location.Z + 0.35f), "~g~" + v.title, 255, 0.35f);
                                            Utils.Render3DText(new Vector3(v.location.X, v.location.Y, v.location.Z + 0.25f), "Press 'E' to access the vendor", 255, 0.3f);
                                            if (Game.IsControlJustPressed(1, Control.Pickup))
                                            {
                                                BaseScript.TriggerServerEvent("bl:SV_E:CheckPerm", "vendors.edit");
                                                Client.INSTANCE.VendorMenu.AccessedVendor = v.id;
                                                BaseScript.TriggerServerEvent("bl:SV_E:Vendor:RefreshVendorItems", v.id);
                                                Client.INSTANCE.VendorMenu.ResetMenuItems();
                                                Client.INSTANCE.VendorMenu.ToggleMenuVisibility();
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Utils.Render3DText(new Vector3(v.location.X, v.location.Y, v.location.Z + 0.35f), "~g~" + v.title, 255, 0.5f);
                                        Utils.Render3DText(new Vector3(v.location.X, v.location.Y, v.location.Z + 0.25f), "Press 'E' to access the vendor", 255, 0.3f);
                                        if (Game.IsControlJustPressed(1, Control.Pickup))
                                        {
                                            BaseScript.TriggerServerEvent("bl:SV_E:CheckPerm", "vendors.edit");
                                            Client.INSTANCE.VendorMenu.AccessedVendor = v.id;
                                            BaseScript.TriggerServerEvent("bl:SV_E:Vendor:RefreshVendorItems", v.id);
                                            Client.INSTANCE.VendorMenu.ResetMenuItems();
                                            Client.INSTANCE.VendorMenu.ToggleMenuVisibility();
                                        }
                                    }
                                }
                            }
                            else
                            {
                                Utils.Render3DText(new Vector3(v.location.X, v.location.Y, v.location.Z + 0.35f), "~g~" + v.title, 255, 0.5f);
                                Utils.Render3DText(new Vector3(v.location.X, v.location.Y, v.location.Z + 0.25f), "Press 'E' to access the vendor", 255, 0.3f);
                                if (Game.IsControlJustPressed(1, Control.Pickup))
                                {
                                    BaseScript.TriggerServerEvent("bl:SV_E:CheckPerm", "vendors.edit");
                                    Client.INSTANCE.VendorMenu.AccessedVendor = v.id;
                                    BaseScript.TriggerServerEvent("bl:SV_E:Vendor:RefreshVendorItems", v.id);
                                    Client.INSTANCE.VendorMenu.ResetMenuItems();
                                    Client.INSTANCE.VendorMenu.ToggleMenuVisibility();
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
