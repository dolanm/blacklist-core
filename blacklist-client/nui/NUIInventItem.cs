﻿using blacklist_client.bases.items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blacklist_client.nui
{
    public class NUIInventItem
    {
        public int quantity { get; set; } = 0;
        public BlItem it { get; set; } = null;

        public NUIInventItem(int q, BlItem i)
        {
            this.quantity = q;
            this.it = i;
        }
    }
}
