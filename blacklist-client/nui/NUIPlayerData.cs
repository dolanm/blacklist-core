﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blacklist_client.nui
{
    public class NUIPlayerData
    {
        public int health { get; set; } = 0;
        public int maxhealth { get; set; } = 0;
        public int cash { get; set; } = 0;
        public int level { get; set; } = 0;
        public int skillpoints { get; set; } = 0;
        public List<NUIInventItem> inventory { get; set; }

        public NUIPlayerData(int h, int mh, int c, int l, int sp, List<NUIInventItem> i)
        {
            this.health = h;
            this.maxhealth = mh;
            this.cash = c;
            this.level = l;
            this.skillpoints = sp;
            this.inventory = i;
        }
    }
}
