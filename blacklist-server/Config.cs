﻿using blacklist_server.bases.menus;
using blacklist_server.utils;
using CitizenFX.Core;
using CitizenFX.Core.Native;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blacklist_server
{
    public class Config
    {
        public ProfileManagement_Settings PM_S = new ProfileManagement_Settings();

        private readonly string[] ConvarList = new string[]
        {
            "sv_enforceGameBuild",

            "bl_db_host",
            "bl_db_user",
            "bl_db_name",
            "bl_db_pass",

            "bl_core_maintenance",

            "bl_useDiscord",
            "bl_discordHook",

            "bl_base_schemaName",
            "bl_base_maxProfiles",
            "bl_base_pluginOverridesCharacterSelector",
            "bl_base_defaultCharDesc",

            "bl_econ_currencyIcon",
            "bl_econ_currencyPlural",
            "bl_econ_currencySingle",
            "bl_econ_startingBonus",
            "bl_econ_baseSalary"
        };

        public Dictionary<string, dynamic> LoadedConfig = new Dictionary<string, dynamic>()
        {
            ["sv_enforceGameBuild"] = 0,

            ["bl_db_host"] = "127.0.0.1",
            ["bl_db_user"] = "root",
            ["bl_db_name"] = "blacklist_fivem",
            ["bl_db_pass"] = "",

            ["bl_core_maintenance"] = false,

            ["bl_useDiscord"] = false,
            ["bl_discordHook"] = "",

            ["bl_base_schemaName"] = "Blacklist Core",
            ["bl_base_maxProfiles"] = 3,
            ["bl_base_pluginOverridesCharacterSelector"] = false,
            ["bl_base_defaultCharDesc"] = "A normal person that looks fresh to the area",
            
            ["bl_econ_currencyIcon"] = "$",
            ["bl_econ_currencyPlural"] = "dollars",
            ["bl_econ_currencySingle"] = "dollar",
            ["bl_econ_startingBonus"] = 500,
            ["bl_econ_baseSalary"] = 5
        };

        public Config()
        {
            #region Default ProfileManagement_Settings
            this.PM_S.CameraLocation = new Vector3(411.7894f - 1.0f, -998.423f, -99.20408f + 0.5f);
            this.PM_S.DoorLocation = new Vector3(409.7066f, -1001.121f, -99.00416f - 1.0f);
            this.PM_S.StopLocation = new Vector3(409.3145f, -998.3396f, -99.00413f);

            this.PM_S.PreviewPed_TurnToHeading = 270.4346f;
            this.PM_S.PreviewPed_SpawnHeading = 2.538533f;
            #endregion

            foreach (string c in this.ConvarList)
            {
                if(this.LoadedConfig.ContainsKey(c))
                {
                    this.LoadedConfig[c] = API.GetConvar(c, "");
                }
            }
            Utils.Logging.PrintToConsole("[Config]: Loaded config: " + JsonConvert.SerializeObject(this.LoadedConfig));
        }
    }
}
