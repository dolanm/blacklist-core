﻿using blacklist_server.api;
using blacklist_server.bases;
using blacklist_server.bases.users;
using blacklist_server.managers;
using blacklist_server.utils;
using CitizenFX.Core;
using CitizenFX.Core.Native;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blacklist_server
{
    public class Server : BaseScript
    {
        public static Server INSTANCE { get; set; }
        public Mysql MYSQL { get; set; }

        public Config ServerCfg = new Config();

        #region Class Instances

        public PermissionManager PermissionManager = new PermissionManager();
        public UserManager UserManager { get; set; }

        public ItemManager ItemManager { get; set; }
        public FactionManager FactionManager { get; set; }
        public SkillManager SkillManager { get; set; }

        public VendorManager VendorManager { get; set; }
        #endregion


        public CommandManager CommandManager = new CommandManager();
        public BlacklistAPI DevAPI { get; set; }
        public BaseScriptProxy BaseScriptProxy { get; set; }

        public Server()
        {
            Server.INSTANCE = this;
            Debug.WriteLine(" ");
            Debug.WriteLine("==========================================================");
            Debug.WriteLine(" ");


            Utils.Logging.PrintToConsole("[Core]: Initializing framework...");
            Utils.Logging.PrintToConsole("[MySQL]: Connecting to database...");

            this.MYSQL = new Mysql(this.ServerCfg.LoadedConfig["bl_db_host"], this.ServerCfg.LoadedConfig["bl_db_name"]);
            this.MYSQL.Login(this.ServerCfg.LoadedConfig["bl_db_user"], this.ServerCfg.LoadedConfig["bl_db_pass"]);

            if (!this.MYSQL.isConnected())
            {
                Utils.Logging.PrintToConsole("[MySQL]: Unable to connect!");
                API.StopResource(API.GetCurrentResourceName());
                return;
            }

            Utils.Logging.PrintToConsole("[MySQL]: Connected!");

            if(Convert.ToBoolean(this.ServerCfg.LoadedConfig["bl_core_maintenance"]))
            {
                string oldName = API.GetConvar("sv_hostname", "");
                API.SetConvar("sv_hostname", "🔒 " + oldName);
                Debug.WriteLine(" ");
                Utils.Logging.PrintToConsole("[Core]: Set hostname to " + API.GetConvar("sv_hostname", ""));
                Utils.Logging.PrintToConsole("[Core]: !SERVER RUNNING IN MAINTENANCE MODE!");
                Utils.Logging.PrintToConsole("[Core]: !SERVER RUNNING IN MAINTENANCE MODE!");
                Utils.Logging.PrintToConsole("[Core]: !SERVER RUNNING IN MAINTENANCE MODE!");
                Utils.Logging.PrintToConsole("[Core]: Set hostname to " + API.GetConvar("sv_hostname", ""));
                Debug.WriteLine(" ");
            }

            this.UserManager = new UserManager();

            this.ItemManager = new ItemManager();
            this.FactionManager = new FactionManager();
            this.SkillManager = new SkillManager();

            this.VendorManager = new VendorManager();

            this.DevAPI = new BlacklistAPI();

            Utils.Logging.PrintToConsole("[Core]: Registering events...");
            EventHandlers.Add("playerConnecting", new Action<Player, string, dynamic, dynamic>(this.OnPlayerConnecting));
            EventHandlers.Add("playerDropped", new Action<Player, string>(this.OnPlayerDisconnect));

            EventHandlers.Add("bl:SV_E:CheckPerm", new Action<Player, string>(this.PermissionManager.CL_HasPerms));

            EventHandlers.Add("bl:SV_E:PlayerMapLoaded", new Action<Player>(this.OnClientMapStart));
            EventHandlers.Add("bl:SV_E:PlayerReady", new Action<Player>(this.OnPlayerReady));

            EventHandlers.Add("bl:SV_E:RequestProfiles", new Action<Player>(this.OnPlayerRequestProfiles));
            EventHandlers.Add("bl:SV_E:Character:SaveLooks", new Action<int, string, string>(this.OnSaveLooks));
            EventHandlers.Add("bl:SV_E:Character:SelectProfile", new Action<Player, int>(this.OnPlayerSelectProfile));
            EventHandlers.Add("bl:SV_E:Character:DeleteProfile", new Action<Player, int>(this.OnPlayerDeleteProfile));
            EventHandlers.Add("bl:SV_E:Character:CreateProfile", new Action<Player, string, double, int>(this.OnPlayerCreateProfile));

            EventHandlers.Add("bl:SV_E:Vendor:Create", new Action<Player, string, bool, int, Vector3, float>(this.VendorManager.CreateVendor));
            EventHandlers.Add("bl:SV_E:Vendor:EDIT_AddItem", new Action<Player, string, int>(this.VendorManager.AddItemToVendor));
            EventHandlers.Add("bl:SV_E:Vendor:RefreshVendorItems", new Action<Player, int>(this.VendorManager.SendVendorItems));
            EventHandlers.Add("bl:SV_E:Vendor:EDIT_SaveItem", new Action<Player, int, int, string>(this.VendorManager.UpdateVendorItems));
            EventHandlers.Add("bl:SV_E:Vendor:EDIT_DeleteItem", new Action<Player, int, int>(this.VendorManager.DeleteItemFromVendor));
            EventHandlers.Add("bl:SV_E:Vendor:EDIT_RetrieveMoney", new Action<Player, int>(this.VendorManager.RetrieveMoneyFromVendor));
            EventHandlers.Add("bl:SV_E:Vendor:EDIT_AddFaction", new Action<Player, string, int>(this.VendorManager.AddFactionToVendor));
            EventHandlers.Add("bl:SV_E:Vendor:EDIT_RemoveFaction", new Action<Player, string, int>(this.VendorManager.RemoveFactionFromVendor));
            EventHandlers.Add("bl:SV_E:Vendor:EDIT_AddClass", new Action<Player, string, string, int>(this.VendorManager.AddClassToVendor));
            EventHandlers.Add("bl:SV_E:Vendor:EDIT_RemoveClass", new Action<Player, string, int>(this.VendorManager.RemoveClassFromVendor));
            EventHandlers.Add("bl:SV_E:Vendor:EDIT_SetName", new Action<Player, string, int>(this.VendorManager.SetVendorTitle));
            EventHandlers.Add("bl:SV_E:Vendor:AttemptPurchase", new Action<Player, int, int>(this.VendorManager.AttemptPurchase));
            EventHandlers.Add("bl:SV_E:Vendor:AttemptSell", new Action<Player, int, int>(this.VendorManager.AttemptSell));

            EventHandlers.Add("bl:SV_E:Inventory:UseItem", new Action<Player, int>(this.ItemManager.UseItem));
            EventHandlers.Add("bl:SV_E:Inventory:DropItem", new Action<Player, Vector3, int, int>(this.ItemManager.DropItem));
            EventHandlers.Add("bl:SV_E:Inventory:PickupItem", new Action<Player, string, string>(this.ItemManager.PickupItem));

            Debug.WriteLine(" ");
            Debug.WriteLine("==========================================================");
            Debug.WriteLine(" ");

            Tick += new Func<Task>(async delegate
            {
                if(this.MYSQL.CONNECTION == null || this.MYSQL.isConnected() == false)
                {
                    this.MYSQL.Login(this.ServerCfg.LoadedConfig["bl_db_user"], this.ServerCfg.LoadedConfig["bl_db_pass"]);
                }
            });

            //this.BaseScriptProxy = new BaseScriptProxy(this.EventHandlers, this.Exports, this.Players);

            this.CommandManager.RegisterCommands();

            Utils.Logging.PrintToConsole(API.GetConvarInt("sv_enforceGameBuild", 0).ToString());
            Utils.Logging.PrintToDiscord("Server started!");
        }

        private void OnClientMapStart([FromSource]Player p)
        {
            Utils.Logging.PrintToConsole("[Core]: " + p.Name + " map loaded!");
        }

        private void OnPlayerReady([FromSource]Player p)
        {
            this.UserManager.onPlayerReady(p);
        }

        private void OnPlayerSelectProfile([FromSource]Player p, int profile)
        {
            User u = this.UserManager.GetPlayerFromID(Convert.ToInt32(p.Handle));
            if(u == null)
            {
                Utils.Logging.PrintToConsole("[Core]: " + p.Name + " tried to select profile without valid User; dropping.");
                p.Drop("Unable to find User, please try again. If this persists, contact an admin.");
                return;
            }

            u.SelectedProfile = profile;

            List<BlCharacter> ActiveCharacters = new List<BlCharacter>();

            PlayerList pl = new PlayerList();
            foreach (Player pp in pl)
            {
                if (pp != p)
                {
                    User uu = this.UserManager.GetPlayerFromID(Convert.ToInt32(pp.Handle));
                    if (uu != null)
                    {
                        if (uu.GetSelectedProfile() != null)
                        {
                            ActiveCharacters.Add(new BlCharacter(uu.GetSelectedProfile().ProfileName, uu.GetSelectedProfile().ProfileDesc, 0, pp.Handle));
                        }
                    }

                    pp.TriggerEvent("bl:CL_E:Character:AddActive", u.GetSelectedProfile().ProfileName, u.GetSelectedProfile().ProfileDesc, 0, p.Handle);
                }
            }

            p.TriggerEvent("bl:CL_E:ProfileSelected", u.SelectedProfile, JsonConvert.SerializeObject(ActiveCharacters));

            Utils.Logging.PrintToConsole("[Core]: " + p.Name + " selected profile (" + u.GetSelectedProfile().ProfileName + ")");

            Utils.Logging.PrintToDiscord(p.Name + " selected profile (" + u.GetSelectedProfile().ProfileName + ")");
        }

        private void OnPlayerCreateProfile([FromSource]Player p, string name, double height, int gender)
        {
            User u = this.UserManager.GetPlayerFromID(Convert.ToInt32(p.Handle));
            if (u == null)
            {
                Utils.Logging.PrintToConsole("[Core]: " + p.Name + " tried to select profile without valid User; dropping.");
                p.Drop("Unable to find User, please try again. If this persists, contact an admin.");
                return;
            }

            if(u.AllProfiles.Count >= Convert.ToInt32(this.ServerCfg.LoadedConfig["bl_base_maxProfiles"]))
            {
                Utils.Logging.PrintToConsole("[Core]: " + p.Name + " tried to create profile while already at max; blocking.");
                return;
            }

            if (height < 4 || height > 7)
            {
                Utils.Logging.PrintToConsole("[Core]: " + p.Name + " tried to create profile with invalid height (" + height.ToString() + ")");
                return;
            }

            if (gender != 0 && gender != 1)
            {
                Utils.Logging.PrintToConsole("[Core]: " + p.Name + " tried to create profile with invalid gender (" + gender.ToString() + ")");
                return;
            }

            Profile pr = u.createProfile(name, height, gender);
            u.sendProfiles();
            u.SelectedProfile = u.AllProfiles.Count - 1;

            p.TriggerEvent("bl:CL_E:ProfileCreated");
        }

        private void OnPlayerDeleteProfile([FromSource]Player p, int profile)
        {
            User u = this.UserManager.GetPlayerFromID(Convert.ToInt32(p.Handle));
            if (u == null)
            {
                Utils.Logging.PrintToConsole("[Core]: " + p.Name + " tried to delete profile without valid User; dropping.");
                p.Drop("Unable to find User, please try again. If this persists, contact an admin.");
                return;
            }

            if (profile < 0 || profile > Convert.ToInt32(this.ServerCfg.LoadedConfig["bl_base_maxProfiles"]))
            {
                Utils.Logging.PrintToConsole("[Core]: " + p.Name + " tried to delete a profile with an invalid index (" + profile + ")");
                return;
            }

            u.removeProfile(profile);
            u.sendProfiles();
            p.TriggerEvent("bl:CL_E:ProfileDeleted");
            Utils.Logging.PrintToConsole("[Core]: " + p.Name + " deleted profile (" + profile + ")");
        }

        private void OnPlayerConnecting([FromSource]Player p, string name, dynamic reason, dynamic deferrals)
        {
            Dictionary<string, string> ids = Utils.GetProperPlayerIDs(p);
            if(!ids.ContainsKey("license"))
            {
                Utils.Logging.PrintToConsole("[Core]: " + p.Name + " attempted to connect but could not find license; dropping.");
                p.Drop("Unable to find license, please try again. If this persists, contact an admin.");
                return;
            }

            //this.ServerCfg.LoadedConfig["bl_core_maintenance"]
            if(!(Convert.ToBoolean(this.ServerCfg.LoadedConfig["bl_core_maintenance"])))
            {
                Dictionary<string, object> BanParas = new Dictionary<string, object>();
                BanParas.Add("license", ids["license"]);
                MySqlDataReader result = null;
                try
                {
                    result = this.MYSQL.Select("bans", BanParas);
                    if (result.HasRows)
                    {
                        while (result.Read())
                        {
                            if (!(result.GetDateTime("expires") <= DateTime.Now))
                            {
                                Utils.Logging.PrintToConsole("[Core]: " + p.Name + " attempted to connect while banned; dropping.");
                                reason("You have been banned.\r\n------------------------\r\nBan ID: " + result.GetInt32("id") + "\r\nBanned By: " + result.GetString("admin") + "\r\nExpiration Date: " + result.GetDateTime("expires") + "\r\nBan Reason: " + result.GetString("reason"));
                                result.Close();
                                API.CancelEvent();
                                return;
                            }
                        }
                    }
                    result.Close();
                }
                catch
                {
                    Utils.Logging.PrintToConsole("[Core]: " + p.Name + " attempted to connect while MySQL was terminated; dropping.");
                    reason("Unable to check your ban status, please try again.");
                    result.Close();
                    API.CancelEvent();
                    return;
                }

                User tempUser = new User(ids["license"]);

                tempUser.username = p.Name;
                tempUser.save();

                Utils.Logging.PrintToConsole("[Core]: " + p.Name + " is currently connecting...");

                Utils.Logging.PrintToDiscord(p.Name + " is currently connecting...");
            }
            else
            {
                if(this.PermissionManager.SV_HasPerms(p, "maintenance.bypass"))
                {
                    User tempUser = new User(ids["license"]);

                    tempUser.username = p.Name;
                    tempUser.save();

                    Utils.Logging.PrintToConsole("[Core]: " + p.Name + " is currently connecting...");

                    Utils.Logging.PrintToDiscord(p.Name + " is currently connecting...");
                }
                else
                {
                    Utils.Logging.PrintToConsole("[Core]: " + p.Name + " attempted to connect while server was running in maintenance mode; dropping.");
                    reason("Server is currently undergoing maintenance, please try again later.");
                    API.CancelEvent();
                    return;
                }
            }
        }

        private void OnPlayerDisconnect([FromSource]Player p, string reason)
        {
            if(!reason.Contains("banned"))
            {
                User u = this.UserManager.GetPlayerFromID(Convert.ToInt32(p.Handle));
                if(u == null)
                {
                    Utils.Logging.PrintToConsole("[Core]: " + p.Name + " Disconnected but could not find User; reason: " + reason);
                    return;
                }

                PlayerList pl = new PlayerList();
                foreach(Player pp in pl)
                {
                    if(pp != p)
                    {
                        pp.TriggerEvent("bl:CL_E:Character:RemoveActive", Convert.ToInt32(p.Handle));
                    }
                }

                this.UserManager.OnlinePlayers.Remove(u);
                Utils.Logging.PrintToConsole("[Core]: " + p.Name + " Disconnected: " + reason);

                Utils.Logging.PrintToDiscord(p.Name + " Disconnected: " + reason);
            }
        }

        private void OnPlayerRequestProfiles([FromSource]Player p)
        {
            Utils.Logging.PrintToConsole("[Core]: " + p.Name + " requested their profiles...");

            User u = this.UserManager.GetPlayerFromID(Convert.ToInt32(p.Handle));
            if(u == null)
            {
                Utils.Logging.PrintToConsole("[Core]: " + p.Name + " requested their profiles but cannot find User; dropping.");
                p.Drop("Unable to find User, please try again. If this persists, contact an admin.");
                return;
            }

            u.sendProfiles();
        }

        private void OnSaveLooks(int source, string heritageJson, string clothingJson)
        {
            PlayerList pl = new PlayerList();
            Player p = pl[source];
            User u = this.UserManager.GetPlayerFromID(Convert.ToInt32(p.Handle));
            if(u != null)
            {
                Profile pr = u.GetSelectedProfile();
                pr.Heritage = JsonConvert.DeserializeObject<MetroHeritage>(heritageJson);
                pr.Clothing = JsonConvert.DeserializeObject<Outfit>(clothingJson);
                u.save();
                u.sendProfiles();
            }
        }
    }
}
