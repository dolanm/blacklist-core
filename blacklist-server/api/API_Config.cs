﻿using blacklist_server.bases.menus;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blacklist_server.api
{
    public class API_Config
    {
        public delegate void OverridePMSettings(string s);
        public OverridePMSettings Override_PM_Settings;

        public API_Config()
        {
            this.Override_PM_Settings = new OverridePMSettings(this.OverridePM_Settings);
        }

        public void OverridePM_Settings(string sJson)
        {
            if(sJson != null && sJson != "")
            {
                ProfileManagement_Settings pms = JsonConvert.DeserializeObject<ProfileManagement_Settings>(sJson);
                if(pms != null)
                {
                    Server.INSTANCE.ServerCfg.PM_S = pms;
                }
            }
        }
    }
}
