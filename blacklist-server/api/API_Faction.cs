﻿using blacklist_server.bases.factions;
using blacklist_server.utils;
using CitizenFX.Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blacklist_server.api
{
    public class API_Faction
    {
        public delegate void Register(string f);
        public Register RegisterFaction;

        public delegate void C_Register(string n, string c);
        public C_Register RegisterClass;

        public delegate Faction GetFaction(string n);
        public GetFaction GetFactionByName = new GetFaction(Server.INSTANCE.FactionManager.GetFaction);


        public API_Faction()
        {
            this.RegisterFaction = new Register(this.Register_Faction);
            this.RegisterClass = new C_Register(this.Register_FactionClass);
        }

        public void Register_FactionClass(string cJson, string name)
        {
            if(cJson != "" && cJson != null)
            {
                MetroClass c = JsonConvert.DeserializeObject<MetroClass>(cJson);
                if(c != null)
                {
                    Server.INSTANCE.FactionManager.RegisterFactionClass(c, name);
                }
            }
        }

        public void Register_Faction(string fJson)
        {
            if(fJson != "")
            {
                Faction f = JsonConvert.DeserializeObject<Faction>(fJson);
                if(f != null)
                {
                    Server.INSTANCE.FactionManager.RegisterFaction(f);
                }
            }
        }
    }
}
