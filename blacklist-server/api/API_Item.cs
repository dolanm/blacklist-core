﻿using blacklist_server.bases;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blacklist_server.api
{
    public class API_Item
    {
        public delegate void Register(string i);
        public Register RegisterItem;

        public delegate BlItem GByName(string n);
        public GByName GetByName;
        public delegate BlItem GByID(int id);
        public GByID GetByID;

        public API_Item()
        {
            //Register Register = new Register(Server.INSTANCE.ItemManager.RegisterItem);
            this.RegisterItem = new Register(this.Register_Item);
            this.GetByName = new GByName(this.Get_ByName);
            this.GetByID = new GByID(this.Get_ByID);
        }

        public void Register_Item(string iJson)
        {
            if(iJson != null && iJson != "")
            {
                BlItem i = JsonConvert.DeserializeObject<BlItem>(iJson);
                if(i != null)
                {
                    Server.INSTANCE.ItemManager.RegisterItem(i);
                }
            }
        }

        public BlItem Get_ByName(string name)
        {
            if(name != null && name != "")
            {
                return Server.INSTANCE.ItemManager.GetItem(name);
            }
            return null;
        }

        public BlItem Get_ByID(int id)
        {
            if (id != -1)
            {
                return Server.INSTANCE.ItemManager.GetItem(id);
            }
            return null;
        }
    }
}
