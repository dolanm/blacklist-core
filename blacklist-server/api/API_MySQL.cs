﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blacklist_server.api
{
    public class API_MySQL
    {
        public delegate void Insert(string table, Object[] values);
        public Insert MySQL_Insert;

        public delegate void DeleteByID(string table, int id);
        public DeleteByID MySQL_DeleteByID;

        public delegate void Update(string table, Dictionary<String, Object> set, Dictionary<String, Object> where);
        public Update MySQL_Update;

        public API_MySQL()
        {
            this.MySQL_Insert = new Insert(MySQLInsert);
            this.MySQL_Update = new Update(MySQLUpdate);
            this.MySQL_DeleteByID = new DeleteByID(MySQLDeleteByID);
        }

        public void MySQLInsert(string table, Object[] values)
        {
            if(!(String.IsNullOrWhiteSpace(table)))
            {
                Server.INSTANCE.MYSQL.Insert(table, values);
            }
        }

        public void MySQLDeleteByID(string table, int id)
        {
            if(!(String.IsNullOrWhiteSpace(table)))
            {
                Server.INSTANCE.MYSQL.DeleteByID(table, id);
            }
        }

        public void MySQLUpdate(string table, Dictionary<String, Object> set, Dictionary<String, Object> where)
        {
            if (!(String.IsNullOrWhiteSpace(table)))
            {
                Server.INSTANCE.MYSQL.Update(table, set, where);
            }
        }

    }
}
