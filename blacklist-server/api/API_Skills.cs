﻿using blacklist_server.bases.skills;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blacklist_server.api
{
    public class API_Skills
    {
        public delegate void Register(string s);
        public Register RegisterSkill;

        public delegate BlSkill GByName(string n);
        public GByName GetByName;

        public API_Skills()
        {
            this.RegisterSkill = new Register(this.Register_Skill);
            this.GetByName = new GByName(this.Get_ByName);
        }

        public void Register_Skill(string sJson)
        {
            if(sJson != null && sJson != "")
            {
                BlSkill s = JsonConvert.DeserializeObject<BlSkill>(sJson);
                if(s != null)
                {
                    Server.INSTANCE.SkillManager.RegisterSkill(s);
                }
            }
        }

        public BlSkill Get_ByName(string name)
        {
            if(name != null && name != "")
            {
                return Server.INSTANCE.SkillManager.GetSkill(name);
            }
            return null;
        }
    }
}
