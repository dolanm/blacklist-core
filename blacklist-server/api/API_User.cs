﻿using blacklist_server.bases.users;
using blacklist_server.utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blacklist_server.api
{
    public class API_User
    {
        public delegate void SaveUser(int id);
        public SaveUser Save;

        public delegate Profile GetSelectedProfile(int id);
        public GetSelectedProfile getSelectedProfile;

        public delegate void SendProfiles(int id);
        public SendProfiles sendProfiles;

        public API_User()
        {
            this.Save = new SaveUser(this.Save_User_ByID);
            this.getSelectedProfile = new GetSelectedProfile(this.Get_SelectedProfile);
            this.sendProfiles = new SendProfiles(this.Send_Profiles);
        }

        public void Save_User_ByID(int id)
        {
            if (id != -1)
            {
                User u = Server.INSTANCE.UserManager.GetPlayerFromID(id);
                if(u != null)
                {
                    u.save();
                    Utils.Logging.PrintToConsole("[API_User]: Save_User_ByID -> Saved user!");
                }
                else
                {
                    Utils.Logging.PrintToConsole("[API_User]: Save_User_ByID -> No user found with ID!");
                }
            }
            else
            {
                Utils.Logging.PrintToConsole("[API_User]: Save_User_ByID -> Invalid ID provided!");
            }
        }

        public Profile Get_SelectedProfile(int id)
        {
            if (id != -1)
            {
                User u = Server.INSTANCE.UserManager.GetPlayerFromID(id);
                if (u != null)
                {
                    return u.GetSelectedProfile();
                }
                else
                {
                    Utils.Logging.PrintToConsole("[API_User]: Get_SelectedProfile -> No user found with ID!");
                    return null;
                }
            }
            else
            {
                Utils.Logging.PrintToConsole("[API_User]: Get_SelectedProfile -> Invalid ID provided!");
                return null;
            }
        }

        public void Send_Profiles(int id)
        {
            if (id != -1)
            {
                User u = Server.INSTANCE.UserManager.GetPlayerFromID(id);
                if (u != null)
                {
                    u.sendProfiles();
                    Utils.Logging.PrintToConsole("[API_User]: Send_Profiles -> Sent profiles!");
                }
                else
                {
                    Utils.Logging.PrintToConsole("[API_User]: Send_Profiles -> No user found with ID!");
                }
            }
            else
            {
                Utils.Logging.PrintToConsole("[API_User]: Send_Profiles -> Invalid ID provided!");
            }
        }
    }
}
