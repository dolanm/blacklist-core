﻿using blacklist_server.utils;
using CitizenFX.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blacklist_server.api
{
    public class BlacklistAPI : BaseScript
    {
        public API_Config ConfigApi { get; set; }
        public API_MySQL MySQLApi { get; set; }
        public API_Faction FactionApi { get; set; }
        public API_Item ItemApi { get; set; }
        public API_User UserApi { get; set; }
        public API_Skills SkillApi { get; set; }


        public BlacklistAPI()
        {
            Utils.Logging.PrintToConsole("[BlacklistAPI]: Hooking...");

            this.ConfigApi = new API_Config();
            this.MySQLApi = new API_MySQL();
            this.FactionApi = new API_Faction();
            this.ItemApi = new API_Item();
            this.UserApi = new API_User();
            this.SkillApi = new API_Skills();

            #region Config Hooks
            Exports.Add("Config_OverridePMSettings", this.ConfigApi.Override_PM_Settings);
            #endregion

            #region MySQL Hooks
            Exports.Add("MySQL_Insert", this.MySQLApi.MySQL_Insert);
            Exports.Add("MySQL_DeleteByID", this.MySQLApi.MySQL_DeleteByID);
            Exports.Add("MySQL_Update", this.MySQLApi.MySQL_Update);
            #endregion

            #region Faction Hooks
            Exports.Add("Faction_Register", this.FactionApi.RegisterFaction);
            Exports.Add("Class_Register", this.FactionApi.RegisterClass);
            Exports.Add("Faction_GetByName", this.FactionApi.GetFactionByName);
            #endregion

            #region Item Hooks
            Exports.Add("Item_Register", this.ItemApi.RegisterItem);
            Exports.Add("Item_GetByName", this.ItemApi.GetByName);
            Exports.Add("Item_GetByID", this.ItemApi.GetByID);
            #endregion

            #region User Hooks
            Exports.Add("User_Save", this.UserApi.Save);
            Exports.Add("User_GetSelectedProfile", this.UserApi.getSelectedProfile);
            Exports.Add("User_SendProfiles", this.UserApi.sendProfiles);
            #endregion

            #region Skill Hooks
            Exports.Add("Skill_Register", this.SkillApi.RegisterSkill);
            Exports.Add("Skill_GetByName", this.SkillApi.GetByName);
            #endregion

            Server.INSTANCE.BaseScriptProxy = new BaseScriptProxy(this.EventHandlers, this.Exports, this.Players);

            Utils.Logging.PrintToConsole("[BlacklistAPI]: Done!");
        }
    }
}
