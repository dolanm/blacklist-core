﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blacklist_server.bases
{
    public class BlCharacter
    {
        public string CharacterName { get; set; } = "";
        public string CharacterDesc { get; set; } = "";
        public int CharacterLevel { get; set; } = 0;
        public string OwnerID { get; set; } = "";

        public BlCharacter(string n, string d, int l, string o)
        {
            this.CharacterName = n;
            this.CharacterDesc = d;
            this.CharacterLevel = l;
            this.OwnerID = o;
        }
    }
}
