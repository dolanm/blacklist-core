﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blacklist_server.bases
{
    public class BlItem
    {
        public int ItemID { get; set; }
        public string ItemName { get; set; }
        public string ItemDescription { get; set; }
        public int MaxStack { get; set; }
        public bool IsUsable { get; set; }
        public bool HasBuffs { get; set; }
        public int ItemRarity { get; set; }

        public Dictionary<string, object> PersistentValues { get; set; }

        public BlItem(int id, string name, string desc, int stack, bool usable = true, bool buffs = false, int rarity = 0)
        {
            this.ItemID = id;
            this.ItemName = name;
            this.ItemDescription = desc;
            this.MaxStack = stack;
            this.IsUsable = usable;
            this.HasBuffs = buffs;
            this.ItemRarity = rarity;

            this.PersistentValues = new Dictionary<string, object>();
        }

        public virtual void Use(int playerid) { }

        public virtual void Drop(int amount) { }
    }
}
