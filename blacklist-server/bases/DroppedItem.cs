﻿using CitizenFX.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blacklist_server.bases
{
    public class DroppedItem
    {
        public string ItemName { get; } = "";
        public int ItemID { get; set; } = 0;
        public int ItemAmount { get; set; } = 0;
        public Vector3 Position { get; set; }

        public DroppedItem(int id, int amount, Vector3 pos)
        {
            this.ItemID = id;

            if (Server.INSTANCE.ItemManager.GetItem(id) != null)
            {
                BlItem tempItem = Server.INSTANCE.ItemManager.GetItem(id);
                this.ItemName = tempItem.ItemName;
            }

            this.ItemAmount = amount;
            this.Position = pos;
        }
    }
}
