﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blacklist_server.bases
{
    public class MetroHeritage
    {
        public int DadFace { get; set; } = 0;
        public int MomFace { get; set; } = 0;

        public float ParentMorph { get; set; } = 0.0f;

        public int Hair { get; set; } = 0;
        public int Eyebrow { get; set; } = 0;
        public int Beard { get; set; } = 0;
        public int HairColor { get; set; } = 0;
        public int EyeColor { get; set; } = 0;
        public int Ageing { get; set; } = 0;
        public int Makeup { get; set; } = 0;
        public int Blush { get; set; } = 0;
        public int Lipstick { get; set; } = 0;
        public int SunDamage { get; set; } = 0;
        public int Freckles { get; set; } = 0;

        public float BeardOpacity { get; set; } = 0.0f;
        public float EyebrowOpacity { get; set; } = 0.0f;

        public float AgeingOpacity { get; set; } = 0.0f;
        public float MakeupOpacity { get; set; } = 0.0f;
        public float BlushOpacity { get; set; } = 0.0f;
        public float LipstickOpacity { get; set; } = 0.0f;
        public float SunDamageOpacity { get; set; } = 0.0f;
        public float FreckleOpacity { get; set; } = 0.0f;
        public int BlushColor { get; set; } = 0;
        public int LipstickColor { get; set; } = 0;
        public int MakeupColor { get; set; } = 0;

        public float NoseWidth { get; set; } = 0.0f;
        public float NoseHeight { get; set; } = 0.0f;
        public float NoseLength { get; set; } = 0.0f;
        public float NoseBoneHeight { get; set; } = 0.0f;
        public float NoseLowering { get; set; } = 0.0f;
        public float NoseBoneTwist { get; set; } = 0.0f;

        public float EyebrowHeight { get; set; } = 0.0f;
        public float EyebrowWidth { get; set; } = 0.0f;

        public float CheekBoneHeight { get; set; } = 0.0f;
        public float CheekBoneWidth { get; set; } = 0.0f;
        public float CheekWidth { get; set; } = 0.0f;

        public float EyeWidth { get; set; } = 0.0f;

        public float LipThickness { get; set; } = 0.0f;

        public float JawWidth { get; set; } = 0.0f;
        public float JawLength { get; set; } = 0.0f;

        public float ChinLowering { get; set; } = 0.0f;
        public float ChinLength { get; set; } = 0.0f;
        public float ChinWidth { get; set; } = 0.0f;
        public float ChinHole { get; set; } = 0.0f;

        public float NeckThickness { get; set; } = 0.0f;
    }
}
