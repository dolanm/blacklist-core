﻿using blacklist_server.bases.factions;
using CitizenFX.Core;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blacklist_server.bases
{
    public class Vendor
    {
        public int id { get; set; } = -1;
        public string title { get; set; } = "";
        public int blipId { get; set; } = 0;
        public Vector3 location { get; set; } = new Vector3(0.0f, 0.0f, 0.0f);
        public float heading { get; set; } = 0.0f;
        public string model { get; set; } = "";
        public bool isHidden { get; set; } = false;
        public List<Faction> factions { get; set; } = new List<Faction>();
        public List<MetroClass> classes { get; set; } = new List<MetroClass>();
        public List<VendorItem> items { get; set; } = new List<VendorItem>();
        public bool useMoney { get; set; } = false;
        public int storedMoney { get; set; } = 0;

        public Vendor()
        {
            this.title = "John Doe";
            this.model = "mp_m_shopkeep_01";
            this.location = new Vector3(0.0f, 0.0f, 0.0f);
            this.factions = new List<Faction>();
            this.classes = new List<MetroClass>();
            this.items = new List<VendorItem>();
        }

        public Vendor(int id)
        {
            this.title = "John Doe";
            this.model = "mp_m_shopkeep_01";
            this.location = new Vector3(0.0f, 0.0f, 0.0f);
            this.factions = new List<Faction>();
            this.classes = new List<MetroClass>();
            this.items = new List<VendorItem>();
            this.load(id);
        }

        public bool Exists()
        {
            return this.id != -1;
        }

        private void load(object id)
        {
            Dictionary<string, object> paras = new Dictionary<string, object>();
            if(id is int)
            {
                paras.Add("id", id);
            }

            MySqlDataReader result = Server.INSTANCE.MYSQL.Select("vendors", paras);
            while(result.Read())
            {
                this.id = result.GetInt32("id");
                this.title = result.GetString("title");
                this.blipId = result.GetInt32("blip");
                this.location = JsonConvert.DeserializeObject<Vector3>(result.GetString("location"));
                this.heading = result.GetFloat("heading");
                this.model = result.GetString("model");
                this.isHidden = result.GetBoolean("isHidden");
                this.factions = JsonConvert.DeserializeObject<List<Faction>>(result.GetString("factions"));
                this.classes = JsonConvert.DeserializeObject<List<MetroClass>>(result.GetString("classes"));
                this.items = JsonConvert.DeserializeObject<List<VendorItem>>(result.GetString("items"));
                this.useMoney = result.GetBoolean("useMoney");
                this.storedMoney = result.GetInt32("storedMoney");
            }
            result.Close();
        }

        public void save()
        {
            if(this.id == -1)
            {
                long idd = Server.INSTANCE.MYSQL.Insert("vendors", new object[] { null, this.title, this.blipId, JsonConvert.SerializeObject(this.location), this.heading, this.model, this.isHidden, JsonConvert.SerializeObject(this.factions), JsonConvert.SerializeObject(this.classes), JsonConvert.SerializeObject(this.items), this.useMoney, this.storedMoney });
                this.id = (int)idd;
                return;
            }
            else
            {
                Dictionary<string, object> tempset = new Dictionary<string, object>();
                tempset.Add("title", this.title);
                tempset.Add("blip", this.blipId);
                tempset.Add("location", JsonConvert.SerializeObject(this.location));
                tempset.Add("heading", this.heading);
                tempset.Add("model", this.model);
                tempset.Add("isHidden", this.isHidden);
                tempset.Add("factions", JsonConvert.SerializeObject(this.factions));
                tempset.Add("classes", JsonConvert.SerializeObject(this.classes));
                tempset.Add("items", JsonConvert.SerializeObject(this.items));
                tempset.Add("useMoney", this.useMoney);
                tempset.Add("storedMoney", this.storedMoney);
                Dictionary<string, object> tempwhere = new Dictionary<string, object>();
                tempwhere.Add("id", this.id);
                int effected = Server.INSTANCE.MYSQL.Update("vendors", tempset, tempwhere);
            }
        }
    }
}
