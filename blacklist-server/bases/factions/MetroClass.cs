﻿using CitizenFX.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blacklist_server.bases.factions
{
    public class MetroClass
    {
        public string ClassName { get; set; }

        public Vector3 SpawnLocation { get; set; } = Vector3.Zero;

        public bool IsDefault { get; set; }

        public MetroClass(string n, Vector3 s, bool def = true)
        {
            this.ClassName = n;
            this.SpawnLocation = s;
            this.IsDefault = def;
        }
    }
}
