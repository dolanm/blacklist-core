﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blacklist_server.bases.skills
{
    public class BlSkill
    {
        public string SkillName { get; set; } = "";
        public string SkillDesc { get; set; } = "";
        public int CurrentLevel { get; set; } = 0;
        public int MaxLevel { get; set; } = 0;
        public bool ShowOnUI { get; set; } = true;

        public BlSkill(string name, string desc, int level, int max, bool show = true)
        {
            this.SkillName = name;
            this.SkillDesc = desc;
            this.CurrentLevel = level;
            this.MaxLevel = max;
            this.ShowOnUI = show;
        }
    }
}
