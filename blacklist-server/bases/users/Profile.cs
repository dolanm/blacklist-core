﻿using blacklist_server.bases.factions;
using blacklist_server.bases.skills;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blacklist_server.bases.users
{
    public class Profile
    {
        public string ProfileName { get; set; }
        public string ProfileDesc { get; set; }

        public int gender { get; set; } = 0;

        public double height { get; set; }

        public int Bal_Wallet { get; set; } = 0;

        public Faction Faction { get; set; }
        public MetroClass Class { get; set; }

        public bool UsesModel { get; set; }
        public MetroModel characterModelInfo { get; set; }

        public MetroHeritage Heritage { get; set; }
        public Outfit Clothing { get; set; }

        public List<BlSkill> Skills = new List<BlSkill>();

        public InventoryItem[] Inventory { get; set; } = new InventoryItem[25];
        public Dictionary<string, string> RecognizedCharacters { get; set; } = new Dictionary<string, string>();


        public int Inventory_GetFirstAvailableSlot()
        {
            var curid = 0;
            foreach(InventoryItem item in this.Inventory)
            {
                if(item == null)
                {
                    return curid;
                }
                curid++;
            }
            return -1;
        }

        public bool Inventory_IsFull()
        {
            return this.Inventory_GetFirstAvailableSlot() == -1;
        }

        public int[] Inventory_GetSlotsWithItem(int id)
        {
            List<int> ids = new List<int>();
            int curslot = 0;
            foreach(InventoryItem i in this.Inventory)
            {
                if(i != null && i.id == id)
                {
                    ids.Add(curslot);
                }
                curslot++;
            }
            return ids.ToArray();
        }

        public void Inventory_AddItem(int slot, BlItem item, int quantity)
        {
            InventoryItem i = new InventoryItem(item.ItemID, quantity);
            this.Inventory[slot] = i;
        }

        public void Inventory_AddItem(int slot, int item, int quantity)
        {
            InventoryItem i = new InventoryItem(item, quantity);
            this.Inventory[slot] = i;
        }

        public void Inventory_AddItem(BlItem item, int quantity)
        {
            var avail = this.Inventory_GetFirstAvailableSlot();
            if(avail != -1)
            {
                this.Inventory_AddItem(avail, item, quantity);
            }
        }

        public void Inventory_AddItem(int item, int quantity)
        {
            var avail = this.Inventory_GetFirstAvailableSlot();
            if(avail != -1)
            {
                this.Inventory_AddItem(avail, item, quantity);
            }
        }

        public void Inventory_AddItem(InventoryItem item)
        {
            int[] slots = this.Inventory_GetSlotsWithItem(item.id);

            int toAdd = item.quantity;
            foreach(int slot in slots)
            {
                InventoryItem invItem = this.Inventory[slot];
                if(invItem != null)
                {
                    BlItem i = Server.INSTANCE.ItemManager.GetItem(item.id);
                    if(i != null && i.ItemID == item.id)
                    {
                        if(invItem.quantity < i.MaxStack)
                        {
                            int bufferOver = (invItem.quantity + item.quantity) - i.MaxStack;
                            if(bufferOver > 0)
                            {
                                var buf = toAdd - bufferOver;
                                toAdd -= buf;
                                invItem.quantity += buf;
                            }
                            else
                            {
                                invItem.quantity += toAdd;
                                return;
                            }
                        }
                    }
                }
            }

            BlItem tempI = Server.INSTANCE.ItemManager.GetItem(item.id);
            while(toAdd > 0)
            {
                if(toAdd > tempI.MaxStack)
                {
                    toAdd -= tempI.MaxStack;
                    int avail = this.Inventory_GetFirstAvailableSlot();
                    if(avail == -1)
                    {
                        return;
                    }
                    this.Inventory_AddItem(tempI.ItemID, tempI.MaxStack);
                }
                else
                {
                    this.Inventory_AddItem(tempI.ItemID, tempI.MaxStack);
                    toAdd = 0;
                }
            }
        }

        public void Inventory_RemoveItem(InventoryItem item)
        {
            var toremove = item.quantity;
            for(var i = 0; i < this.Inventory.Length; i++)
            {
                InventoryItem inventItem = this.Inventory[i];
                if(inventItem != null && inventItem.id == item.id)
                {
                    if(inventItem.quantity > toremove)
                    {
                        inventItem.quantity -= toremove;
                        return;
                    }else if(inventItem.quantity == toremove)
                    {
                        this.Inventory[i] = null;
                        return;
                    }
                    else
                    {
                        var hasRemoved = inventItem.quantity;
                        this.Inventory[i] = null;
                        toremove -= hasRemoved;
                    }
                }
            }
        }

        public Boolean Inventory_HasItemAtLeast(InventoryItem item)
        {
            var total = 0;
            foreach(InventoryItem ni in this.Inventory)
            {
                if(ni != null && ni.id == item.id)
                {
                    total += ni.quantity;
                }
            }
            return total > 0;
        }

        public Boolean Inventory_HasItem(string ItemName)
        {
            bool doesExist = false;
            foreach(var i in this.Inventory)
            {
                if(i != null)
                {
                    BlItem item = Server.INSTANCE.ItemManager.GetItem(ItemName);
                    if(item != null)
                    {
                        if(item.ItemName == ItemName)
                        {
                            doesExist = true;
                        }
                    }
                }
            }
            return doesExist;
        }
    }
}
