﻿using blacklist_server.bases.factions;
using blacklist_server.utils;
using CitizenFX.Core;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace blacklist_server.bases.users
{
    public class User
    {
        public int id { get; set; } = -1;
        public string username { get; set; } = "";
        public string license { get; set; } = "";

        public List<Profile> AllProfiles { get; set; }
        public int SelectedProfile { get; set; } = -1;

        public int ServerID { get; set; }

        public DateTime InputTime { get; set; }

        public User()
        {
            this.AllProfiles = new List<Profile>();
        }

        public User(int id)
        {
            this.AllProfiles = new List<Profile>();
            this.load(id);
        }

        public User(string license)
        {
            this.AllProfiles = new List<Profile>();
            this.license = license;
            this.load(license);
        }

        public bool Exists()
        {
            return this.id != -1;
        }

        public void OnLogin(string username)
        {
            Regex rgx = new Regex("[^a-zA-Z0-9 -]");
            this.username = rgx.Replace(username, "?");
            this.save();
        }

        public Player getPlayer()
        {
            return new PlayerList()[this.ServerID];
        }

        public Profile createProfile(string name, double height, int gender)
        {
            Profile pr = new Profile();

            pr.ProfileName = name;
            pr.height = height;
            pr.gender = gender;

            string gen = "";
            if(gender == 0)
            {
                gen = "Male";
            }
            else
            {
                gen = "Female";
            }

            pr.ProfileDesc = gen + " | " + pr.height.ToString() + "ft | " + Server.INSTANCE.ServerCfg.LoadedConfig["bl_base_defaultCharDesc"];

            if(Server.INSTANCE.FactionManager.GetDefaultFaction() == null)
            {
                Utils.Logging.PrintToConsole("[User]: Attempted to create a new profile while no default faction is set; creating temporary one.");

                Faction tempFaction = new Faction("CREATED_BY_SYSTEM", true);
                tempFaction.AddClass(new MetroClass("SYSTEM_DEFAULT", new CitizenFX.Core.Vector3(0f, 0f, 0f), true));
                pr.Faction = tempFaction;
            }
            else
            {
                pr.Faction = Server.INSTANCE.FactionManager.GetDefaultFaction();
            }

            if(pr.Faction.Classes.Count > 0)
            {
                pr.Class = pr.Faction.Classes[0];
            }
            else
            {
                Utils.Logging.PrintToConsole("[User]: Attempted to create a new profile while no default class is set; creating temporary one.");
                pr.Faction.AddClass(new MetroClass("SYSTEM_DEFAULT", new CitizenFX.Core.Vector3(0f, 0f, 0f), true));
                pr.Class = pr.Faction.Classes[0];
            }

            pr.Bal_Wallet = Convert.ToInt32(Server.INSTANCE.ServerCfg.LoadedConfig["bl_econ_startingBonus"]);

            for(int i = 0; i < Server.INSTANCE.SkillManager.Registered.Count; i++)
            {
                pr.Skills.Add(Server.INSTANCE.SkillManager.Registered[i]);
            }

            this.AllProfiles.Add(pr);
            this.save();
            return pr;
        }

        public void removeProfile(int index)
        {
            this.AllProfiles.RemoveAt(index);
            this.save();
        }

        public Profile GetSelectedProfile()
        {
            if (this.SelectedProfile == -1)
                return null;
            return this.AllProfiles[this.SelectedProfile];
        }

        private void load(object id)
        {
            Dictionary<string, object> paras = new Dictionary<string, object>();
            if(id is int)
            {
                paras.Add("id", id);
            }else if(id is string)
            {
                paras.Add("license", id);
            }

            MySqlDataReader result = Server.INSTANCE.MYSQL.Select("users", paras);
            while(result.Read())
            {
                this.id = result.GetInt32("id");
                this.username = result.GetString("username");
                this.license = result.GetString("license");
                this.AllProfiles = JsonConvert.DeserializeObject<List<Profile>>(result.GetString("profiles"));

                this.InputTime = (DateTime)result["inputtime"];
            }
            result.Close();
        }

        public void save()
        {
            if(this.id == -1)
            {
                this.InputTime = DateTime.Now;
                long idd = Server.INSTANCE.MYSQL.Insert("users", new object[] { null, this.username, this.license, JsonConvert.SerializeObject(this.AllProfiles), this.InputTime });
                this.id = (int)idd;
                return;
            }
            else
            {
                Dictionary<string, object> tempset = new Dictionary<string, object>();
                tempset.Add("username", this.username);
                tempset.Add("license", this.license);
                tempset.Add("profiles", JsonConvert.SerializeObject(this.AllProfiles));
                Dictionary<string, object> tempwhere = new Dictionary<string, object>();
                tempwhere.Add("id", this.id);
                int effected = Server.INSTANCE.MYSQL.Update("users", tempset, tempwhere);
            }
        }

        public void sendProfiles()
        {
            Player p = this.getPlayer();
            if(p != null)
            {
                p.TriggerEvent("bl:CL_E:UserProfiles", JsonConvert.SerializeObject(this.AllProfiles));
                Utils.Logging.PrintToConsole("[User]: Loaded " + this.AllProfiles.Count.ToString() + " profiles for " + p.Name);
            }
        }
    }
}
