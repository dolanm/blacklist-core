﻿CREATE TABLE IF NOT EXISTS `users` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`username` varchar(255) NOT NULL,
	`license` varchar(255) NOT NULL,
	`profiles` text NOT NULL,
	`inputTime` datetime NOT NULL,
	PRIMARY KEY(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `vendors` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`title` varchar(255) NOT NULL,
	`blip` int(11) NOT NULL,
	`location` text NOT NULL,
	`heading` FLOAT NOT NULL,
	`model` varchar(255) NOT NULL,
	`isHidden` tinyint(1) NOT NULL,
	`factions` text NOT NULL,
	`classes` text NOT NULL,
	`items` text NOT NULL,
	`useMoney` tinyint(1) NOT NULL,
	`storedMoney` int(11) NOT NULL,
	PRIMARY KEY(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `bans` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`username` varchar(255) NOT NULL,
	`license` varchar(255) NOT NULL,
	`reason` text NOT NULL,
	`admin` varchar(255) NOT NULL,
	`expires` datetime NOT NULL,
	`inputtime` datetime NOT NULL,
	PRIMARY KEY(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;