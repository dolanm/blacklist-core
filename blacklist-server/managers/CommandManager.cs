﻿using blacklist_server.bases;
using blacklist_server.bases.factions;
using blacklist_server.bases.users;
using blacklist_server.utils;
using CitizenFX.Core;
using CitizenFX.Core.Native;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blacklist_server.managers
{
    public class CommandManager
    {
        public void RegisterCommands()
        {
            #region STAFF COMMANDS
            #region Super Admin
            API.RegisterCommand("devkit", new Action<int, List<object>, string>((source, args, raw) =>
            {

            }), true);

            API.RegisterCommand("createvendor", new Action<int, List<object>, string>((source, args, raw) =>
            {
                if(args.Count > 0)
                {
                    if(source <= 0)
                    {
                        Utils.Logging.PrintToConsole("[CommandManager]: Error running command 'createvendor'. This command can only be ran in game");
                    }
                    else
                    {
                        PlayerList pl = new PlayerList();
                        Player sourcePlayer = pl[source];
                        if(sourcePlayer != null)
                        {
                            sourcePlayer.TriggerEvent("bl:CL_E:Vendor:CreateCallback", args[0].ToString(), Boolean.Parse(args[1].ToString()), Int32.Parse(args[2].ToString()));
                        }
                    }
                }
            }), true);

            API.RegisterCommand("createitem", new Action<int, List<object>, string>((source, args, raw) =>
            {
                //id, item, amount
                if(args.Count > 0)
                {
                    PlayerList pl = new PlayerList();
                    if(source <= 0)
                    {
                        Player targetPlayer = pl[Int32.Parse(args[0].ToString())];
                        User targetUser = Server.INSTANCE.UserManager.GetPlayerFromID(Int32.Parse(args[0].ToString()));

                        if (targetUser != null)
                        {
                            if (targetUser.GetSelectedProfile() != null)
                            {
                                if(!(targetUser.GetSelectedProfile().Inventory_IsFull()))
                                {
                                    if(Server.INSTANCE.ItemManager.References.ContainsKey(args[1].ToString()))
                                    {
                                        BlItem tempItem = Server.INSTANCE.ItemManager.References[args[1].ToString()];
                                        targetUser.GetSelectedProfile().Inventory_AddItem(new InventoryItem(tempItem.ItemID, Int32.Parse(args[2].ToString())));

                                        targetUser.save();
                                        targetUser.sendProfiles();

                                        Utils.Logging.PrintToConsole("Console gave (" + targetPlayer.Name + ") " + targetUser.GetSelectedProfile().ProfileName + " x" + args[2].ToString() + " " + tempItem.ItemName);
                                        Utils.Logging.PrintToDiscord("Console gave (" + targetPlayer.Name + ") " + targetUser.GetSelectedProfile().ProfileName + " x" + args[2].ToString() + " " + tempItem.ItemName);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        Player sourcePlayer = pl[source];
                        Player targetPlayer = pl[Int32.Parse(args[0].ToString())];

                        User sourceUser = Server.INSTANCE.UserManager.GetPlayerFromID(source);
                        User targetUser = Server.INSTANCE.UserManager.GetPlayerFromID(Int32.Parse(args[0].ToString()));
                        if (sourceUser != null && targetUser != null)
                        {
                            if (sourceUser.GetSelectedProfile() != null && targetUser.GetSelectedProfile() != null)
                            {
                                if (!(targetUser.GetSelectedProfile().Inventory_IsFull()))
                                {
                                    if (Server.INSTANCE.ItemManager.References.ContainsKey(args[1].ToString()))
                                    {
                                        BlItem tempItem = Server.INSTANCE.ItemManager.References[args[1].ToString()];
                                        targetUser.GetSelectedProfile().Inventory_AddItem(new InventoryItem(tempItem.ItemID, Int32.Parse(args[2].ToString())));

                                        targetUser.save();
                                        targetUser.sendProfiles();

                                        Utils.Logging.PrintToConsole("(" + sourcePlayer.Name + ") " + sourceUser.GetSelectedProfile().ProfileName + " gave (" + targetPlayer.Name + ") " + targetUser.GetSelectedProfile().ProfileName + " x" + args[2].ToString() + " " + tempItem.ItemName);
                                        Utils.Logging.PrintToDiscord("(" + sourcePlayer.Name + ") " + sourceUser.GetSelectedProfile().ProfileName + " gave (" + targetPlayer.Name + ") " + targetUser.GetSelectedProfile().ProfileName + " x" + args[2].ToString() + " " + tempItem.ItemName);
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    if(source <= 0)
                    {
                        Utils.Logging.PrintToConsole("[CommandManager]: Error running command 'createitem'. Usage: <id> <item> <amount>");
                    }
                    else
                    {
                        PlayerList pl = new PlayerList();
                        Player sourcePlayer = pl[source];

                        sourcePlayer.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[CommandManager]", "Error running command 'createitem'. Usage: <id> <item> <amount>" } });
                    }
                }
            }), true);

            API.RegisterCommand("charban", new Action<int, List<object>, string>((source, args, raw) =>
            {

            }), true);

            API.RegisterCommand("money", new Action<int, List<object>, string>((source, args, raw) =>
            {
                if(args.Count > 0)
                {
                    PlayerList pl = new PlayerList();

                    if(args[0].ToString().ToLower() == "view")
                    {
                        Player targetPlayer = pl[Int32.Parse(args[1].ToString())];

                        User targetUser = Server.INSTANCE.UserManager.GetPlayerFromID(Int32.Parse(args[1].ToString()));
                        if(targetUser != null)
                        {
                            if(targetUser.GetSelectedProfile() != null)
                            {
                                if(source <= 0)
                                {
                                    Utils.Logging.PrintToConsole("(" + targetPlayer.Name + ") " + targetUser.GetSelectedProfile().ProfileName + " has a balance of $" + targetUser.GetSelectedProfile().Bal_Wallet);
                                }
                                else
                                {
                                    Player sourcePlayer = pl[source];
                                    if(sourcePlayer != null)
                                    {
                                        sourcePlayer.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[CommandManager]", "(" + targetPlayer.Name + ") " + targetUser.GetSelectedProfile().ProfileName + " has a balance of $" + targetUser.GetSelectedProfile().Bal_Wallet } });
                                    }
                                }
                            }
                        }
                    }else if(args[0].ToString().ToLower() == "set")
                    {
                        Player targetPlayer = pl[Int32.Parse(args[1].ToString())];

                        User targetUser = Server.INSTANCE.UserManager.GetPlayerFromID(Int32.Parse(args[1].ToString()));
                        if (targetUser != null)
                        {
                            if (targetUser.GetSelectedProfile() != null)
                            {
                                if (source <= 0)
                                {
                                    if(Int32.Parse(args[2].ToString()) > 0)
                                    {
                                        targetUser.GetSelectedProfile().Bal_Wallet = Int32.Parse(args[2].ToString());
                                        targetUser.save();
                                        targetUser.sendProfiles();
                                        Utils.Logging.PrintToConsole("(" + targetPlayer.Name + ") " + targetUser.GetSelectedProfile().ProfileName + " has been given $" + args[2].ToString());
                                    }
                                    else
                                    {
                                        Utils.Logging.PrintToConsole("Error running command 'money'. Usage: <view/set> <id> <amount>");
                                    }
                                }
                                else
                                {
                                    Player sourcePlayer = pl[source];
                                    if (sourcePlayer != null)
                                    {
                                        if (Int32.Parse(args[2].ToString()) > 0)
                                        {
                                            targetUser.GetSelectedProfile().Bal_Wallet = Int32.Parse(args[2].ToString());
                                            targetUser.save();
                                            targetUser.sendProfiles();
                                            sourcePlayer.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[CommandManager]", "(" + targetPlayer.Name + ") " + targetUser.GetSelectedProfile().ProfileName + " has been given $" + args[2].ToString() } });
                                        }
                                        else
                                        {
                                            sourcePlayer.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[CommandManager]", "Error running command 'money'. Usage: <view/set> <id> <amount>" } });
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (source <= 0)
                    {
                        Utils.Logging.PrintToConsole("[CommandManager]: Error running command 'money'. Usage: <view/set> <id> <amount>");
                    }
                    else
                    {
                        PlayerList pl = new PlayerList();
                        Player sourcePlayer = pl[source];

                        sourcePlayer.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[CommandManager]", "Error running command 'money'. Usage: <view/set> <id> <amount>" } });
                    }
                }
            }), true);
            #endregion

            #region Admin
            API.RegisterCommand("staffmode", new Action<int, List<object>, string>((source, args, raw) =>
            {

            }), true);

            API.RegisterCommand("charsetname", new Action<int, List<object>, string>((source, args, raw) =>
            {

            }), true);

            API.RegisterCommand("setmodel", new Action<int, List<object>, string>((source, args, raw) =>
            {

            }), true);

            API.RegisterCommand("bodygroup", new Action<int, List<object>, string>((source, args, raw) =>
            {

            }), true);

            API.RegisterCommand("notify", new Action<int, List<object>, string>((source, args, raw) =>
            {
                if (args.Count > 0)
                {
                    string side = args[0].ToString();
                    args.RemoveAt(0);
                    string FullMsg = "";
                    foreach (string arg in args)
                    {
                        FullMsg = FullMsg + arg + " ";
                    }

                    PlayerList pl = new PlayerList();
                    foreach (Player p in pl)
                    {
                        p.TriggerEvent("bl:CL_E:NUI:Notify", FullMsg, side);
                    }
                }
            }), true);
            #endregion

            #region Moderator
            API.RegisterCommand("setclass", new Action<int, List<object>, string>((source, args, raw) =>
            {
                if(args.Count > 0)
                {
                    PlayerList pl = new PlayerList();
                    Player targetPlayer = pl[Int32.Parse(args[0].ToString())];

                    User targetUser = Server.INSTANCE.UserManager.GetPlayerFromID(Int32.Parse(args[0].ToString()));
                    if(targetUser != null && targetUser.GetSelectedProfile() != null)
                    {
                        if(source <= 0)
                        {
                            MetroClass foundClass = null;

                            //Console
                            foreach(MetroClass c in targetUser.GetSelectedProfile().Faction.Classes)
                            {
                                if(c.ClassName.ToLower() == args[1].ToString().ToLower())
                                {
                                    foundClass = c;
                                }
                            }

                            if(foundClass != null)
                            {
                                targetUser.GetSelectedProfile().Class = foundClass;
                                targetUser.save();
                                targetUser.sendProfiles();

                                Utils.Logging.PrintToConsole("(" + targetPlayer.Name + ") " + targetUser.GetSelectedProfile().ProfileName + " has been set to class '" + foundClass.ClassName + "'");
                            }
                            else
                            {
                                Utils.Logging.PrintToConsole("[CommandManager]: Error finding specified class '" + args[1].ToString() + "'");
                            }
                        }
                        else
                        {
                            //Player
                            MetroClass foundClass = null;
                            Player sourcePlayer = pl[source];

                            //Console
                            foreach (MetroClass c in targetUser.GetSelectedProfile().Faction.Classes)
                            {
                                if (c.ClassName.ToLower() == args[1].ToString().ToLower())
                                {
                                    foundClass = c;
                                }
                            }

                            if (foundClass != null)
                            {
                                targetUser.GetSelectedProfile().Class = foundClass;
                                targetUser.save();
                                targetUser.sendProfiles();

                                sourcePlayer.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[CommandManager]", "(" + targetPlayer.Name + ") " + targetUser.GetSelectedProfile().ProfileName + " has been set to class '" + foundClass.ClassName + "'" } });
                            }
                            else
                            {
                                sourcePlayer.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[CommandManager]", "Error finding specified class '" + args[1].ToString() + "'" } });
                            }
                        }
                    }
                }
            }), true);

            API.RegisterCommand("plytransfer", new Action<int, List<object>, string>((source, args, raw) =>
            {
                if(args.Count > 0)
                {
                    //Do things
                    PlayerList pl = new PlayerList();
                    Player targetPlayer = pl[Int32.Parse(args[0].ToString())];

                    User targetUser = Server.INSTANCE.UserManager.GetPlayerFromID(Int32.Parse(args[0].ToString()));

                    if (targetUser != null)
                    {
                        if (targetUser.GetSelectedProfile() != null)
                        {
                            if(source <= 0)
                            {
                                if (Server.INSTANCE.FactionManager.Registered.Count > 0)
                                {
                                    string faction_id = "";
                                    //Check for valid faction
                                    if (args[1].ToString().Contains("_"))
                                    {
                                        faction_id = args[1].ToString().Replace('_', ' ');
                                    }
                                    else
                                    {
                                        faction_id = args[1].ToString();
                                    }

                                    if (Server.INSTANCE.FactionManager.GetFaction(faction_id) != null)
                                    {
                                        targetUser.GetSelectedProfile().Faction = Server.INSTANCE.FactionManager.GetFaction(faction_id);
                                        targetUser.save();
                                        targetUser.sendProfiles();

                                        Utils.Logging.PrintToConsole("[CommandManager]: (" + targetPlayer.Name + ") " + targetUser.GetSelectedProfile().ProfileName + " has been tranfered to faction '" + faction_id + "'");
                                    }
                                }
                                else
                                {
                                    Utils.Logging.PrintToConsole("[CommandManager]: Error running command 'plytransfer'. Invalid faction specified in arguments");
                                }
                            }
                            else
                            {
                                Player sourcePlayer = pl[source];

                                if (Server.INSTANCE.FactionManager.Registered.Count > 0)
                                {
                                    string faction_id = "";
                                    //Check for valid faction
                                    if (args[1].ToString().Contains("_"))
                                    {
                                        faction_id = args[1].ToString().Replace('_', ' ');
                                    }
                                    else
                                    {
                                        faction_id = args[1].ToString();
                                    }

                                    if (Server.INSTANCE.FactionManager.GetFaction(faction_id) != null)
                                    {
                                        targetUser.GetSelectedProfile().Faction = Server.INSTANCE.FactionManager.GetFaction(faction_id);
                                        targetUser.save();
                                        targetUser.sendProfiles();

                                        sourcePlayer.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[CommandManager]", "(" + targetPlayer.Name + ") " + targetUser.GetSelectedProfile().ProfileName + " has been tranfered to faction '" + faction_id + "'" } });
                                    }
                                }
                                else
                                {
                                    sourcePlayer.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[CommandManager]", "Error running command 'plytransfer'. Invalid faction specified in arguments" } });
                                }
                            }
                        }
                    }
                }else{
                    if (source <= 0)
                    {
                        Utils.Logging.PrintToConsole("[CommandManager]: Error running command 'plytransfer'. Usage: <id> <faction>");
                    }
                    else
                    {
                        PlayerList pl = new PlayerList();
                        Player sourcePlayer = pl[source];

                        sourcePlayer.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[CommandManager]", "Error running command 'money'. Usage: <view/set> <id> <amount>" } });
                    }
                }
            }), true);

            API.RegisterCommand("goto", new Action<int, List<object>, string>((source, args, raw) =>
            {

            }), true);

            API.RegisterCommand("bring", new Action<int, List<object>, string>((source, args, raw) =>
            {

            }), true);

            API.RegisterCommand("return", new Action<int, List<object>, string>((source, args, raw) =>
            {

            }), true);

            API.RegisterCommand("respond", new Action<int, List<object>, string>((source, args, raw) =>
            {
                PlayerList pl = new PlayerList();
                Player sourcePlayer = pl[source];
                if (args.Count > 0)
                {
                    int targetId = Int32.Parse(args[0].ToString());
                    Player targetPlayer = pl[targetId];
                    if(targetPlayer != null)
                    {
                        args.RemoveAt(0);
                        StringBuilder sb = new StringBuilder();
                        foreach(string a in args)
                        {
                            sb.Append(a + " ");
                        }
                        targetPlayer.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[Response]", sb.ToString() } });
                    }
                }
                else
                {
                    sourcePlayer.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[CommandManager]", "Error running command 'respond'. Usage: <id> <msg>" } });
                }
            }), true);

            API.RegisterCommand("ban", new Action<int, List<object>, string>((source, args, raw) =>
            {

            }), true);

            API.RegisterCommand("kick", new Action<int, List<object>, string>((source, args, raw) =>
            {
                if(args.Count > 0)
                {
                    PlayerList pl = new PlayerList();
                    if(source <= 0)
                    {
                        Player targetPlayer = pl[Int32.Parse(args[0].ToString())];
                        if(targetPlayer != null)
                        {
                            args.RemoveAt(0);
                            StringBuilder sb = new StringBuilder();
                            foreach(string a in args)
                            {
                                sb.Append(a + " ");
                            }
                            Utils.Logging.PrintToConsole("[CommandManager]: Player '" + targetPlayer.Name + "' has been kicked by Console");
                            targetPlayer.Drop("You have been kicked.\r\n------------------------\r\nKicked By: Console\r\nKick Reason: " + sb.ToString());
                        }
                    }
                    else
                    {
                        Player sourcePlayer = pl[source];
                        if(sourcePlayer != null)
                        {
                            Player targetPlayer = pl[Int32.Parse(args[0].ToString())];
                            if (targetPlayer != null)
                            {
                                args.RemoveAt(0);
                                StringBuilder sb = new StringBuilder();
                                foreach (string a in args)
                                {
                                    sb.Append(a + " ");
                                }
                                Utils.Logging.PrintToConsole("[CommandManager]: Player '" + targetPlayer.Name + "' has been kicked by " + sourcePlayer.Name);
                                targetPlayer.Drop("You have been kicked.\r\n------------------------\r\nKicked By: " + sourcePlayer.Name + "\r\nKick Reason: " + sb.ToString());
                            }
                        }
                    }
                }
            }), true);
            #endregion
            #endregion

            API.RegisterCommand("help", new Action<int, List<object>, string>((source, args, raw) =>
            {
                PlayerList pl = new PlayerList();
                Player sourcePlayer = pl[source];
                if (args.Count > 0)
                {
                    if(sourcePlayer != null)
                    {
                        StringBuilder sb = new StringBuilder();
                        foreach(string a in args)
                        {
                            sb.Append(a + " ");
                        }

                        foreach(Player p in pl)
                        {
                            if(p != sourcePlayer)
                            {
                                if(Server.INSTANCE.PermissionManager.SV_HasPerms(p, "staff.receivehelp"))
                                {
                                    p.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[Help](" + sourcePlayer.Name + " #" + source.ToString() + ")", sb.ToString() } });
                                }
                            }
                        }

                        sourcePlayer.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[Help](" + sourcePlayer.Name + " #" + source.ToString() + ")", sb.ToString() } });
                    }
                }
                else
                {
                    sourcePlayer.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^1[CommandManager]",  "Error running command 'help'. Usage: <msg>"} });
                }
            }), false);
        }
    }
}
