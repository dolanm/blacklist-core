﻿using blacklist_server.bases.factions;
using blacklist_server.utils;
using CitizenFX.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blacklist_server.managers
{
    public class FactionManager
    {
        public List<Faction> Registered = new List<Faction>();

        public FactionManager()
        {
            Utils.Logging.PrintToConsole("[FactionManager]: Initializing...");

            #region Add Core Factions
            Faction Staff = new Faction("Staff", false);
            Staff.AddClass(new MetroClass("default", new Vector3(0.0f, 0.0f, 0.0f), false));
            this.RegisterFaction(Staff);
            #endregion

            Utils.Logging.PrintToConsole("[FactionManager]: Done!");
        }

        public void RegisterFaction(dynamic f)
        {
            if (f != null)
            {
                this.Registered.Add(f);
                Utils.Logging.PrintToConsole("[FactionManager]: Registered faction '" + f.FactionName + "'");
            }
            else
            {
                Utils.Logging.PrintToConsole("[FactionManager]: Could not register null faction...");
            }
        }

        public void RegisterFactionClass(dynamic c, string facName)
        {
            if(c != null)
            {
                if(this.GetFaction(facName) != null)
                {
                    this.GetFaction(facName).AddClass(c);
                }
            }
        }

        public Faction GetDefaultFaction()
        {
            Faction returnedFaction = null;
            if(this.Registered.Count > 0)
            {
                foreach(Faction f in this.Registered)
                {
                    if(f.IsDefault)
                    {
                        returnedFaction = f;
                    }
                }
            }
            return returnedFaction;
        }

        public Faction GetFaction(string name)
        {
            if(this.Registered.Count > 0)
            {
                Faction f = null;
                foreach(Faction fac in this.Registered)
                {
                    if(fac.FactionName.ToLower() == name.ToLower())
                    {
                        f = fac;
                    }
                }
                return f;
            }
            else
            {
                return null;
            }
        }
    }
}
