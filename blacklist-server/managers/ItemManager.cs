﻿using blacklist_server.bases;
using blacklist_server.bases.users;
using blacklist_server.utils;
using CitizenFX.Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blacklist_server.managers
{
    public class ItemManager
    {
        public List<BlItem> Registered { get; set; }
        public Dictionary<String, BlItem> References = new Dictionary<string, BlItem>();
        //public List<DroppedItem> WorldItems = new List<DroppedItem>();
        public Dictionary<string, DroppedItem> WorldItems = new Dictionary<string, DroppedItem>();

        public ItemManager()
        {
            this.Registered = new List<BlItem>();

            Utils.Logging.PrintToConsole("[ItemManager]: Initializing...");
            #region Add Core Items
            #endregion
            Utils.Logging.PrintToConsole("[ItemManager]: Done!");
        }

        public void RegisterItem(BlItem i)
        {
            if(i != null)
            {
                this.Registered.Add(i);
                if(i.ItemName.Contains(" "))
                {
                    string fixedName = i.ItemName.Replace(' ', '_');
                    this.References.Add(fixedName, i);
                }
                else
                {
                    this.References.Add(i.ItemName, i);
                }
                Utils.Logging.PrintToConsole("[ItemManager]: Registered item '" + i.ItemName + "'");
            }
            else
            {
                Utils.Logging.PrintToConsole("[ItemManager]: Could not register null item...");
            }
        }

        public BlItem GetItem(string id)
        {
            if(id != "")
            {
                if (this.References.ContainsKey(id))
                {
                    return this.References[id];
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public BlItem GetItem(int id)
        {
            if(id != -1)
            {
                if(this.References.Count > 0)
                {
                    BlItem returnedItem = null;
                    foreach(KeyValuePair<string, BlItem> rItem in this.References)
                    {
                        if(rItem.Value.ItemID == id)
                        {
                            returnedItem = rItem.Value;
                        }
                    }
                    return returnedItem;
                }
            }
            return null;
        }

        public void UseItem([FromSource]Player p, int ItemID)
        {
            if(ItemID != -1)
            {
                User u = Server.INSTANCE.UserManager.GetPlayerFromID(Int32.Parse(p.Handle));
                if(u != null && u.GetSelectedProfile() != null)
                {
                    Profile pr = u.GetSelectedProfile();
                    if(pr.Inventory_HasItemAtLeast(new InventoryItem(ItemID, 1)))
                    {
                        BlItem tempItem = this.GetItem(ItemID);
                        if(tempItem != null)
                        {
                            tempItem.Use(Int32.Parse(p.Handle));
                            pr.Inventory_RemoveItem(new InventoryItem(ItemID, 1));

                            u.save();
                            u.sendProfiles();

                            p.TriggerEvent("bl:CL_E:NUI:Notify", "You have used " + tempItem.ItemName, "right");

                            Utils.Logging.PrintToConsole("(" + p.Name + ") " + pr.ProfileName + " used item(" + tempItem.ItemName + ")");
                            Utils.Logging.PrintToDiscord("(" + p.Name + ") " + pr.ProfileName + " used item(" + tempItem.ItemName + ")");
                        }
                    }
                }
            }
        }

        public void PickupItem([FromSource]Player p, string uuid, string itemJson)
        {
            if(!(String.IsNullOrWhiteSpace(itemJson)) && !(String.IsNullOrWhiteSpace(uuid)))
            {
                DroppedItem droppedItem = JsonConvert.DeserializeObject<DroppedItem>(itemJson);
                if(this.WorldItems.ContainsKey(uuid))
                {
                    if (Server.INSTANCE.UserManager.GetPlayerFromID(Int32.Parse(p.Handle)) != null)
                    {
                        User u = Server.INSTANCE.UserManager.GetPlayerFromID(Int32.Parse(p.Handle));
                        if (u.GetSelectedProfile() != null && !u.GetSelectedProfile().Inventory_IsFull())
                        {
                            Profile pr = u.GetSelectedProfile();
                            pr.Inventory_AddItem(new InventoryItem(droppedItem.ItemID, droppedItem.ItemAmount));
                            this.WorldItems.Remove(uuid);

                            p.TriggerEvent("bl:CL_E:NUI:Notify", "You have picked up " + droppedItem.ItemName, "right");

                            u.save();
                            u.sendProfiles();

                            PlayerList pl = new PlayerList();
                            foreach (Player pp in pl)
                            {
                                pp.TriggerEvent("bl:CL_E:Inventory:ItemPickedUp", uuid, itemJson);
                            }

                            Utils.Logging.PrintToConsole("(" + p.Name + ") " + pr.ProfileName + " picked up item: " + itemJson);
                            Utils.Logging.PrintToDiscord("(" + p.Name + ") " + pr.ProfileName + " picked up item: " + itemJson);
                        }
                    }
                }
                else
                {
                    Utils.Logging.PrintToConsole("(" + p.Name + ") tried to pickup an invalid item: " + itemJson);
                    Utils.Logging.PrintToDiscord("(" + p.Name + ") tried to pickup an invalid item: " + itemJson);
                }
                /*if(this.WorldItems.Contains(droppedItem))
                {
                    if(Server.INSTANCE.UserManager.GetPlayerFromID(Int32.Parse(p.Handle)) != null)
                    {
                        User u = Server.INSTANCE.UserManager.GetPlayerFromID(Int32.Parse(p.Handle));
                        if(u.GetSelectedProfile() != null && !u.GetSelectedProfile().Inventory_IsFull())
                        {
                            Profile pr = u.GetSelectedProfile();
                            pr.Inventory_AddItem(new InventoryItem(droppedItem.ItemID, droppedItem.ItemAmount));
                            this.WorldItems.Remove(droppedItem);

                            p.TriggerEvent("bl:CL_E:NUI:Notify", "You have picked up " + droppedItem.ItemName, "right");

                            PlayerList pl = new PlayerList();
                            foreach(Player pp in pl)
                            {
                                pp.TriggerEvent("bl:CL_E:Inventory:ItemPickedUp", itemJson);
                            }

                            Utils.Logging.PrintToConsole("(" + p.Name + ") " + pr.ProfileName + " picked up item: " + itemJson);
                            Utils.Logging.PrintToDiscord("(" + p.Name + ") " + pr.ProfileName + " picked up item: " + itemJson);
                        }
                    }
                }
                else
                {
                    Utils.Logging.PrintToConsole("(" + p.Name + ") tried to pickup an invalid item: " + itemJson);
                    Utils.Logging.PrintToDiscord("(" + p.Name + ") tried to pickup an invalid item: " + itemJson);
                }*/
            }
        }

        public void DropItem([FromSource]Player p, Vector3 pos, int ItemID, int amount)
        {
            if(ItemID != -1 && amount > 0)
            {
                User u = Server.INSTANCE.UserManager.GetPlayerFromID(Int32.Parse(p.Handle));
                if(u != null && u.GetSelectedProfile() != null)
                {
                    BlItem tempItem = this.GetItem(ItemID);
                    Profile pr = u.GetSelectedProfile();
                    if(tempItem != null)
                    {
                        if (pr.Inventory_HasItemAtLeast(new InventoryItem(ItemID, amount)))
                        {
                            pr.Inventory_RemoveItem(new InventoryItem(ItemID, amount));
                            DroppedItem di = new DroppedItem(ItemID, amount, pos);

                            Guid uuid = Guid.NewGuid();
                            this.WorldItems.Add(uuid.ToString(), di);

                            PlayerList pl = new PlayerList();
                            foreach (Player pp in pl)
                            {
                                User uu = Server.INSTANCE.UserManager.GetPlayerFromID(Int32.Parse(pp.Handle));
                                if (uu != null && uu.GetSelectedProfile() != null)
                                {
                                    //pp.TriggerEvent("bl:CL_E:Inventory:ItemDropped", pos, ItemID, amount);
                                    pp.TriggerEvent("bl:CL_E:Inventory:ItemDropped", uuid.ToString(), JsonConvert.SerializeObject(di));
                                }
                            }

                            //this.WorldItems.Add(di);
                            //this.WorldItems.Add(new DroppedItem(ItemID, amount, pos));

                            u.save();
                            u.sendProfiles();

                            if (amount > 1)
                            {
                                p.TriggerEvent("bl:CL_E:NUI:Notify", "You have dropped " + amount + " " + tempItem.ItemName + "s", "right");
                            }
                            else
                            {
                                p.TriggerEvent("bl:CL_E:NUI:Notify", "You have dropped " + amount + " " + tempItem.ItemName, "right");
                            }

                            Utils.Logging.PrintToConsole("(" + p.Name + ") " + pr.ProfileName + " dropped item(" + tempItem.ItemName + ") at " + JsonConvert.SerializeObject(pos));
                            Utils.Logging.PrintToDiscord("(" + p.Name + ") " + pr.ProfileName + " dropped item(" + tempItem.ItemName + ") at " + JsonConvert.SerializeObject(pos));
                        }
                        else
                        {
                            p.TriggerEvent("bl:CL_E:NUI:Notify", "You do not have enough of this item", "right");
                        }
                    }
                }
            }
        }
    }
}
