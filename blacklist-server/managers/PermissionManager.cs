﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blacklist_server.managers
{
    public class PermissionManager
    {
        public void CL_HasPerms([FromSource]Player p, string node)
        {
            if(!String.IsNullOrWhiteSpace(p.Handle) && !String.IsNullOrWhiteSpace(node))
            {
                if(API.IsPlayerAceAllowed(p.Handle, node)){
                    p.TriggerEvent("bl:CL_E:HasPerm", node, true);
                }
                else
                {
                    p.TriggerEvent("bl:CL_E:HasPerm", node, false);
                }
            }
        }

        public bool SV_HasPerms(Player p, string node)
        {
            if (!String.IsNullOrWhiteSpace(p.Handle) && !String.IsNullOrWhiteSpace(node))
            {
                if (API.IsPlayerAceAllowed(p.Handle, node))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }
}
