﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using blacklist_server.bases.skills;
using blacklist_server.utils;

namespace blacklist_server.managers
{
    public class SkillManager
    {
        public List<BlSkill> Registered { get; set; }
        public Dictionary<string, BlSkill> References = new Dictionary<string, BlSkill>();

        public SkillManager()
        {
            this.Registered = new List<BlSkill>();

            Utils.Logging.PrintToConsole("[SkillManager]: Initializing...");
            #region Add Core Skills
            #endregion
            Utils.Logging.PrintToConsole("[SkillManager]: Done!");
        }

        public void RegisterSkill(BlSkill s)
        {
            if(s != null)
            {
                this.Registered.Add(s);
                if(s.SkillName.Contains(" "))
                {
                    string fixedName = s.SkillName.Replace(' ', '_');
                    this.References.Add(fixedName, s);
                }
                else
                {
                    this.References.Add(s.SkillName, s);
                }
                Utils.Logging.PrintToConsole("[SkillManager]: Registered skill '" + s.SkillName + "'");
            }
            else
            {
                Utils.Logging.PrintToConsole("[SkillManager]: Could not register null skill...");
            }
        }

        public BlSkill GetSkill(string id)
        {
            if (id != "")
            {
                if (this.References.ContainsKey(id))
                {
                    return this.References[id];
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
    }
}
