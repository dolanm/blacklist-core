﻿using blacklist_server.bases.users;
using blacklist_server.utils;
using CitizenFX.Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blacklist_server.managers
{
    public class UserManager
    {
        public List<User> OnlinePlayers { get; set; }

        public UserManager()
        {
            Utils.Logging.PrintToConsole("[UserManager]: Initializing...");
            this.OnlinePlayers = new List<User>();
            Utils.Logging.PrintToConsole("[UserManager]: Done!");
        }

        public void onPlayerReady(Player p)
        {
            Utils.Logging.PrintToConsole("[UserManager]: " + p.Name + " is ready!");
            Dictionary<string, string> ids = Utils.GetProperPlayerIDs(p);

            User tempUser = new User(ids["license"]);
            tempUser.ServerID = Convert.ToInt32(p.Handle);

            tempUser.OnLogin(p.Name);
            tempUser.sendProfiles();
            this.OnlinePlayers.Add(tempUser);
            p.TriggerEvent("bl:CL_E:ServerReady", JsonConvert.SerializeObject(Server.INSTANCE.ServerCfg.LoadedConfig), JsonConvert.SerializeObject(Server.INSTANCE.ServerCfg.PM_S), JsonConvert.SerializeObject(Server.INSTANCE.VendorManager.RegisteredVendors), JsonConvert.SerializeObject(Server.INSTANCE.ItemManager.Registered), JsonConvert.SerializeObject(Server.INSTANCE.FactionManager.Registered));
            Utils.Logging.PrintToConsole("[UserManager]: " + tempUser.username + " has connected!");
        }

        public User GetPlayerFromID(int id)
        {
            foreach(User u in this.OnlinePlayers)
            {
                if(u.ServerID == id)
                {
                    return u;
                }
            }
            return null;
        }
    }
}
