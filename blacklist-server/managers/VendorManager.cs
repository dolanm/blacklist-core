﻿using blacklist_server.bases;
using blacklist_server.bases.factions;
using blacklist_server.bases.users;
using blacklist_server.utils;
using CitizenFX.Core;
using CitizenFX.Core.Native;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blacklist_server.managers
{
    public enum VendorItemMode
    {
        BOTH = 0,
        BUY_ONLY = 1,
        SELL_ONLY = 2
    }

    public class VendorManager
    {
        public List<Vendor> RegisteredVendors = new List<Vendor>();

        public VendorManager()
        {
            List<int> vendorIds = new List<int>();
            MySqlDataReader result = Server.INSTANCE.MYSQL.Select("vendors");
            if(result.HasRows)
            {
                while(result.Read())
                {
                    vendorIds.Add(result.GetInt32("id"));
                }
            }
            result.Close();
            if(vendorIds.Count > 0)
            {
                foreach(int i in vendorIds)
                {
                    Vendor tempVendor = new Vendor(i);
                    this.RegisteredVendors.Add(tempVendor);
                }
            }

            Utils.Logging.PrintToConsole("[VendorManager]: Loaded " + this.RegisteredVendors.Count + " vendors.");
        }

        public void CreateVendor([FromSource]Player p, string model, bool hidden, int bl, Vector3 pos, float head)
        {
            /*Vendor newVendor = new Vendor();
            newVendor.save();
            this.RegisteredVendors.Add(newVendor);*/

            if(Server.INSTANCE.PermissionManager.SV_HasPerms(p, "vendors.create"))
            {
                if(!(String.IsNullOrWhiteSpace(model)))
                {
                    Vendor v = new Vendor();
                    v.model = model;
                    v.location = pos;
                    v.heading = head;
                    v.isHidden = hidden;
                    v.blipId = bl;
                    v.title = "Vendor";
                    v.save();
                    this.RegisteredVendors.Add(v);

                    PlayerList pl = new PlayerList();
                    foreach(Player pp in pl)
                    {
                        pp.TriggerEvent("bl:CL_E:Vendor:UpdateList", JsonConvert.SerializeObject(this.RegisteredVendors));
                    }

                    Utils.Logging.PrintToConsole("[VendorManager]: (" + p.Name + ") created a new vendor at location " + JsonConvert.SerializeObject(pos));
                }
            }
        }

        public void AttemptSell([FromSource]Player p, int vendorId, int itemIndex)
        {
            if(this.RegisteredVendors.Count > 0)
            {
                if(this.RegisteredVendors[vendorId - 1] != null)
                {
                    Vendor v = this.RegisteredVendors[vendorId - 1];
                    if(v.items.Count > 0)
                    {
                        User u = Server.INSTANCE.UserManager.GetPlayerFromID(Int32.Parse(p.Handle));
                        if(u != null && u.GetSelectedProfile() != null)
                        {
                            Profile pr = u.GetSelectedProfile();
                            if(v.items[itemIndex] != null)
                            {
                                VendorItem vi = v.items[itemIndex];
                                BlItem i = Server.INSTANCE.ItemManager.GetItem(vi.baseId);
                                if (pr.Inventory_HasItemAtLeast(new InventoryItem(vi.baseId, 1)))
                                {
                                    if(vi.useStock)
                                    {
                                        if(!(vi.currentStock >= vi.maxStock))
                                        {
                                            pr.Inventory_RemoveItem(new InventoryItem(vi.baseId, 1));
                                            pr.Bal_Wallet += vi.sellPrice;
                                            vi.currentStock += 1;

                                            u.save();
                                            u.sendProfiles();
                                            v.save();
                                            this.SendVendorItems(p, v.id);

                                            p.TriggerEvent("bl:CL_E:NUI:Notify", "Your have sold a " + vi.name, "right");

                                            Utils.Logging.PrintToConsole("(" + p.Name + ") " + u.GetSelectedProfile().ProfileName + " sold item(" + i.ItemName + ") from vendor(" + vendorId + ")");
                                            Utils.Logging.PrintToDiscord("(" + p.Name + ") " + u.GetSelectedProfile().ProfileName + " sold item(" + i.ItemName + ") from vendor(" + vendorId + ")");
                                        }
                                        else
                                        {
                                            p.TriggerEvent("bl:CL_E:NUI:Notify", "This items stock is full", "right");
                                        }
                                    }
                                    else
                                    {
                                        pr.Inventory_RemoveItem(new InventoryItem(vi.baseId, 1));
                                        pr.Bal_Wallet += vi.sellPrice;

                                        u.save();
                                        u.sendProfiles();
                                        v.save();
                                        this.SendVendorItems(p, v.id);

                                        p.TriggerEvent("bl:CL_E:NUI:Notify", "Your have sold a " + vi.name, "right");

                                        Utils.Logging.PrintToConsole("(" + p.Name + ") " + u.GetSelectedProfile().ProfileName + " sold item(" + i.ItemName + ") to vendor(" + vendorId + ")");
                                        Utils.Logging.PrintToDiscord("(" + p.Name + ") " + u.GetSelectedProfile().ProfileName + " sold item(" + i.ItemName + ") to vendor(" + vendorId + ")");
                                    }
                                }
                                else
                                {
                                    // Log attempt to sell to vendor without having item
                                    p.TriggerEvent("bl:CL_E:NUI:Notify", "You do not have this item", "right");
                                    Utils.Logging.PrintToConsole("(" + p.Name + ") " + u.GetSelectedProfile().ProfileName + " attempted to sell item(" + i.ItemName + ") to vendor(" + vendorId + ")");
                                    Utils.Logging.PrintToDiscord("(" + p.Name + ") " + u.GetSelectedProfile().ProfileName + " attempted to sell item(" + i.ItemName + ") to vendor(" + vendorId + ")");
                                }
                            }
                            else
                            {
                                // Log attempt to sell to vendor with null item index
                                p.TriggerEvent("bl:CL_E:NUI:Notify", "Unable to sell item (Code: NII)", "right");
                                Utils.Logging.PrintToConsole("(" + p.Name + ") " + u.GetSelectedProfile().ProfileName + " attempted to sell null item(" + itemIndex + ") to vendor(" + vendorId + ")");
                                Utils.Logging.PrintToDiscord("(" + p.Name + ") " + u.GetSelectedProfile().ProfileName + " attempted to sell null item(" + itemIndex + ") to vendor(" + vendorId + ")");
                            }
                        }
                        else
                        {
                            // Log attempt to sell to vendor without User object
                            p.TriggerEvent("bl:CL_E:NUI:Notify", "Unable to sell item (Code: NUO)", "right");
                            Utils.Logging.PrintToConsole("(" + p.Name + ") attempted to sell an item without a User object to vendor(" + vendorId + ")");
                            Utils.Logging.PrintToDiscord("(" + p.Name + ") attempted to sell an item without a User object to vendor(" + vendorId + ")");
                        }
                    }
                    else
                    {
                        // Log attempt to sell to vendor without items
                        Utils.Logging.PrintToConsole("(" + p.Name + ") attempted to sell an item to vendor(" + vendorId + ") without it having items");
                        Utils.Logging.PrintToDiscord("(" + p.Name + ") attempted to sell an item to vendor(" + vendorId + ") without it having items");
                    }
                }
            }
            else
            {
                // Log attempt to sell without registered vendors
                Utils.Logging.PrintToConsole("(" + p.Name + ") attempted to sell an item to vendor(" + vendorId + ") without it existing");
                Utils.Logging.PrintToDiscord("(" + p.Name + ") attempted to sell an item to vendor(" + vendorId + ") without it existing");
            }
        }

        public void AttemptPurchase([FromSource]Player p, int vendorId, int itemIndex)
        {
            if(this.RegisteredVendors.Count > 0)
            {
                if(this.RegisteredVendors[vendorId - 1] != null)
                {
                    Vendor v = this.RegisteredVendors[vendorId - 1];
                    if(v.items.Count > 0)
                    {
                        User u = Server.INSTANCE.UserManager.GetPlayerFromID(Int32.Parse(p.Handle));
                        if(u != null && u.GetSelectedProfile() != null)
                        {
                            Profile pr = u.GetSelectedProfile();
                            if(v.items[itemIndex] != null)
                            {
                                VendorItem vi = v.items[itemIndex];
                                if(vi.useStock)
                                {
                                    // Purchase and deduct
                                    if(!(vi.currentStock <= 0))
                                    {
                                        if (pr.Bal_Wallet >= vi.buyPrice)
                                        {
                                            if (!(pr.Inventory_IsFull()))
                                            {
                                                BlItem i = Server.INSTANCE.ItemManager.GetItem(vi.baseId);
                                                if (i != null)
                                                {
                                                    if (v.useMoney)
                                                    {
                                                        v.storedMoney += vi.buyPrice;
                                                    }

                                                    pr.Bal_Wallet = pr.Bal_Wallet - vi.buyPrice;
                                                    pr.Inventory_AddItem(new InventoryItem(i.ItemID, 1));

                                                    vi.currentStock = vi.currentStock - 1;

                                                    u.save();
                                                    u.sendProfiles();

                                                    v.save();
                                                    this.SendVendorItems(p, v.id);

                                                    p.TriggerEvent("bl:CL_E:NUI:Notify", "You have purchased a " + i.ItemName, "right");
                                                }
                                            }
                                            else
                                            {
                                                p.TriggerEvent("bl:CL_E:NUI:Notify", "Your inventory is full", "right");
                                            }
                                        }
                                        else
                                        {
                                            p.TriggerEvent("bl:CL_E:NUI:Notify", "Your cannot afford this item", "right");
                                        }
                                    }
                                    else
                                    {
                                        p.TriggerEvent("bl:CL_E:NUI:Notify", "This item is out of stock", "right");
                                    }
                                }
                                else
                                {
                                    if(pr.Bal_Wallet >= vi.buyPrice)
                                    {
                                        if (!(pr.Inventory_IsFull()))
                                        {
                                            BlItem i = Server.INSTANCE.ItemManager.GetItem(vi.baseId);
                                            if(i != null)
                                            {
                                                if(v.useMoney)
                                                {
                                                    v.storedMoney += vi.buyPrice;
                                                }

                                                pr.Bal_Wallet = pr.Bal_Wallet - vi.buyPrice;
                                                pr.Inventory_AddItem(new InventoryItem(i.ItemID, 1));

                                                u.save();
                                                u.sendProfiles();

                                                v.save();
                                                this.SendVendorItems(p, v.id);

                                                p.TriggerEvent("bl:CL_E:NUI:Notify", "You have purchased a " + i.ItemName, "right");
                                                Utils.Logging.PrintToConsole("(" + p.Name + ") " + u.GetSelectedProfile().ProfileName + " pruchased item(" + i.ItemName + ") from vendor(" + vendorId + ")");
                                                Utils.Logging.PrintToDiscord("(" + p.Name + ") " + u.GetSelectedProfile().ProfileName + " pruchased item(" + i.ItemName + ") from vendor(" + vendorId + ")");
                                            }
                                        }
                                        else
                                        {
                                            p.TriggerEvent("bl:CL_E:NUI:Notify", "Your inventory is full", "right");
                                        }
                                    }
                                    else
                                    {
                                        p.TriggerEvent("bl:CL_E:NUI:Notify", "Your cannot afford this item", "right");
                                    }
                                }
                            }
                        }
                        else
                        {
                            // Log attempt to purchase from vendor without valid User object
                            Utils.Logging.PrintToConsole("(" + p.Name + ") attempted to purchase item without valid User object from vendor(" + vendorId + ")");
                            Utils.Logging.PrintToDiscord("(" + p.Name + ") attempted to purchase item without valid User object from vendor(" + vendorId + ")");
                        }
                    }
                    else
                    {
                        // Log attempt to purchase from vendor with no items
                        Utils.Logging.PrintToConsole("(" + p.Name + ") attempted to purchase item from vendor(" + vendorId + ") wtih no items");
                        Utils.Logging.PrintToDiscord("(" + p.Name + ") attempted to purchase item from vendor(" + vendorId + ") with no items");
                    }
                }
            }
        }

        public void DeleteItemFromVendor([FromSource]Player p, int vendorId, int itemId)
        {
            if (API.IsPlayerAceAllowed(p.Handle, "vendors.edit") && this.RegisteredVendors.Count > 0)
            {
                if (this.RegisteredVendors[vendorId - 1] != null)
                {
                    Vendor v = this.RegisteredVendors[vendorId - 1];
                    if(v.items.Count > 0)
                    {
                        if (v.items[itemId] != null)
                        {
                            v.items.RemoveAt(itemId);
                            v.save();
                            this.SendVendorItems(p, vendorId);

                            User u = Server.INSTANCE.UserManager.GetPlayerFromID(Int32.Parse(p.Handle));
                            if (u != null)
                            {
                                Utils.Logging.PrintToConsole("(" + p.Name + ") " + u.GetSelectedProfile().ProfileName + " deleted item(" + itemId + ") from vendor(" + vendorId + ")");
                                Utils.Logging.PrintToDiscord("(" + p.Name + ") " + u.GetSelectedProfile().ProfileName + " deleted item(" + itemId + ") from vendor(" + vendorId + ")");
                            }
                            else
                            {
                                Utils.Logging.PrintToConsole("(?) " + p.Name + " deleted item(" + itemId + ") from vendor(" + vendorId + ")");
                                Utils.Logging.PrintToDiscord("(?) " + p.Name + " deleted item(" + itemId + ") from vendor(" + vendorId + ")");
                            }
                        }
                    }
                }
            }
            else
            {
                User u = Server.INSTANCE.UserManager.GetPlayerFromID(Int32.Parse(p.Handle));
                if (u != null)
                {
                    Utils.Logging.PrintToConsole("(" + p.Name + ") " + u.GetSelectedProfile().ProfileName + " tried to delete an item in a vendor!");
                    Utils.Logging.PrintToDiscord("(" + p.Name + ") " + u.GetSelectedProfile().ProfileName + " tried to delete an item in a vendor!");
                }
                else
                {
                    Utils.Logging.PrintToConsole("(?) " + p.Name + " tried to delete an item in a vendor!");
                    Utils.Logging.PrintToDiscord("(?) " + p.Name + " tried to delete an item in a vendor!");
                }
            }
        }

        public void UpdateVendorItems([FromSource]Player p, int vendorId, int itemId, string itemJson)
        {
            if(API.IsPlayerAceAllowed(p.Handle, "vendors.edit") && this.RegisteredVendors.Count > 0)
            {
                if(this.RegisteredVendors[vendorId - 1] != null)
                {
                    Vendor v = this.RegisteredVendors[vendorId - 1];
                    if(v.items.Count > 0)
                    {
                        if(v.items[itemId] != null)
                        {
                            VendorItem vi = JsonConvert.DeserializeObject<VendorItem>(itemJson);
                            v.items[itemId] = vi;
                            v.save();
                            this.SendVendorItems(p, vendorId);

                            User u = Server.INSTANCE.UserManager.GetPlayerFromID(Int32.Parse(p.Handle));
                            if (u != null)
                            {
                                Utils.Logging.PrintToConsole("(" + p.Name + ") " + u.GetSelectedProfile().ProfileName + " updated item(" + itemId + ") in vendor(" + vendorId + ")");
                                Utils.Logging.PrintToDiscord("(" + p.Name + ") " + u.GetSelectedProfile().ProfileName + " updated item(" + itemId + ") in vendor(" + vendorId + ")");
                            }
                            else
                            {
                                Utils.Logging.PrintToConsole("(?) " + p.Name + " updated item(" + itemId + ") in vendor(" + vendorId + ")");
                                Utils.Logging.PrintToDiscord("(?) " + p.Name + " updated item(" + itemId + ") in vendor(" + vendorId + ")");
                            }
                        }
                    }
                }
            }
            else
            {
                User u = Server.INSTANCE.UserManager.GetPlayerFromID(Int32.Parse(p.Handle));
                if (u != null)
                {
                    Utils.Logging.PrintToConsole("(" + p.Name + ") " + u.GetSelectedProfile().ProfileName + " tried to update an item in a vendor!");
                    Utils.Logging.PrintToDiscord("(" + p.Name + ") " + u.GetSelectedProfile().ProfileName + " tried to update an item in a vendor!");
                }
                else
                {
                    Utils.Logging.PrintToConsole("(?) " + p.Name + " tried to update an item in a vendor!");
                    Utils.Logging.PrintToDiscord("(?) " + p.Name + " tried to update an item in a vendor!");
                }
            }
        }

        public void RetrieveMoneyFromVendor([FromSource]Player p, int vendorId)
        {
            if(API.IsPlayerAceAllowed(p.Handle, "vendors.edit") && this.RegisteredVendors.Count > 0)
            {
                if (this.RegisteredVendors[vendorId - 1] != null)
                {
                    Vendor v = this.RegisteredVendors[vendorId - 1];
                    User u = Server.INSTANCE.UserManager.GetPlayerFromID(Int32.Parse(p.Handle));
                    if (u != null)
                    {
                        if(u.GetSelectedProfile() != null)
                        {
                            if(v.useMoney)
                            {
                                if(v.storedMoney > 0)
                                {
                                    int oldMoney = v.storedMoney;

                                    u.GetSelectedProfile().Bal_Wallet += v.storedMoney;
                                    u.save();
                                    u.sendProfiles();

                                    v.storedMoney = 0;
                                    v.save();
                                    this.SendVendorItems(p, vendorId);

                                    Utils.Logging.PrintToConsole("(" + p.Name + ") " + u.GetSelectedProfile().ProfileName + " retrieved money from vendor(" + vendorId + "): " + oldMoney + " -> " + v.storedMoney);
                                    Utils.Logging.PrintToDiscord("(" + p.Name + ") " + u.GetSelectedProfile().ProfileName + " retrieved money from vendor(" + vendorId + "): " + oldMoney + " -> " + v.storedMoney);
                                }
                            }
                        }
                    }
                }
            }
        }

        public void SetVendorTitle([FromSource]Player p, string name, int vendorId)
        {
            if(API.IsPlayerAceAllowed(p.Handle, "vendors.edit") && this.RegisteredVendors.Count > 0)
            {
                if(this.RegisteredVendors[vendorId - 1] != null)
                {
                    Vendor v = this.RegisteredVendors[vendorId - 1];
                    User u = Server.INSTANCE.UserManager.GetPlayerFromID(Int32.Parse(p.Handle));
                    if(u != null)
                    {
                        if(u.GetSelectedProfile() != null)
                        {
                            string oldName = v.title;
                            v.title = name;
                            v.save();
                            this.SendVendorItems(p, vendorId);

                            Utils.Logging.PrintToConsole("(" + p.Name + ") " + u.GetSelectedProfile().ProfileName + " changed vendor title for vendor(" + vendorId + "): " + oldName + " -> " + v.title);
                            Utils.Logging.PrintToDiscord("(" + p.Name + ") " + u.GetSelectedProfile().ProfileName + " changed vendor title for vendor(" + vendorId + "): " + oldName + " -> " + v.title);
                        }
                    }
                }
            }
        }

        public void SendVendorItems([FromSource]Player p, int vendorId)
        {
            if(this.RegisteredVendors.Count > 0)
            {
                Vendor tempVendor = this.RegisteredVendors[vendorId - 1];
                if(tempVendor != null)
                {
                    PlayerList pl = new PlayerList();
                    foreach(Player ply in pl)
                    {
                        ply.TriggerEvent("bl:CL_E:Vendor:RefreshData", vendorId, tempVendor.title, tempVendor.storedMoney, JsonConvert.SerializeObject(tempVendor.items), JsonConvert.SerializeObject(tempVendor.factions), JsonConvert.SerializeObject(tempVendor.classes));
                    }
                }
            }
        }

        public void AddFactionToVendor([FromSource]Player p, string facName, int vendorId)
        {
            if(API.IsPlayerAceAllowed(p.Handle, "vendors.edit") && this.RegisteredVendors.Count > 0)
            {
                Vendor tempVendor = this.RegisteredVendors[vendorId - 1];
                if(tempVendor != null)
                {
                    if(Server.INSTANCE.FactionManager.GetFaction(facName) != null)
                    {
                        Faction tempFaction = Server.INSTANCE.FactionManager.GetFaction(facName);
                        tempVendor.factions.Add(tempFaction);
                        tempVendor.save();
                        this.SendVendorItems(p, vendorId);

                        User u = Server.INSTANCE.UserManager.GetPlayerFromID(Int32.Parse(p.Handle));
                        if (u != null)
                        {
                            Utils.Logging.PrintToConsole("(" + p.Name + ") " + u.GetSelectedProfile().ProfileName + " added new faction to vendor(" + vendorId + "): " + JsonConvert.SerializeObject(tempFaction));
                            Utils.Logging.PrintToDiscord("(" + p.Name + ") " + u.GetSelectedProfile().ProfileName + " added new faction to vendor(" + vendorId + "): " + JsonConvert.SerializeObject(tempFaction));
                        }
                        else
                        {
                            Utils.Logging.PrintToConsole("(?) " + p.Name + " added new faction to vendor(" + vendorId + "): " + JsonConvert.SerializeObject(tempFaction));
                            Utils.Logging.PrintToDiscord("(?) " + p.Name + " added new faction to vendor(" + vendorId + "): " + JsonConvert.SerializeObject(tempFaction));
                        }
                    }
                }
            }
            else
            {
                User u = Server.INSTANCE.UserManager.GetPlayerFromID(Int32.Parse(p.Handle));
                if (u != null)
                {
                    Utils.Logging.PrintToConsole("(" + p.Name + ") " + u.GetSelectedProfile().ProfileName + " tried to add a new faction to a vendor!");
                    Utils.Logging.PrintToDiscord("(" + p.Name + ") " + u.GetSelectedProfile().ProfileName + " tried to add a new faction to a vendor!");
                }
                else
                {
                    Utils.Logging.PrintToConsole("(?) " + p.Name + " tried to add a new faction to a vendor!");
                    Utils.Logging.PrintToDiscord("(?) " + p.Name + " tried to add a new faction to a vendor!");
                }
            }
        }

        public void RemoveClassFromVendor([FromSource]Player p, string classId, int vendorId)
        {
            if (API.IsPlayerAceAllowed(p.Handle, "vendors.edit") && this.RegisteredVendors.Count > 0)
            {
                Vendor tempVendor = this.RegisteredVendors[vendorId - 1];
                if(tempVendor != null)
                {
                    if(tempVendor.classes.Count > 0)
                    {
                        MetroClass c = null;
                        foreach(MetroClass cc in tempVendor.classes)
                        {
                            if(cc.ClassName == classId)
                            {
                                c = cc;
                            }
                        }

                        if(c != null)
                        {
                            tempVendor.classes.Remove(c);
                            tempVendor.save();
                            this.SendVendorItems(p, vendorId);

                            User u = Server.INSTANCE.UserManager.GetPlayerFromID(Int32.Parse(p.Handle));
                            if (u != null)
                            {
                                Utils.Logging.PrintToConsole("(" + p.Name + ") " + u.GetSelectedProfile().ProfileName + " removed class '" + c.ClassName + "' from vendor(" + vendorId + ")");
                                Utils.Logging.PrintToDiscord("(" + p.Name + ") " + u.GetSelectedProfile().ProfileName + " removed class '" + c.ClassName + "' from vendor(" + vendorId + ")");
                            }
                            else
                            {
                                Utils.Logging.PrintToConsole("(?) " + p.Name + " removed class '" + c.ClassName + "' from vendor(" + vendorId + ")");
                                Utils.Logging.PrintToDiscord("(?) " + p.Name + " removed class '" + c.ClassName + "' from vendor(" + vendorId + ")");
                            }
                        }
                    }
                }
            }
            else
            {
                User u = Server.INSTANCE.UserManager.GetPlayerFromID(Int32.Parse(p.Handle));
                if (u != null)
                {
                    Utils.Logging.PrintToConsole("(" + p.Name + ") " + u.GetSelectedProfile().ProfileName + " tried to remove a class from a vendor!");
                    Utils.Logging.PrintToDiscord("(" + p.Name + ") " + u.GetSelectedProfile().ProfileName + " tried to remove a class from a vendor!");
                }
                else
                {
                    Utils.Logging.PrintToConsole("(?) " + p.Name + " tried to remove a class from a vendor!");
                    Utils.Logging.PrintToDiscord("(?) " + p.Name + " tried to remove a class from a vendor!");
                }
            }
        }

        public void RemoveFactionFromVendor([FromSource]Player p, string facId, int vendorId)
        {
            if (API.IsPlayerAceAllowed(p.Handle, "vendors.edit") && this.RegisteredVendors.Count > 0)
            {
                Vendor tempVendor = this.RegisteredVendors[vendorId - 1];
                if (tempVendor != null)
                {
                    if(tempVendor.factions.Count > 0)
                    {
                        Faction f = null;
                        foreach(Faction ff in tempVendor.factions)
                        {
                            if(ff.FactionName == facId)
                            {
                                f = ff;
                            }
                        }

                        if(f != null)
                        {
                            tempVendor.factions.Remove(f);
                            tempVendor.save();
                            this.SendVendorItems(p, vendorId);

                            User u = Server.INSTANCE.UserManager.GetPlayerFromID(Int32.Parse(p.Handle));
                            if (u != null)
                            {
                                Utils.Logging.PrintToConsole("(" + p.Name + ") " + u.GetSelectedProfile().ProfileName + " removed faction '" + f.FactionName + "' from vendor(" + vendorId + ")");
                                Utils.Logging.PrintToDiscord("(" + p.Name + ") " + u.GetSelectedProfile().ProfileName + " removed faction '" + f.FactionName + "' from vendor(" + vendorId + ")");
                            }
                            else
                            {
                                Utils.Logging.PrintToConsole("(?) " + p.Name + " removed faction '" + f.FactionName + "' from vendor(" + vendorId + ")");
                                Utils.Logging.PrintToDiscord("(?) " + p.Name + " removed faction '" + f.FactionName + "' from vendor(" + vendorId + ")");
                            }
                        }
                    }
                }
            }
            else
            {
                User u = Server.INSTANCE.UserManager.GetPlayerFromID(Int32.Parse(p.Handle));
                if (u != null)
                {
                    Utils.Logging.PrintToConsole("(" + p.Name + ") " + u.GetSelectedProfile().ProfileName + " tried to remove a faction from a vendor!");
                    Utils.Logging.PrintToDiscord("(" + p.Name + ") " + u.GetSelectedProfile().ProfileName + " tried to remove a faction from a vendor!");
                }
                else
                {
                    Utils.Logging.PrintToConsole("(?) " + p.Name + " tried to remove a faction from a vendor!");
                    Utils.Logging.PrintToDiscord("(?) " + p.Name + " tried to remove a faction from a vendor!");
                }
            }
        }

        public void AddClassToVendor([FromSource]Player p, string facName, string className, int vendorId)
        {
            if (API.IsPlayerAceAllowed(p.Handle, "vendors.edit") && this.RegisteredVendors.Count > 0)
            {
                Vendor tempVendor = this.RegisteredVendors[vendorId - 1];
                if (tempVendor != null)
                {
                    if (Server.INSTANCE.FactionManager.GetFaction(facName) != null)
                    {
                        Faction tempFaction = Server.INSTANCE.FactionManager.GetFaction(facName);
                        if(tempFaction.Classes.Count > 0)
                        {
                            MetroClass tempClass = null;
                            foreach(MetroClass c in tempFaction.Classes)
                            {
                                if(c.ClassName == className)
                                {
                                    tempClass = c;
                                }
                            }

                            if(tempClass != null)
                            {
                                tempVendor.classes.Add(tempClass);
                                tempVendor.save();
                                this.SendVendorItems(p, vendorId);

                                User u = Server.INSTANCE.UserManager.GetPlayerFromID(Int32.Parse(p.Handle));
                                if (u != null)
                                {
                                    Utils.Logging.PrintToConsole("(" + p.Name + ") " + u.GetSelectedProfile().ProfileName + " added new class to vendor(" + vendorId + "): " + JsonConvert.SerializeObject(tempClass));
                                    Utils.Logging.PrintToDiscord("(" + p.Name + ") " + u.GetSelectedProfile().ProfileName + " added new class to vendor(" + vendorId + "): " + JsonConvert.SerializeObject(tempClass));
                                }
                                else
                                {
                                    Utils.Logging.PrintToConsole("(?) " + p.Name + " added new class to vendor(" + vendorId + "): " + JsonConvert.SerializeObject(tempClass));
                                    Utils.Logging.PrintToDiscord("(?) " + p.Name + " added new class to vendor(" + vendorId + "): " + JsonConvert.SerializeObject(tempClass));
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                User u = Server.INSTANCE.UserManager.GetPlayerFromID(Int32.Parse(p.Handle));
                if (u != null)
                {
                    Utils.Logging.PrintToConsole("(" + p.Name + ") " + u.GetSelectedProfile().ProfileName + " tried to add a new class to a vendor!");
                    Utils.Logging.PrintToDiscord("(" + p.Name + ") " + u.GetSelectedProfile().ProfileName + " tried to add a new class to a vendor!");
                }
                else
                {
                    Utils.Logging.PrintToConsole("(?) " + p.Name + " tried to add a new class to a vendor!");
                    Utils.Logging.PrintToDiscord("(?) " + p.Name + " tried to add a new class to a vendor!");
                }
            }
        }

        public void AddItemToVendor([FromSource]Player p, string itemRef, int vendorId)
        {
            if(API.IsPlayerAceAllowed(p.Handle, "vendors.edit") && this.RegisteredVendors.Count > 0)
            {
                Vendor tempVendor = this.RegisteredVendors[vendorId - 1];
                if (tempVendor != null)
                {
                    if (Server.INSTANCE.ItemManager.GetItem(itemRef) != null)
                    {
                        BlItem tempItem = Server.INSTANCE.ItemManager.GetItem(itemRef);
                        VendorItem vi = new VendorItem();
                        vi.baseId = tempItem.ItemID;
                        vi.name = tempItem.ItemName;
                        vi.buyPrice = 0;
                        vi.sellPrice = 0;
                        vi.stockAmount = 0;
                        vi.useStock = false;
                        vi.maxStock = 0;
                        vi.currentStock = 1;

                        tempVendor.items.Add(vi);
                        tempVendor.save();
                        this.SendVendorItems(p, vendorId);

                        User u = Server.INSTANCE.UserManager.GetPlayerFromID(Int32.Parse(p.Handle));
                        if (u != null)
                        {
                            Utils.Logging.PrintToConsole("(" + p.Name + ") " + u.GetSelectedProfile().ProfileName + " added new item to vendor(" + vendorId + "): " + JsonConvert.SerializeObject(vi));
                            Utils.Logging.PrintToDiscord("(" + p.Name + ") " + u.GetSelectedProfile().ProfileName + " added new item to vendor(" + vendorId + "): " + JsonConvert.SerializeObject(vi));
                        }
                        else
                        {
                            Utils.Logging.PrintToConsole("(?) " + p.Name + " added new item to vendor(" + vendorId + "): " + JsonConvert.SerializeObject(vi));
                            Utils.Logging.PrintToDiscord("(?) " + p.Name + " added new item to vendor(" + vendorId + "): " + JsonConvert.SerializeObject(vi));
                        }
                    }
                }
            }
            else
            {
                User u = Server.INSTANCE.UserManager.GetPlayerFromID(Int32.Parse(p.Handle));
                if(u != null)
                {
                    Utils.Logging.PrintToConsole("(" + p.Name + ") " + u.GetSelectedProfile().ProfileName + " tried to add a new item to a vendor!");
                    Utils.Logging.PrintToDiscord("(" + p.Name + ") " + u.GetSelectedProfile().ProfileName + " tried to add a new item to a vendor!");
                }
                else
                {
                    Utils.Logging.PrintToConsole("(?) " + p.Name + " tried to add a new item to a vendor!");
                    Utils.Logging.PrintToDiscord("(?) " + p.Name + " tried to add a new item to a vendor!");
                }
            }
        }
    }
}
