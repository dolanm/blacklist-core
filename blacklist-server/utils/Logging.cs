﻿using CitizenFX.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blacklist_server.utils
{
    public class Logging
    {
        public void PrintToConsole(string msg)
        {
            if (msg == null || msg == "")
            {
                CitizenFX.Core.Debug.WriteLine("[" + DateTime.Now.ToString("HH:mm:ss") + "][BLACKLIST] => Invalid message provided in: Logging.PrintToConsole(string msg)");
            }
            else
            {
                StreamWriter sw = this.getFile();
                sw.WriteLine("[" + DateTime.Now.ToString("HH:mm:ss") + "][BLACKLIST] => " + msg);
                sw.Close();
                CitizenFX.Core.Debug.WriteLine("[" + DateTime.Now.ToString("HH:mm:ss") + "][BLACKLIST] => " + msg.Replace('^', '&'));
            }
        }

        public void PrintToDiscord(string msg)
        {
            if(Convert.ToBoolean(Server.INSTANCE.ServerCfg.LoadedConfig["bl_useDiscord"]) == true)
            {
                Server.INSTANCE.BaseScriptProxy.Exports["bl-core"].SendToDiscord(16753920, "LS: Life", msg, DateTime.Now.ToString());
            }
        }

        public StreamWriter getFile()
        {
            String name = DateTime.Now.ToString("yyyy-MM-dd");

            return Utils.getFile("logs/" + name, "log");
        }
    }
}
