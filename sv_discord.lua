exports('SendToDiscord', function(color, name, message, footer)
	sendToDiscord(color, name, message, footer)
end)

function sendToDiscord(color, name, message, footer)
	local embed = {
		{
			["color"] = color,
			["title"] = "**" .. name .. "**",
			["description"] = message,
			["footer"] = {
				["text"] = footer,
			},
		}
	}

	PerformHttpRequest(GetConvar("bl_discordHook", ""), function(err, text, headers) end, 'POST', json.encode({username = name, embeds = embed}), {['Content-Type'] = 'application/json'})
end